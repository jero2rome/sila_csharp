# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and starting with version number 1.0.0 this project will adhere to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

The version numbers in this changelog refer to the `Sila2` core and `Sila2.Utils` libraries (guaranteed to match).

## [vNext]

### Compatible changes

- !117 * added BinaryTransferError to ErrorHandling class and use it within the binary transfer framework methods
       * extended FeatureDefinitionEx class to get all kinds of feature elements by list or from FQI
       * adjusted the raising of all validation and defined execution errors to use fully qualified parameter identifiers

### Compatible Changes
- !109 Fix: SiLA2.Tools CodeGen not able to generate code if path contains spaces
- !111 Update servers to expose a command to stop the server
- !115 Disable dotnet tests on CI due to CI issue
- !114 Sila2.Tools: Replace sila_java code generator with xslt code generator

## [0.4.0]

### Compatible Changes 
- Update `RobotControl` to support new standardized UniteLabs features
- !99 Update info log color to make it easier to read on a blue background
- !63 Version information displayed with latest tag and commit hash when using `--version` flag on apps
- !63 Fix: listing of interfaces on returning non-zero exit code on apps
- !105 Add: support for encryption on SiLA2Server (clients not yet supported)

## [0.3.0]
### Incompatible Changes
- !87 adjusted Feature identifiers returned by ImplementedFeatures property of the SiLAService Feature.
- !90 update library and servers to sila_base 1.0 release

### Compatible Changes
- Update global stub generation scripts for linux
- !80 add support for metadata and example
- !48 add Sila2.Tools package with automated stub generation support
- Improve error message of exceptions with multiple cause

## [0.2.0] - (introduction of changelog, first tag-based release)
- version numbers of Sila2 NuGet package and assembly are now identical
- dropped `-master` suffix from released NuGet package because it was recognized as *prerelease*
- !62 updated the `sila_base` library to `v2`, introducing breaking changes w.r.t observable commands, SmallBinary and SiLAErrors
- !56 refactored the Sila2 namespaces (breaking) and added util-APIs for managing observable commands
- first tag-triggered release of `Sila2` and `Sila2.Utils`
- removal of redundant code in `sila_library` directory

## [0.1.0] - (everything before introduction of the changelog)
- first release
