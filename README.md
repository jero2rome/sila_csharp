# SiLA C# [![pipeline status](https://gitlab.com/SiLA2/sila_csharp/badges/master/pipeline.svg)](https://gitlab.com/SiLA2/sila_csharp/commits/master)
This repository contains examples, libraries and reference implementations of the SiLA 2 standard using the language C#.

It is intended to provide the content for all operating systems that support the .NET Core platform (Microsoft Windows, 
Linux, macOS). By now the project infrastructure is only provided for Windows Visual Studio but can also be build 
via the .NET CLI, the C# code itself as well as the protobuf compiler scripts can be used on all platforms.

SiLA Java reference implementation. This repository consists of reference implementations, Software Drivers, and libraries to drive the adoption of the SiLA 2 Standard.

|||
| ---------------| ----------------------------------------------------------- |
| SiLA Homepage  | [https://sila-standard.com](https://sila-standard.com)      |
| Chat group     | [Join the group on Slack](https://join.slack.com/t/sila-standard/shared_invite/enQtNDI0ODcxMDg5NzkzLTBhOTU3N2I0NTc4NDcyMjg2ZDIwZDc1Yjg4N2FmYjZkMzljZDAyZjAwNTc5OTVjYjIwZWJjYjA0YTY0NTFiNDA)|
| Maintainer     | Christoph Pohl ([cpohl@inheco.com](mailto:cpohl@inheco.com)) of [Inheco](https://www.inheco.com)|
| Maintainer     | Stefan Koch    ([koch@equicon.de](mailto:koch@equicon.de)) of [EQUIcon Software GmbH](https://www.equicon.de/en/)|

If you are new to the tooling with Git Repositories, and the .NET build systems, we provided a step by step guide in [Quick Start](https://gitlab.com/SiLA2/sila_csharp/wikis/Quick-Start).

## Wiki
The wiki includes quick start guides and tutorials as an easy entry point: [wiki](https://gitlab.com/SiLA2/sila_csharp/wikis/home)

## Status
**Important**: This code, in its current form, is mainly to give active SiLA 2 WG Members and other interested parties a reference point. 
It might not comply with the latest version of the Standard, and its contents may change in the future.

For more general information about the standard, we refer to the [sila_base](https://gitlab.com/SiLA2/sila_base) repository.

The reference code is currently mainly usable from source, proper versioned binaries are in progress and will be available beginning of 2020.

## Cloning
Clone the repository either with HTTPs or SSH (choose the appropriate link in the clone button)
```bash
git clone https://gitlab.com/SiLA2/sila_csharp.git
```

**Note**: This repository uses [sila_base](https://gitlab.com/SiLA2/sila_base) as a submodule. Hence you need to fetch submodules

In the new folder: Initialize the yor local configuration file:
```bash
git submodule update --init --recursive
```
Fetch all the data from the project and check out the appropriate commit listed.
```bash
git submodule update --recursive
```

As an entry point, refer to `examples/HelloSila`.

## Contributing

To get involved, read our [contributing docs](https://gitlab.com/SiLA2/sila_base/blob/master/CONTRIBUTING.md) in [sila_base](https://gitlab.com/SiLA2/sila_base).

### C# Style Guide

Please follow the [general C# coding guidelines](https://docs.microsoft.com/en-us/dotnet/standard/design-guidelines/).

## Getting Started

If you would like to get started with this repository then please refer to the [SiLA C# Quick Start Guide](https://gitlab.com/SiLA2/sila_csharp/wikis/Quick-Start). This guide will help you set up your development environment and teach you how to run the example Hello SiLA server.

### Using the Library
If you would like to use the latest release of the project just add the nuget repository to your feed and 
add the library as a dependecy to your project. See [instructions](docs/deployment.md).

## Project Structure
There are two solutions in this repository:

- [sila_csharp.sln](sila_csharp.sln): which includes the whole library projects, and all example projects
- [servers/servers.sln](servers/servers.sln): which includes only ACTUAL SiLA 2 compliant server and working server implementations


#### servers
SiLA Servers (e.g. device drivers) are server implementation which are compliant to the SiLA 2 standard. These 
implementations are usually either .NET Core 3 or .NET Framework 4.7.2

To build the [servers/servers.sln](servers/servers.sln) you will need to at least have:
    - .NET Core 3.0 SDK or greater
    - .NET Framework 4.7.2 (if on windows)

If you would like to use the servers then please refer to the README of the corresponding server for installation 
instructions.

For a list of available servers please have a look at [servers](servers/README.md).

#### examples
Example server and client implementations for demonstrating the SiLA 2 working principles. any `*.ServerApp` project 
corresponds to a server application and any `*.ClientApp` corresponds to a client implementation. 

### library
The SiLA Library provides base classes for implementing SiLA Servers and utilities that can be used by SiLA Clients according to Part A+B of the SiLA 2 specification.

### tools
Tools such as the protobuf compilers of gRPC (will contain also code generators).

## Build and Package the Sila2.Tools package
You must build Sila2.Tools project and package it so it can be added to your nuget cache by the sila_implementaions projects which use it.

Before running any examples you must
```
cd sila_tools/Sila2.Tools
dotnet pack -c Release
```

## Development Guidelines

### Testing and Debugging
If you would like to test the CI using a local gitlab runner then [read here](https://gitlab.com/unitelabs/unitelabs_guidelines/blob/master/tools/ci/guideline.md#running-a-ci-pipeline-stage-with-the-gitlab-runner)

### Driver Development targeting .NET Framework
`sila_csharp` is built using .NET standard which is the base layer libraries that can be used 
by both the .NET core and .NET framework runtimes. Therefore (unless otherwise necessary) 
your base libraries should be built for .NET standard such that .NET core and .NET framework 
applications can use them.

In most cases, one might want to use `sila_csharp` to develop a Device driver which requires 
interaction with the instrument via a .NET Framework API. In this case your Device driver 
application should be built against a .NET Framwork.

In most cases, one might want to use the `sila_csharp/src` in the implementation that connects the SiLA commands with the device/instrument API (e.g. a .NET API from a DLL).

 [TODO: Example to be given here]


### Compiling proto files

To compile the protobuf files into C# stubs used by the implementations, the proto compiler(s) of the Grpc.Tools package are used. On Windows 10 always execute the script on the Powershell:


```bash
tools/generate_proto_and_stub.sh <feature-file> <proto-path> <stub-path>
```

Each server implementation documents how this should be done.

Note: As the required C# stubs are contained in the repository you don't need to compile the protobuf files to be able to build 
the examples. Only modifications of the protobuf files will require a new compilation.

### Generating `FeatureDefinition.cs` from Schema
If the [FeatureDefinition.xsd]() schema changes in sila_base, you will have to re-generate the 
[FeatureDefinition.cs](src/Sila2/models/FeatureDefinition.cs) class file. To do so you will need the [XSD](https://docs.microsoft.com/en-us/dotnet/standard/serialization/xml-schema-definiti
on-tool-xsd-exe) tool

```cmd
cd src\Sila2\models
xsd.exe ..\..\..\sila_base\sila_base\schema\FeatureDefinition.xsd /classes /namespace:Sila2
```

Commit the updated class.


## Issues and Bugs
If you see an issue please feel free to file a bug on the project [list of issues](https://gitlab.com/SiLA2/sila_csharp/issues)

## License
This code is licensed under the [MIT License](https://en.wikipedia.org/wiki/MIT_License)
