# Library Deployment
SiLA C# library is planned to be made available to the public via 
a NuGet feed such as nuget.org or MyGet.com. But this is still pending to be done
until the first 1.0.0 release of SiLA C#.

**Staging Deployment**
To prepare for official releases this project performs staging deployments
which involve deploying the NuGet package to a public staging NuGet feed
which users can add as a source in their development environements.

The remainder of this section will explain what is made avaialble in the staging
feed, where it is located and how to use these packages.

**Staging NuGet Feed**
Staging NuGet feed is found here: [https://nexus.unitelabs.ch/#browse/browse:nuget-sila-development](https://nexus.unitelabs.ch/#browse/browse:nuget-sila-development)

**Versioning**
The SiLA Library project is versioned with the following pattern `<version-number>-master`
where `<version-number>` is the planned version to be released in the next release cycle.

For every commit to `master` the CI will build and publish two packages which are identical but with different package versions names:

  1. `<version-number>-master-<commit-id>`: Package identifiable by its commit hash, or planned version number

  2. `<version-number>-master`: Package which overwrites any previous packages that we're committed by an earlier `master` commit with the same planned version number.

This then allows you as a developer to either depend on `Sila2` as a NuGet package by EXACT reference to the version you would like to use, or using the latest master build for a given planned release.

## Adding the Staging Source to NuGet Feed
Edit your NuGet.Config file `~/.nuget/NuGet/NuGet.Config` (or possibly another location if Windows) and add this line within the `<packageSources>` element
```
<add key="nuget-sila" value="https://nexus.unitelabs.ch/repository/nuget-sila-development" protocolVersion="2" />
```

Now go to any project and add the SiLA library as a dependency:

```
dotnet add package Sila2.<version-number>-master-<commit-id>
```

Or you can add the latest master build of a given planned version release:
```
dotnet add package Sila2.<version-number>-master
```

**NOTE**: this approach is only reccomended if you want the most up to date version of a planned version release! As it may break compatibility on every update to master.

## Testing the NuGet Package Locally
If you would like to test the package locally before a release here are the steps you will need to follow

1. Create a local directory to release your nuget packages into e.g.
  
```bash
mkdir -p ~/.nuget/NuGet.Local
```

2. Add `~/.nuget/NuGet.Local` to the list of NuGet feeds by adding the following line to 
your `NuGet.Config` inside the `<packageSources>` tag:

```bash
<add key="Local" value="/home/unitelabs/.nuget/NuGet.local/" />
```

3. Build and release the `Sila2` package into the local feed

```bash
cd library/Sila2
dotnet build
dotnet nuget push bin/Debug/Sila2.<version>.nupkg -s ~/.nuget/NuGet.Local
```

5. Clear the NuGet cache

```bash
rm -rf ~/.nuget/packages/sila2/*
```

4. Go to the application testing the new `Sila2` release

```bash
cd <some-test-app>
dotnet add Sila2
dotnet restore
dotnet run
```
