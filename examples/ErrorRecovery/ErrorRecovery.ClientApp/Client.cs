﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Common.Logging;
using Grpc.Core;
using Sila2.Utils;

namespace Sila2.Examples.ErrorRecovery.ClientApp
{
    using RecoverableErrorProvider = De.Equicon.Examples.Recoverableerrorprovider.V1;
    using SiLAFramework = Org.Silastandard;
    using ErrorRecovery = Org.Silastandard.Core.Errorrecoveryservice.V1;

    internal class Client
    {
        #region Instances of the gRPC clients for the implemented features (generated by the proto compiler)

        private readonly RecoverableErrorProvider.RecoverableErrorProvider.RecoverableErrorProviderClient _recoverableErrorProviderClient;
        private readonly ErrorRecovery.ErrorRecoveryService.ErrorRecoveryServiceClient _errorRecoveryClient;

        #endregion

        private static readonly ILog Log = LogManager.GetLogger<Client>();

        private bool _errorSubscriptionRunning;
        private CancellationTokenSource _cancellationToken;
        private CancellationTokenSource _moveNextCancellationToken;

        public Client(Channel channel)
        {
            _recoverableErrorProviderClient = new RecoverableErrorProvider.RecoverableErrorProvider.RecoverableErrorProviderClient(channel);
            _errorRecoveryClient = new ErrorRecovery.ErrorRecoveryService.ErrorRecoveryServiceClient(channel);
        }

        public bool HandleErrorManually { private get; set; }

        #region ErrorRecoveryService

        public void SetErrorHandlingTimeout(int timeout)
        {
            _errorRecoveryClient.SetErrorHandlingTimeout(new ErrorRecovery.SetErrorHandlingTimeout_Parameters { ErrorHandlingTimeout = new SiLAFramework.Integer { Value = timeout } });
        }

        public async Task StartSubscribingErrors()
        {
            try
            {
                Task.Run(async () =>
                {
                    _cancellationToken = new CancellationTokenSource();
                    _moveNextCancellationToken = new CancellationTokenSource();
                    var responseStream = _errorRecoveryClient.Subscribe_RecoverableErrors(new ErrorRecovery.Subscribe_RecoverableErrors_Parameters(), Metadata.Empty, null, _cancellationToken.Token).ResponseStream;
                    while (!_moveNextCancellationToken.IsCancellationRequested && await responseStream.MoveNext(_moveNextCancellationToken.Token))
                    {
                        if (!_errorSubscriptionRunning)
                        {
                            _errorSubscriptionRunning = true;
                            Log.Info("Error subscription started");
                        }

                        ConsoleOutput(GetCurrentErrorsString(responseStream.Current.RecoverableErrors), true, ConsoleColor.DarkYellow);

                        // handle the error by reading the user input or wait for automatically handling (async)
                        DoErrorHandling(responseStream.Current);
                    }
                });

                while (!_errorSubscriptionRunning)
                {
                    await Task.Delay(200);
                }
            }
            catch (RpcException e)
            {
                if (e.Status.StatusCode == StatusCode.Cancelled)
                {
                    // CANCELED exception is not handled here because cancellation has been required
                    Log.Info("Error subscription canceled");
                    return;
                }
                Log.Error(ErrorHandling.HandleException(e));
            }
        }

        public void StopSubscribingErrors()
        {
            try
            {
                if (_errorSubscriptionRunning)
                {
                    _errorSubscriptionRunning = false;
                    if (_moveNextCancellationToken != null)
                    {
                        Log.Debug("Canceling error subscription (MoveNext) ...");
                        _moveNextCancellationToken.Cancel();
                        _moveNextCancellationToken = null;


                        if (_cancellationToken != null)
                        {
                            Log.Debug("Canceling error subscription (Stream) ...");
                            _cancellationToken.Cancel();
                            _cancellationToken = null;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error("Exception while canceling error subscription: " + e.Message);
            }
        }

        #endregion

        #region RecoverableErrorProvider commands

        public async Task RaiseRecoverableError(string errorMessage, int durationTillError, int automaticExecutionTimeout)
        {
            var methodName = new System.Diagnostics.StackTrace().GetFrame(2).GetMethod().Name;

            try
            {
                // start command
                var cmdId = _recoverableErrorProviderClient.RaiseRecoverableError(new RecoverableErrorProvider.RaiseRecoverableError_Parameters
                {
                    ErrorMessage = new SiLAFramework.String { Value = errorMessage },
                    DurationTillError = new SiLAFramework.Integer { Value = durationTillError },
                    AutomaticExecutionTimeout = new SiLAFramework.Integer { Value = automaticExecutionTimeout }
                }).CommandExecutionUUID;
                Log.Debug($"{methodName} command started (id = {cmdId.Value}) ...");

                // wait for command execution to finish
                using (var call = _recoverableErrorProviderClient.RaiseRecoverableError_Info(cmdId))
                {
                    var responseStream = call.ResponseStream;
                    while (await responseStream.MoveNext())
                    {
                        var currentExecutionInfo = responseStream.Current;
                        if (currentExecutionInfo.CommandStatus == SiLAFramework.ExecutionInfo.Types.CommandStatus.FinishedSuccessfully || currentExecutionInfo.CommandStatus == SiLAFramework.ExecutionInfo.Types.CommandStatus.FinishedWithError)
                        {
                            break;
                        }
                    }
                }

                // get and log response
                var response = _recoverableErrorProviderClient.RaiseRecoverableError_Result(cmdId);
                Log.Info("Response: " + response.ErrorHandlingResult.Value);
            }
            catch (RpcException e)
            {
                Log.Error(ErrorHandling.HandleException(e));
            }
        }

        #endregion

        #region Helper methods

        /// <summary>
        /// Handles the first error of the RecoverableErrors property value by reading the user input (continuation option) from the console.
        /// If no recoverable error is contained, cancel a potentially pending  reading request in case the error has been handled automatically meanwhile.
        /// </summary>
        /// <param name="response">The current value of the RecoverableErrors property</param>
        private void DoErrorHandling(ErrorRecovery.Subscribe_RecoverableErrors_Responses response)
        {
            if (response.RecoverableErrors.Count == 0)
            {
                // no errors to be handled
                return;
            }

            // handle first error
            var recoverableError = response.RecoverableErrors[0].RecoverableError;

            // write error text
            ConsoleOutput($"Recoverable error \"{recoverableError.ErrorMessage.Value}\" in command {recoverableError.CommandIdentifier.Value} (id={recoverableError.CommandExecutionUUID.Value})", true, ConsoleColor.Red);
            var s = new StringBuilder("  available continuation options:\n");
            var number = 0;
            foreach (var co in recoverableError.ContinuationOptions)
            {
                s.AppendLine($"  {++number} - [{co.ContinuationOption.Identifier.Value}] {(recoverableError.DefaultOption.Value == co.ContinuationOption.Identifier.Value && recoverableError.AutomaticExecutionTimeout.Value > 0 ? $"(DEFAULT, {recoverableError.AutomaticExecutionTimeout.Value} s)" : string.Empty)} '{co.ContinuationOption.Description.Value}'");
            }

            if (!HandleErrorManually)
            {
                ConsoleOutput(s + "Waiting instead of selecting a continuation option for demonstrating automatic handling ...", true, ConsoleColor.Cyan);
                return;
            }

            // read user input
            var selectedOption = string.Empty;
            do
            {
                s.Append("Select continuation option number: ");
                ConsoleOutput(s.ToString(), false, ConsoleColor.Cyan);

                // read user input from console
                var key = Console.ReadKey();
                Console.WriteLine("");

                if (!int.TryParse(key.KeyChar.ToString(), out var selectedOptionNumber))
                {
                    ConsoleOutput("Entered key is not a number!", true, ConsoleColor.Red);
                    continue;
                }

                if (selectedOptionNumber < 1 || selectedOptionNumber > recoverableError.ContinuationOptions.Count)
                {
                    ConsoleOutput($"Invalid option number! (must be between 1 and {recoverableError.ContinuationOptions.Count})", true, ConsoleColor.Red);
                    continue;
                }

                selectedOption = recoverableError.ContinuationOptions[selectedOptionNumber - 1].ContinuationOption.Identifier.Value;
            } while (string.IsNullOrEmpty(selectedOption));

            // handle error
            Log.Info($"Sending continuation option '{selectedOption}' ...");
            var handleErrorResponse = _errorRecoveryClient.ExecuteContinuationOption(
                new ErrorRecovery.ExecuteContinuationOption_Parameters
                {
                    CommandExecutionUUID = recoverableError.CommandExecutionUUID,
                    ContinuationOption = new SiLAFramework.String { Value = selectedOption }
                });
        }

        private static string GetCurrentErrorsString(Google.Protobuf.Collections.RepeatedField<ErrorRecovery.DataType_RecoverableError> errors)
        {
            var s = new StringBuilder($"\n[error subscription] {(errors.Count == 0 ? "no" : errors.Count.ToString())} unhandled recoverable error{(errors.Count == 1 ? string.Empty : "s")}\n");
            foreach (var e in errors)
            {
                s.AppendLine($"  * '{e.RecoverableError.ErrorMessage.Value}' in command {e.RecoverableError.CommandIdentifier.Value} (id={e.RecoverableError.CommandExecutionUUID.Value})");
            }

            return s.ToString();
        }

        public static void ConsoleOutput(string output, bool newLine, ConsoleColor color = ConsoleColor.Gray)
        {
            var previousColor = Console.ForegroundColor;
            Console.ForegroundColor = color;
            Console.Write(output + (newLine ? "\n" : string.Empty));
            Console.ForegroundColor = previousColor;
        }

        #endregion
    }
}
