﻿namespace Sila2.Examples.ErrorRecovery.ClientApp
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Threading;
    using CommandLine;
    using Common.Logging;
    using Grpc.Core;
    using Discovery;
    using Utils;

    public class Program
    {
        static void Main(string[] args)
        {
            Logging.SetupCommonLogging();
            var log = LogManager.GetLogger<Program>();
            try
            {
                Channel channel;
                ClientCmdLineArgs options = null;
                Parser.Default.ParseArguments<ClientCmdLineArgs>(args)
                    .WithParsed(o => options = o)
                    .WithNotParsed((errs) => ClientCmdLineArgs.HandleParseError(errs));
                var clientCredentials = options.Encryption != null
                    ? Encryption.CreateSslCredentials(options.Encryption)
                    : null;
                if (options.IpAddress == null && options.Port == -1)
                {
                    const string namePattern = "Error Recovery Server";
                    var server = SiLADiscovery.GetServers(4000, clientCredentials).FirstOrDefault(s => s.Config.Name == namePattern);
                    if (server == null)
                    {
                        throw new InvalidOperationException($"No server named: {namePattern} found");
                    }
                    channel = server.Channel;
                }
                else
                {
                    channel = ServiceFinder.BuildNewChannel(IPAddress.Parse(options.IpAddress), options.Port, clientCredentials);
                }

                var client = new Client(channel);
                Console.WriteLine($"Client created using channel {channel.Target}");

                Console.WriteLine($"\nCalling \"Subscribe_RecoverableErrors\" ...");
                client.StartSubscribingErrors().Wait();

                // raising an error after 3s without automatic execution
                int preErrorDuration = 3;
                int automaticExecutionTimeout = 0;
                client.HandleErrorManually = true;
                Console.WriteLine("\n-----------------------------------------------------------------------------------------------------------------------");
                Console.WriteLine($"Calling \"RaiseRecoverableError (after {preErrorDuration}s, automaticExecutionTimeout = {automaticExecutionTimeout}s)\" ...");
                client.RaiseRecoverableError("Test error to be handled manually", preErrorDuration, automaticExecutionTimeout).Wait();

                Thread.Sleep(1000);

                // raising an error after 3s with automatic execution after 1s
                automaticExecutionTimeout = 1;
                client.HandleErrorManually = false;
                Console.WriteLine("\n-----------------------------------------------------------------------------------------------------------------------");
                Console.WriteLine($"Calling \"RaiseRecoverableError (after {preErrorDuration}s, automaticExecutionTimeout = {automaticExecutionTimeout}s)\" ...");
                client.RaiseRecoverableError($"Test error with automatic handling after {automaticExecutionTimeout}s", preErrorDuration, automaticExecutionTimeout).Wait();

                Thread.Sleep(1000);

                // set error handling timeout to 8s and raise an error without automatic error handling
                const int timeout = 8;
                Console.WriteLine("\n-----------------------------------------------------------------------------------------------------------------------");
                Console.WriteLine($"Calling \"SetErrorHandlingTimeout({timeout}s)\" ...");
                client.SetErrorHandlingTimeout(timeout);

                Thread.Sleep(1000);

                automaticExecutionTimeout = 0;
                Console.WriteLine("\n-----------------------------------------------------------------------------------------------------------------------");
                Console.WriteLine($"Calling \"RaiseRecoverableError (after {preErrorDuration}s, automaticExecutionTimeout = {automaticExecutionTimeout}s)\" ...");
                client.RaiseRecoverableError("Test error without manual or automatic handling", preErrorDuration, automaticExecutionTimeout).Wait();

                Thread.Sleep(1000);

                client.StopSubscribingErrors();

                Thread.Sleep(1000);
                Console.WriteLine("Shutting down connection...");
                channel.ShutdownAsync().Wait();

                Console.WriteLine("Press any key to exit...");
                Console.ReadKey();
            }
            catch (Exception e)
            {

                log.Error("Client Error: ", e);
            }
        }
    }
}
