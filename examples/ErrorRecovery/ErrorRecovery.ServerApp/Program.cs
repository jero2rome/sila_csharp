using System;
using System.Net.NetworkInformation;
using Common.Logging;
using Sila2.Utils;

namespace Sila2.Examples.ErrorRecovery.ServerApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Logging.SetupCommonLogging();
            var log = LogManager.GetLogger<Server>();
            try
            {
                var options = CmdLineArgs.Parse<ServerCmdLineArgs>(args);

                NetworkInterface networkInterface = null;
                try
                {
                    // create SiLA2 Server
                    networkInterface = Networking.GetNetworkInterface(options.InterfaceNameOrCIDR);
                }
                catch (ArgumentNullException)
                {
                    // do nothing, network interface stays unassigned
                }
                var server = new Server(options.Port, networkInterface, options.ConfigFile, options.Credentials);
                server.StartServer();
                log.Info("Error Recovery Server listening on port " + options.Port);

                ServerCli.BlockUntilStop();
                server.ShutdownServer().Wait();
            }
            catch (Exception e)
            {
                log.Error("Server creation/initialization exception: ", e);
            }
        }
    }
}