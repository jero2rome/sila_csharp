using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Common.Logging;
using Grpc.Core;
using Sila2.De.Equicon.Examples.Recoverableerrorprovider.V1;
using Sila2.Utils;
using SiLAFramework = Sila2.Org.Silastandard;
using ErrorRecovery;

namespace Sila2.Examples.ErrorRecovery.ServerApp
{
    /// <summary>Implements the functionality of the RecoverableErrorProvider feature.</summary>
    internal class RecoverableErrorProviderImpl : RecoverableErrorProvider.RecoverableErrorProviderBase, IDisposable
    {
        #region Private memebers

        private static readonly ILog Log = LogManager.GetLogger<RecoverableErrorProviderImpl>();

        private readonly ObservableCommandManager<RaiseRecoverableError_Parameters, RaiseRecoverableError_Responses> _raiseRecoverableErrorCommandManager =
                new ObservableCommandManager<RaiseRecoverableError_Parameters, RaiseRecoverableError_Responses>(TimeSpan.FromMinutes(10));

        private readonly ErrorRecoveryServiceImpl _errorRecoveryService;

        private readonly Feature _silaFeature;

        private const string RepeatOptionIdentifier = "Repeat";
        private const string IgnoreOptionIdentifier = "Ignore";

        #endregion

        #region Constructors and destructors

        public RecoverableErrorProviderImpl(Feature silaFeature, ErrorRecoveryServiceImpl errorRecoveryService)
        {
            _silaFeature = silaFeature;
            _errorRecoveryService = errorRecoveryService;
        }

        #endregion

        #region Overrides of RecoverableErrorProviderBase

        #region Command 'RaiseRecoverableError'

        /// <summary>Triggers the RaiseRecoverableError command execution.</summary>
        public override async Task<SiLAFramework.CommandConfirmation> RaiseRecoverableError(RaiseRecoverableError_Parameters request, ServerCallContext context)
        {
            Validation.ValidateParameter(request.DurationTillError.Value, _silaFeature, "RaiseRecoverableError", "DurationTillError");
            Validation.ValidateParameter(request.AutomaticExecutionTimeout.Value, _silaFeature, "RaiseRecoverableError", "AutomaticExecutionTimeout");

            var command = await _raiseRecoverableErrorCommandManager.AddCommand(request, RaiseRecoverableErrorTask());

            return command.Confirmation;
        }

        /// <summary>Monitors the execution state of the command execution assigned to the given command execution ID.</summary>
        public override async Task RaiseRecoverableError_Info(SiLAFramework.CommandExecutionUUID request, IServerStreamWriter<SiLAFramework.ExecutionInfo> responseStream, ServerCallContext context)
        {
            try
            {
                await _raiseRecoverableErrorCommandManager.RegisterForInfo(request, responseStream, context.CancellationToken);
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw;
            }
        }

        /// <summary>Gets the result of the TemperatureControl command execution assigned to the given command execution ID.</summary>
        public override Task<RaiseRecoverableError_Responses> RaiseRecoverableError_Result(SiLAFramework.CommandExecutionUUID request, ServerCallContext context)
        {
            var command = _raiseRecoverableErrorCommandManager.GetCommand(request);
            return Task.FromResult(command.Result());
        }

        /// <summary>
        /// Does the observable command execution.
        /// - wait the specified duration
        /// - raise an recoverable error
        /// </summary>
        private Func<IProgress<SiLAFramework.ExecutionInfo>, RaiseRecoverableError_Parameters, Guid, CancellationToken, RaiseRecoverableError_Responses> RaiseRecoverableErrorTask()
        {
            return (progress, parameters, commandExecutionUUID, cancellationToken) =>
            {
                Log.Info("Starting observable command execution ...");

                var durationTillError = parameters.DurationTillError.Value;
                RecoverableError.ErrorHandlingState errorHandlingResult;
                RecoverableError recoverableError;

                do
                {
                    var executionInfo = new SiLAFramework.ExecutionInfo
                    {
                        ProgressInfo = new SiLAFramework.Real { Value = 0 },
                        EstimatedRemainingTime = new SiLAFramework.Duration()
                    };

                    // wait the specified time (report progress every second)
                    var startTime = DateTime.Now;
                    while ((DateTime.Now - startTime).TotalSeconds < durationTillError)
                    {
                        var elapsedSeconds = (DateTime.Now - startTime).TotalSeconds;
                        executionInfo.EstimatedRemainingTime.Seconds = durationTillError - (long)Math.Round(elapsedSeconds);
                        executionInfo.ProgressInfo.Value = elapsedSeconds / durationTillError;
                        progress.Report(executionInfo);
                        Thread.Sleep(200);
                    }

                    executionInfo.ProgressInfo = new SiLAFramework.Real { Value = 1 };
                    executionInfo.EstimatedRemainingTime = new SiLAFramework.Duration { Seconds = 0, Nanos = 0 };
                    progress.Report(executionInfo);

                    // raise recoverable error
                    recoverableError = new RecoverableError
                    {
                        CommandExecutionUUID = commandExecutionUUID,
                        FullyQualifiedCommandIdentifier = _silaFeature.GetFullyQualifiedCommandIdentifier("RaiseRecoverableError"),
                        ErrorMessage = parameters.ErrorMessage.Value,
                        ContinuationOptions = new List<RecoverableError.ContinuationOption>
                        {
                            new RecoverableError.ContinuationOption { Identifier = RepeatOptionIdentifier, Description = "Repeat executing the command" },
                            new RecoverableError.ContinuationOption { Identifier = IgnoreOptionIdentifier, Description = "Finish command execution by ignoring the error" },
                            new RecoverableError.ContinuationOption { Identifier = "OptionA", Description = "Recover the error by choosing option A" },
                            new RecoverableError.ContinuationOption { Identifier = "OptionB", Description = "Recover the error by choosing option B" },
                            new RecoverableError.ContinuationOption { Identifier = "OptionC", Description = "Recover the error by choosing option C" }
                        }
                    };

                    if (parameters.AutomaticExecutionTimeout.Value > 0)
                    {
                        // add default option and automatic execution timeout
                        recoverableError.DefaultOption = IgnoreOptionIdentifier;
                        recoverableError.AutomaticExecutionTimeout = TimeSpan.FromSeconds(parameters.AutomaticExecutionTimeout.Value);
                    }

                    Log.Error($"Recoverable error generated: \"{recoverableError.ErrorMessage}\"\nduring execution of command '{recoverableError.FullyQualifiedCommandIdentifier}' (id= {recoverableError.CommandExecutionUUID})");
                    errorHandlingResult = _errorRecoveryService.RaiseRecoverableError(ref recoverableError);

                } while (recoverableError.SelectedContinuationOption == RepeatOptionIdentifier);

                var resultMessage = $"Recoverable error {recoverableError.CommandExecutionUUID} has been ";
                switch (errorHandlingResult)
                {
                    case RecoverableError.ErrorHandlingState.ErrorHandled:
                        resultMessage += $"handled by choosing continuation option '{recoverableError.SelectedContinuationOption}'";
                        break;
                    case RecoverableError.ErrorHandlingState.ErrorHandledAutomatically:
                        resultMessage += $"automatically handled setting default continuation option '{recoverableError.SelectedContinuationOption}' after {recoverableError.AutomaticExecutionTimeout.TotalSeconds}s";
                        break;
                    case RecoverableError.ErrorHandlingState.Aborted:
                        resultMessage += "aborted by the client";
                        break;
                    case RecoverableError.ErrorHandlingState.ErrorHandlingTimeoutElapsed:
                        resultMessage += $"aborted automatically after {_errorRecoveryService.ErrorHandlingTimeout.TotalSeconds}s (error handling timeout)";
                        break;
                }

                return new RaiseRecoverableError_Responses { ErrorHandlingResult = new SiLAFramework.String { Value = resultMessage } };
            };
        }

        #endregion

        #endregion

        public void Dispose()
        {
            _raiseRecoverableErrorCommandManager.Dispose();
        }
    }
}