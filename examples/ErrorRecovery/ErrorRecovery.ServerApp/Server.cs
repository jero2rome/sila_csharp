﻿using System;
using System.Net.NetworkInformation;
using Common.Logging;
using ErrorRecovery;
using Grpc.Core;
using Sila2.Server;

namespace Sila2.Examples.ErrorRecovery.ServerApp
{
    /// <summary>
    /// Implements a SiLA 2 Server that provides a GreetingProvider and a TemperatureController feature.
    /// </summary>
    public class Server : SiLA2Server, IDisposable
    {
        private readonly ErrorRecoveryServiceImpl _errorRecoveryServiceImpl;
        private readonly RecoverableErrorProviderImpl _errorProviderImpl;
        private static ILog Log = LogManager.GetLogger<Server>();

        public Server(int portNumber, NetworkInterface networkInterface, string configFile, ServerCredentials credentials)
        : base(new ServerInformation(
                "Error Recovery Server",
                "An example server implementing the core feature 'ErrorRecoveryService' and the test feature 'RecoverableErrorProvider'",
                "www.equicon.de",
                "1.0"),
                portNumber,
                networkInterface,
                credentials,
                configFile)
        {
            // add ErrorRecoveryProvider feature implementation
            _errorRecoveryServiceImpl = new ErrorRecoveryServiceImpl(ReadFeature("features/ErrorRecoveryService.sila.xml"));
            GrpcServer.Services.Add(Org.Silastandard.Core.Errorrecoveryservice.V1.ErrorRecoveryService.BindService(_errorRecoveryServiceImpl));

            // add RecoverableErrorProvider feature implementation
            _errorProviderImpl = new RecoverableErrorProviderImpl(ReadFeature("features/RecoverableErrorProvider.sila.xml"), _errorRecoveryServiceImpl);
            GrpcServer.Services.Add(De.Equicon.Examples.Recoverableerrorprovider.V1.RecoverableErrorProvider.BindService(_errorProviderImpl));
        }

        public void Dispose()
        {
            _errorProviderImpl.Dispose();
        }
    }
}