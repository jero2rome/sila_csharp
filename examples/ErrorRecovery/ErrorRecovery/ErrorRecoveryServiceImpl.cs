﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Common.Logging;
using Grpc.Core;
using Sila2;
using Sila2.Org.Silastandard.Core.Errorrecoveryservice.V1;
using Sila2.Utils;
using Timer = System.Timers.Timer;

namespace ErrorRecovery
{
    public class ErrorRecoveryServiceImpl : ErrorRecoveryService.ErrorRecoveryServiceBase
    {
        #region Nested classes

        private class RecoverableErrorEx : RecoverableError
        {
            #region Events

            public delegate void StateChangedEventHandler(object sender, EventArgs eva);
            public event StateChangedEventHandler StateChangedEvent;

            #endregion

            #region constructors

            public RecoverableErrorEx(RecoverableError error)
            {
                FullyQualifiedCommandIdentifier = error.FullyQualifiedCommandIdentifier;
                CommandExecutionUUID = error.CommandExecutionUUID;
                ErrorMessage = error.ErrorMessage;
                ContinuationOptions = error.ContinuationOptions;
                DefaultOption = error.DefaultOption;
                AutomaticExecutionTimeout = error.AutomaticExecutionTimeout;

                // set automatic execution timer
                if (AutomaticExecutionTimeout.TotalSeconds > 0)
                {
                    var automaticExecutionTimer = new Timer(AutomaticExecutionTimeout.TotalSeconds * 1000);
                    automaticExecutionTimer.Elapsed += OnAutomaticExecutionTimerElapsed;
                    automaticExecutionTimer.AutoReset = false;
                    automaticExecutionTimer.Enabled = true;
                }

                State = ErrorHandlingState.Active;
                ErrorHandlingStarted = DateTime.Now;
            }

            #endregion

            #region Private members

            #endregion

            #region Properties

            public ErrorHandlingState State { get; private set; }

            public DateTime ErrorHandlingStarted { get; }

            #endregion

            #region Methods

            public void SetErrorHandlingState(ErrorHandlingState state)
            {
                if (State != state)
                {
                    State = state;
                    StateChangedEvent?.Invoke(this, EventArgs.Empty);
                }
            }

            private void OnAutomaticExecutionTimerElapsed(object source, System.Timers.ElapsedEventArgs e)
            {
                SelectedContinuationOption = DefaultOption;
                SetErrorHandlingState(ErrorHandlingState.ErrorHandledAutomatically);
            }

            #endregion
        }

        private class OccurredErrors
        {
            private readonly Dictionary<Guid, RecoverableErrorEx> _errors = new Dictionary<Guid, RecoverableErrorEx>();

            public IEnumerable<RecoverableError> UnhandledErrors
            {
                get
                {
                    lock (_errors)
                    {
                        return _errors.Values.Where(e => e.State == RecoverableError.ErrorHandlingState.Active);
                    }
                }
            }

            public RecoverableErrorEx AddError(RecoverableError error)
            {
                lock (_errors)
                {
                    _errors[error.CommandExecutionUUID] = new RecoverableErrorEx(error);
                    _errors[error.CommandExecutionUUID].StateChangedEvent += OnErrorStateChanged;

                    LastChange = DateTime.Now;
                    return GetError(error.CommandExecutionUUID);
                }
            }

            public RecoverableErrorEx GetError(Guid commandExecutionUUID)
            {
                lock (_errors)
                {
                    if (_errors.ContainsKey(commandExecutionUUID))
                    {
                        return _errors[commandExecutionUUID];
                    }
                }

                return null;
            }

            private void OnErrorStateChanged(object sender, EventArgs a)
            {
                if (((RecoverableErrorEx)sender).State != RecoverableError.ErrorHandlingState.Active)
                {
                    ((RecoverableErrorEx)sender).StateChangedEvent -= OnErrorStateChanged;
                }

                LastChange = DateTime.Now;
            }

            public DateTime LastChange = DateTime.Now;
        }

        #endregion

        #region Members

        private static readonly ILog Log = LogManager.GetLogger<ErrorRecoveryServiceImpl>();

        private readonly OccurredErrors _occurredErrors = new OccurredErrors();

        #endregion

        #region Properties

        private Feature SiLAFeature { get; }

        public TimeSpan ErrorHandlingTimeout { get; private set; }

        #endregion

        #region Constructor

        public ErrorRecoveryServiceImpl(Feature silaFeature)
        {
            SiLAFeature = silaFeature;
        }

        #endregion

        #region Overrides of ErrorRecoveryServiceBase

        public override Task<SetErrorHandlingTimeout_Responses> SetErrorHandlingTimeout(SetErrorHandlingTimeout_Parameters request, ServerCallContext context)
        {
            ErrorHandlingTimeout = TimeSpan.FromSeconds(request.ErrorHandlingTimeout.Value);

            return Task.FromResult(new SetErrorHandlingTimeout_Responses());
        }

        public override async Task Subscribe_RecoverableErrors(Subscribe_RecoverableErrors_Parameters request, IServerStreamWriter<Subscribe_RecoverableErrors_Responses> responseStream, ServerCallContext context)
        {
            var methodName = new System.Diagnostics.StackTrace().GetFrame(2).GetMethod().Name;
            Log.Debug($"\"{methodName}\" request received");

            // set to min value to force sending the response at least one time immediately after subscription
            DateTime lastResponseMessage = DateTime.MinValue;

            do
            {
                // check if errors state has changed since last response message
                if (_occurredErrors.LastChange > lastResponseMessage)
                {
                    // assemble and send response
                    var response = new Subscribe_RecoverableErrors_Responses();
                    foreach (var error in _occurredErrors.UnhandledErrors) { response.RecoverableErrors.Add(error.ToSiLAType()); }

                    lastResponseMessage = DateTime.Now;
                    Log.Info($"send new RecoverableErrors value with {_occurredErrors.UnhandledErrors.ToList().Count} unhandled errors ...");
                    await responseStream.WriteAsync(response);
                }

                await Task.Delay(500);
            } while (!context.CancellationToken.IsCancellationRequested);

            Log.Warn($"\"{methodName}\" subscription cancelled");
        }

        public override Task<ExecuteContinuationOption_Responses> ExecuteContinuationOption(ExecuteContinuationOption_Parameters request, ServerCallContext context)
        {
            // check command execution UUID for validity
            if (!Guid.TryParse(request.CommandExecutionUUID.Value, out var commandExecutionUUID))
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                    SiLAFeature.GetFullyQualifiedCommandParameterIdentifier("ExecuteContinuationOption", "CommandExecutionUUID"),
                    $"The given string'{request.CommandExecutionUUID.Value}' is not a valid command execution UUID"));
            }

            // check if specified command execution UUID belongs to an active error
            lock (_occurredErrors)
            {
                var recoverableError = _occurredErrors.GetError(commandExecutionUUID);

                if (recoverableError == null || recoverableError.State != RecoverableError.ErrorHandlingState.Active)
                {
                    RaiseInvalidCommandExecutionUUIDError(recoverableError);
                }

                // check if specified continuation option is valid
                if (recoverableError != null && recoverableError.ContinuationOptions.All(o => o.Identifier != request.ContinuationOption.Value))
                {
                    var error = SiLAFeature.GetDefinedExecutionErrors().Find(match => match.Identifier == "UnknownContinuationOption");
                    ErrorHandling.RaiseSiLAError(ErrorHandling.CreateDefinedExecutionError(SiLAFeature.GetFullyQualifiedDefinedExecutionErrorIdentifier(error.Identifier), error.Description));
                }

                recoverableError.SelectedContinuationOption = request.ContinuationOption.Value;
                recoverableError.InputData = request.InputData;
                recoverableError.SetErrorHandlingState(RecoverableError.ErrorHandlingState.ErrorHandled);
            }

            return Task.FromResult(new ExecuteContinuationOption_Responses());
        }

        public override Task<AbortErrorHandling_Responses> AbortErrorHandling(AbortErrorHandling_Parameters request, ServerCallContext context)
        {
            // check command execution UUID for validity
            if (!Guid.TryParse(request.CommandExecutionUUID.Value, out var commandExecutionUUID))
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                    SiLAFeature.GetFullyQualifiedCommandParameterIdentifier("ExecuteContinuationOption", "CommandExecutionUUID"),
                    $"The given string'{request.CommandExecutionUUID.Value}' is not a valid command execution UUID"));
            }

            // check if specified command execution UUID belongs to an active error
            lock (_occurredErrors)
            {
                var recoverableError = _occurredErrors.GetError(commandExecutionUUID);

                if (recoverableError == null || recoverableError.State != RecoverableError.ErrorHandlingState.Active)
                {
                    RaiseInvalidCommandExecutionUUIDError(recoverableError);
                }

                recoverableError.SetErrorHandlingState(RecoverableError.ErrorHandlingState.Aborted);
            }

            return Task.FromResult(new AbortErrorHandling_Responses());
        }

        #endregion

        /// <summary>
        /// Adds the given error to the list of active errors to be reported to subscribers.
        /// Waits until the error either has been handled (manually or automatically) or
        /// the error handling timeout has elapsed.
        /// </summary>
        /// <param name="error"></param>
        public RecoverableError.ErrorHandlingState RaiseRecoverableError(ref RecoverableError error)
        {
            var handledError = _occurredErrors.AddError(error);
            Log.Info($"Error added: {error.ToSiLAType()}");

            // wait for error to be handled
            while (handledError.State == RecoverableError.ErrorHandlingState.Active)
            {
                lock (_occurredErrors)
                {
                    // check error handling timeout
                    if (ErrorHandlingTimeout > new TimeSpan() && DateTime.Now - handledError.ErrorHandlingStarted > ErrorHandlingTimeout)
                    {
                        handledError.SetErrorHandlingState(RecoverableError.ErrorHandlingState.ErrorHandlingTimeoutElapsed);
                        break;
                    }
                }

                Thread.Sleep(500);
            }

            // enter selected option
            error.SelectedContinuationOption = handledError.SelectedContinuationOption;

            return handledError.State;
        }

        private void RaiseInvalidCommandExecutionUUIDError(RecoverableErrorEx recoverableError)
        {
            var error = SiLAFeature.GetDefinedExecutionErrors().Find(match => match.Identifier == "InvalidCommandExecutionUUID");
            var errorMessage = error.Description;
            if (recoverableError != null)
            {
                errorMessage = "The specified recoverable error has been ";
                switch (recoverableError.State)
                {
                    case RecoverableError.ErrorHandlingState.ErrorHandled:
                        errorMessage += $"handled by choosing continuation option '{recoverableError.SelectedContinuationOption}'";
                        break;
                    case RecoverableError.ErrorHandlingState.ErrorHandledAutomatically:
                        errorMessage += $"automatically handled setting default continuation option '{recoverableError.SelectedContinuationOption}' after {recoverableError.AutomaticExecutionTimeout.TotalSeconds} s";
                        break;
                    case RecoverableError.ErrorHandlingState.Aborted:
                        errorMessage += "aborted by the client";
                        break;
                    case RecoverableError.ErrorHandlingState.ErrorHandlingTimeoutElapsed:
                        errorMessage += $"aborted automatically after {ErrorHandlingTimeout.TotalSeconds} s (error handling timeout)";
                        break;
                }
            }

            ErrorHandling.RaiseSiLAError(ErrorHandling.CreateDefinedExecutionError(SiLAFeature.GetFullyQualifiedDefinedExecutionErrorIdentifier(error.Identifier), errorMessage));
        }
    }
}
