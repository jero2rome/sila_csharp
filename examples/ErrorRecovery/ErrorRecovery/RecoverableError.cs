﻿using System;
using System.Collections.Generic;
using Sila2.Org.Silastandard.Core.Errorrecoveryservice.V1;
using SiLAFramework = Sila2.Org.Silastandard;

namespace ErrorRecovery
{
    public class RecoverableError
    {
        public enum ErrorHandlingState
        {
            Active = 0,
            ErrorHandled,
            ErrorHandledAutomatically,
            Aborted,
            ErrorHandlingTimeoutElapsed
        }

        #region Nested classes
        
        public class ContinuationOption
        {
            public ContinuationOption()
            {
                Identifier = Description = RequiredInputData = string.Empty;
            }

            public string Identifier { get; set; }

            public string Description { get; set; }

            public string RequiredInputData { get; set; }

            public DataType_ContinuationOption ToSiLAType()
            {
                return new DataType_ContinuationOption
                {
                    ContinuationOption = new DataType_ContinuationOption.Types.ContinuationOption_Struct
                    {
                        Identifier = new SiLAFramework.String { Value = this.Identifier },
                        Description = new SiLAFramework.String { Value = this.Description },
                        RequiredInputData = new SiLAFramework.String { Value = this.RequiredInputData }
                    }
                };
            }
        }

        #endregion

        public RecoverableError()
        {
            FullyQualifiedCommandIdentifier = ErrorMessage = DefaultOption = string.Empty;
        }

        public string FullyQualifiedCommandIdentifier { get; set; }

        public Guid CommandExecutionUUID { get; set; }

        public string ErrorMessage { get; set; }

        public List<ContinuationOption> ContinuationOptions { get; set; }

        public string DefaultOption { get; set; }

        public TimeSpan AutomaticExecutionTimeout { get; set; }

        /// <summary>Will be set by the ErrorRecoveryService</summary>
        public string SelectedContinuationOption { get; set; }

        /// <summary>Will be set by the ErrorRecoveryService</summary>
        public SiLAFramework.Any InputData { get; set; }

        public DataType_RecoverableError ToSiLAType()
        {
            var result = new DataType_RecoverableError
            {
                RecoverableError = new DataType_RecoverableError.Types.RecoverableError_Struct
                {
                    CommandIdentifier = new SiLAFramework.String { Value = this.FullyQualifiedCommandIdentifier },
                    CommandExecutionUUID = new SiLAFramework.String { Value = this.CommandExecutionUUID.ToString() },
                    ErrorMessage = new SiLAFramework.String { Value = this.ErrorMessage },
                    DefaultOption = new SiLAFramework.String { Value = this.DefaultOption },
                    AutomaticExecutionTimeout = new SiLAFramework.Integer { Value = (long)this.AutomaticExecutionTimeout.TotalSeconds },
                }
            };

            foreach (var option in this.ContinuationOptions)
            {
                result.RecoverableError.ContinuationOptions.Add(option.ToSiLAType());
            }

            return result;
        }
    }
}
