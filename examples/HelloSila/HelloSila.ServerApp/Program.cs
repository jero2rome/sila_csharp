namespace Sila2.Examples.HelloSila.ServerApp
{
    using System;
    using System.Threading;
    using System.Net.NetworkInformation;
    using Common.Logging;
    using Utils;

    public class Program
    {
        public static void Main(string[] args)
        {
            Logging.SetupCommonLogging();
            var log = LogManager.GetLogger<Server>();
            try
            {
                var options = CmdLineArgs.Parse<ServerCmdLineArgs>(args);

                Server server;
                NetworkInterface networkInterface = null;
                try
                {
                    // create SiLA2 Server
                    networkInterface = Networking.GetNetworkInterface(options.InterfaceNameOrCIDR);
                }
                catch (ArgumentNullException)
                {
                    // do nothing, network interface stays unassigned
                }
                server = new Server(options.Port, networkInterface, options.ConfigFile, options.Credentials);
                server.StartServer();
                log.Info("HelloSila Server listening on port " + options.Port);

                ServerCli.BlockUntilStop();
                server.ShutdownServer().Wait();
            }
            catch (Exception e)
            {
                log.Error("Server creation/initialization exception: ", e);
            }
        }
    }
}