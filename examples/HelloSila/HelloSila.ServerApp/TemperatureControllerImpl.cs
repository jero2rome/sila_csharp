using Sila2.Utils;

namespace Sila2.Examples.HelloSila.ServerApp
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Common.Logging;
    using Grpc.Core;
    using Sila2.Server;
    using SiLAFramework = Org.Silastandard;
    using Org.Silastandard.Examples.Temperaturecontroller.V1;

    /// <summary>
    /// Implements the functionality of the TemperatureControl feature.
    /// </summary>
    internal class TemperatureControllerImpl : TemperatureController.TemperatureControllerBase, IDisposable
    {
        #region Private memebers

        private static readonly ILog Log = LogManager.GetLogger<TemperatureControllerImpl>();
        private const string ControlTemperatureCmdId = "ControlTemperature";

        private readonly ObservableCommandManager<ControlTemperature_Parameters, ControlTemperature_Responses>
            _controlTemperatureCommandManager =
                new ObservableCommandManager<ControlTemperature_Parameters, ControlTemperature_Responses>(
                    TimeSpan.FromMinutes(1));
        private readonly TimeSpan _executionTime = TimeSpan.FromSeconds(100);
        private readonly ThermostatSimulator _thermostat = new ThermostatSimulator();

        public GreetingProviderImpl GreetingProviderImpl { private get; set; }
        public SiLA2Server SiLAServer { private get; set; }
        public Feature SiLAFeature { private get; set; }

        #endregion

        #region Constructors and destructors

        public TemperatureControllerImpl()
        {

        }

        #endregion

        #region Overrides of TemperatureControllerBase

        #region Command 'ControlTemperature'

        /// <summary>Triggers the TemperatureControl command execution.</summary>
        public override async Task<SiLAFramework.CommandConfirmation> ControlTemperature(
            ControlTemperature_Parameters request, ServerCallContext context)
        {
            Validation.ValidateParameter(request.TargetTemperature.Value, SiLAFeature, "ControlTemperature", "TargetTemperature");
            var command = await _controlTemperatureCommandManager.AddCommand(request, ControlTemperatureTask());
            return command.Confirmation;
        }

        /// <summary>Monitors the execution state of the command execution assigned to the given command execution ID.</summary>
        public override async Task ControlTemperature_Info(SiLAFramework.CommandExecutionUUID request,
            IServerStreamWriter<SiLAFramework.ExecutionInfo> responseStream, ServerCallContext context)
        {
            try
            {
                await _controlTemperatureCommandManager.RegisterForInfo(request, responseStream, context.CancellationToken);
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw;
            }
        }

        /// <summary>Gets the result of the TemperatureControl command execution assigned to the given command execution ID.</summary>
        public override Task<ControlTemperature_Responses> ControlTemperature_Result(
            SiLAFramework.CommandExecutionUUID request, ServerCallContext context)
        {
            var command = _controlTemperatureCommandManager.GetCommand(request);
            return Task.FromResult(command.Result());
        }

        #endregion

        #region Property 'CurrentTemperature'

        /// <summary>
        /// Returns the current temperature with a frequency of 1 Hz.
        /// </summary>
        /// <param name="request">The gRPC parameters (empty)</param>
        /// <param name="responseStream">The response stream.</param>
        /// <param name="context">The call context (not used here).</param>
        /// <returns></returns>
        public override async Task Subscribe_CurrentTemperature(Subscribe_CurrentTemperature_Parameters request,
            IServerStreamWriter<Subscribe_CurrentTemperature_Responses> responseStream, ServerCallContext context)
        {
            var methodName = new System.Diagnostics.StackTrace().GetFrame(2).GetMethod().Name;
            Log.Debug($"\n\"{methodName}\" request received");

            do
            {
                await responseStream.WriteAsync(new Subscribe_CurrentTemperature_Responses
                {
                    CurrentTemperature = new SiLAFramework.Real
                    {
                        Value = _thermostat.CurrentTemperature
                    }
                });
                await Task.Delay(1000);
            } while (!context.CancellationToken.IsCancellationRequested);

            Log.Warn($"\n\"{methodName}\" subscription cancelled");
        }

        #endregion

        #endregion

        /// <summary>
        /// Does the temperature control:
        ///  - sets the required target temperature
        ///  - waits for target temperature has been reached or an error occured meanwhile
        ///  - sets the command execution state assigned to the given command execution ID
        /// </summary>
        /// <param name="executionID">The ID of the command execution.</param>
        /// <param name="targetTemperature">The target temperature to be reached.</param>
        // set new target temperature
        private Func<IProgress<SiLAFramework.ExecutionInfo>, ControlTemperature_Parameters, CancellationToken,
            ControlTemperature_Responses> ControlTemperatureTask()
        {
            return (progress, parameters, cancellationToken) =>
            {
                Log.Info("Starting to control the temperature");
                var targetTemperature = parameters.TargetTemperature.Value;
                var executionInfo = new SiLAFramework.ExecutionInfo
                {
                    ProgressInfo = new SiLAFramework.Real {Value = 0},
                    EstimatedRemainingTime = new SiLAFramework.Duration()
                };
                // Initialize
                progress.Report(executionInfo);

                // persist start temperature
                var startTemperature = _thermostat.CurrentTemperature;

                var taskThread = new Thread(() =>
                {
                    _thermostat.SetTargetTemperature(targetTemperature, cancellationToken);
                });
                
                taskThread.Start();
                UpdateExecutionInfo(taskThread, executionInfo, startTemperature, progress);

                executionInfo.ProgressInfo = new SiLAFramework.Real
                {
                    Value = 1
                };
                executionInfo.EstimatedRemainingTime = new SiLAFramework.Duration
                {
                    Seconds = 0,
                    Nanos = 0
                };
                progress.Report(executionInfo);

                return new ControlTemperature_Responses();
            };
        }

        private void UpdateExecutionInfo(Thread taskThread, SiLAFramework.ExecutionInfo executionInfo,
            double startTemperature, IProgress<SiLAFramework.ExecutionInfo> progress)
        {
            while (taskThread.IsAlive)
            {
                progress.Report(executionInfo);
                Thread.Sleep(500);
                executionInfo.ProgressInfo.Value = Math.Abs(_thermostat.CurrentTemperature - startTemperature) /
                                                   Math.Abs(_thermostat.TargetTemperature - startTemperature);

                var absoluteTemperatureDifference =
                    Math.Abs(_thermostat.TargetTemperature - _thermostat.CurrentTemperature);
                var remainingTime = absoluteTemperatureDifference / ThermostatSimulator.KELVIN_PER_SECONDS;
                executionInfo.EstimatedRemainingTime = new SiLAFramework.Duration
                {
                    Seconds = (long) remainingTime,
                    Nanos = (int) ((remainingTime - Math.Floor(remainingTime)) * Math.Pow(10, 9))
                };
            }

            executionInfo.CommandStatus = SiLAFramework.ExecutionInfo.Types.CommandStatus.FinishedSuccessfully;
            progress.Report(executionInfo);
        }

        public void Dispose()
        {
            _controlTemperatureCommandManager.Dispose();
        }
    }
}