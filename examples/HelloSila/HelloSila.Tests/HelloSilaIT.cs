namespace Sila2.Examples.HelloSila.Tests
{
    using System;
    using System.Threading.Tasks;
    using System.Linq;
    using System.Collections.Generic;
    using Grpc.Core;
    using ClientApp;
    using Discovery;
    using Utils;
    using Xunit;
    using Xunit.Abstractions;
    using Sila2.Tests.Common;

    /// <summary>
    /// Fixture class to hold any resources to be shared between tests
    /// e.g. Server and Client
    /// </summary>
    public class ClientServerFixture : IDisposable, IAsyncLifetime
    {
        public readonly Client client;
        public const int ServerPort = 50052;
        private readonly ServerApp.Server server;
        private readonly Channel channel;

        public ClientServerFixture()
        {
            GrpcConfigurator.EnableDebugTrace();
            //var serverCredentials = GrpcConfigurator.CreateTestServerCredentials();
            var networkInterface = Networking.FindValidNetworkInterface();
            server = new ServerApp.Server(ServerPort, networkInterface, null, ServerCredentials.Insecure);
            server.StartServer();
            //var channelOptions = new List<ChannelOption>
            //{
            //    new ChannelOption(ChannelOptions.SslTargetNameOverride, "localhost")
            //};
            //var clientCredentials = GrpcConfigurator.CreateTestSslCredentials();
            //channel = SiLADiscovery.GetServers(4000, clientCredentials, channelOptions).First(server => server.Config.Name == "HelloSila Server").Channel;
            channel = SiLADiscovery.GetServers(4000).First(server => server.Config.Name == "HelloSila Server").Channel;
            client = new Client(channel);
        }

        /// <summary>
        /// TODO: seems like this disposal occurs but the sockets are still open
        ///       however when running the clients this is not quite the case. Kind of weird.
        ///       See: https://github.com/xunit/xunit/issues/1066
        /// </summary>
        public void Dispose()
        {
        }

        public Task InitializeAsync()
        {
            return Task.CompletedTask;
        }

        public Task DisposeAsync()
        {
            return Task.WhenAll(server.ShutdownServer(), channel.ShutdownAsync());
        }
    }

    /// <summary>
    /// Integration test class to validate functionality of MockShaker server
    /// using client to drive interaction.
    /// </summary>
    public class HelloSilaIT : IClassFixture<ClientServerFixture>
    {
        private readonly ITestOutputHelper output;
        private readonly ClientServerFixture fixture;

        public HelloSilaIT(ITestOutputHelper output, ClientServerFixture fixture)
        {
            this.output = output;
            this.fixture = fixture;
            Sila2.Utils.Logging.SetupCommonLogging();
            Sila2.Tests.Common.Logging.SetupLogForwarding(this.output);
        }

        [Fact]
        public void TestSayHello()
        {
            var response = fixture.client.SayHello("SiLA");

            Assert.Equal("Hello SiLA", response);
        }

        [Fact]
        public void TestGetCurrentYear()
        {
            var currentYear = fixture.client.GetStartYear();
            Assert.Equal(DateTime.Now.Year, currentYear);
        }

        [Fact]
        public async void TestControlTemperature()
        {
            fixture.client.SayHello("SiLA");
            var expectedTemperature = UnitConverter.DegreeCelsius2Kelvin(45);
            var error = 1;
            fixture.client.ControlTemperature(expectedTemperature).Wait();
            var actualTemperature = await fixture.client.GetCurrentTemperature(1);
            Assert.InRange(actualTemperature, expectedTemperature - error, actualTemperature + error);
        }
    }
}