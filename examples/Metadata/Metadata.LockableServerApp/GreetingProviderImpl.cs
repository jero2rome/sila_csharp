﻿namespace Sila2.Examples.Metadata.LockableServerApp
{
    using System.Threading.Tasks;
    using Grpc.Core;
    using Org.Silastandard.Examples.Greetingprovider.V1;
    using SiLAFramework = Org.Silastandard;

    /// <summary>
    /// Implements the functionality of the GreetingsProviderImpl feature.
    /// </summary>
    internal class GreetingProviderImpl : GreetingProvider.GreetingProviderBase
    {
        /// <summary>Gets or sets the start year of the server</summary>
        public int ServerStartYear { private get; set; }

        public LockControllerImpl LockController { private get; set; }

        #region Overrides of GreetingProviderBase

        /// <summary>Answers the SayHello RPC by returning a greeting message.</summary>
        /// <param name="request">The call parameters containing the Name parameter.</param>
        /// <param name="context">The call context (not used here).</param>
        /// <returns>The call response containing the Greeting field.</returns>
        public override Task<SayHello_Responses> SayHello(SayHello_Parameters request, ServerCallContext context)
        {
            // check if server is locked and (if so) if the lock identifier is valid
            LockController.CheckLock(context.RequestHeaders);

            return Task.FromResult(new SayHello_Responses { Greeting = new SiLAFramework.String { Value = "Hello " + request.Name.Value } });
        }

        /// <summary>
        /// Receives the unobservable property RPC and returns the current year as a field of the call response.
        /// </summary>
        /// <param name="request">The call parameters (empty for properties).</param>
        /// <param name="context">The call context (not used here).</param>
        /// <returns>The call response containing the CurrentYear field.</returns>
        public override Task<Get_StartYear_Responses> Get_StartYear(Get_StartYear_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_StartYear_Responses { StartYear = new SiLAFramework.Integer { Value = ServerStartYear } });
        }

        #endregion
    }
}