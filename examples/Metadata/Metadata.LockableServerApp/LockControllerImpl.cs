﻿namespace Sila2.Examples.Metadata.LockableServerApp
{
    using System;
    using System.Threading.Tasks;
    using System.Timers;
    using Common.Logging;
    using Grpc.Core;
    using Org.Silastandard;
    using Org.Silastandard.Core.Lockcontroller.V1;
    using Sila2.Server;
    using Utils;
    using Boolean = Org.Silastandard.Boolean;
    using String = Org.Silastandard.String;

    internal class LockControllerImpl : LockController.LockControllerBase
    {
        #region Properties

        public Feature LockableFeature { private get; set; }

        public Feature SiLAFeature { private get; set; }

        #endregion

        #region Private members

        private static readonly ILog Log = LogManager.GetLogger<LockControllerImpl>();

        private string _currentLockIdentifier = string.Empty;

        private TimeSpan _lockTimeout;

        private readonly Timer _lockTimeoutTimer;

        private bool IsServerLocked => !string.IsNullOrEmpty(_currentLockIdentifier);

        #endregion

        #region Constructors and destructors

        public LockControllerImpl()
        {
            // create lockout timeout timer
            _lockTimeoutTimer = new Timer
            {
                AutoReset = false,
                Enabled = false
            };

            _lockTimeoutTimer.Elapsed += OnLockTimeoutExpired;
        }

        #endregion

        #region Overrides of LockControllerBase

        public override Task<Get_FCPAffectedByMetadata_LockIdentifier_Responses> Get_FCPAffectedByMetadata_LockIdentifier(Get_FCPAffectedByMetadata_LockIdentifier_Parameters request, ServerCallContext context)
        {
            var sayHelloCmd = LockableFeature.GetDefinedCommands().Find(match => match.Identifier == "SayHello");

            Get_FCPAffectedByMetadata_LockIdentifier_Responses response = new Get_FCPAffectedByMetadata_LockIdentifier_Responses
            {
                AffectedCalls =
                {
                    new String {Value = LockableFeature.GetFullyQualifiedCommandIdentifier(sayHelloCmd.Identifier)}
                }
            };

            return Task.FromResult(response);
        }

        public override Task<Get_IsLocked_Responses> Get_IsLocked(Get_IsLocked_Parameters request, ServerCallContext context)
        {
            return Task.FromResult(new Get_IsLocked_Responses { IsLocked = new Boolean { Value = IsServerLocked } });
        }

        public override Task<LockServer_Responses> LockServer(LockServer_Parameters request, ServerCallContext context)
        {
            // check if server is alreday locked
            if (IsServerLocked)
            {
                // raise defined execution error
                var error = SiLAFeature.GetDefinedExecutionErrors().Find(match => match.Identifier == "ServerAlreadyLocked");
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateDefinedExecutionError(SiLAFeature.GetFullyQualifiedDefinedExecutionErrorIdentifier(error.Identifier), error.Description));
            }

            _currentLockIdentifier = request.LockIdentifier.Value;

            // if timeout has been set: setup and start lock timeout timer
            _lockTimeout = new TimeSpan(0, 0, (int)request.Timeout.Value);
            if (_lockTimeout.TotalSeconds > 0)
            {
                _lockTimeoutTimer.Interval = _lockTimeout.TotalMilliseconds;
                _lockTimeoutTimer.Enabled = true;
            }

            Log.Info($"Server locked.{(_lockTimeoutTimer.Enabled ? "Lock timeout timer started." : string.Empty)}");

            return Task.FromResult(new LockServer_Responses());
        }

        public override Task<UnlockServer_Responses> UnlockServer(UnlockServer_Parameters request, ServerCallContext context)
        {
            // check if server is locked
            if (!IsServerLocked)
            {
                // raise defined execution error
                var error = SiLAFeature.GetDefinedExecutionErrors().Find(match => match.Identifier == "ServerNotLocked");
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateDefinedExecutionError(SiLAFeature.GetFullyQualifiedDefinedExecutionErrorIdentifier(error.Identifier), error.Description));
            }

            // check if lock identifier is valid
            if (!_currentLockIdentifier.Equals(request.LockIdentifier.Value))
            {
                // raise defined execution error
                var error = SiLAFeature.GetDefinedExecutionErrors().Find(match => match.Identifier == "InvalidLockIdentifier");
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateDefinedExecutionError(SiLAFeature.GetFullyQualifiedDefinedExecutionErrorIdentifier(error.Identifier), error.Description));
            }

            _lockTimeoutTimer.Enabled = false;
            _currentLockIdentifier = string.Empty;

            Log.Info("Server unlocked.");

            return Task.FromResult(new UnlockServer_Responses());
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Checks whether a lock identifier is required.
        /// In that case the given metadata is checked for valid lock identifier. An error is raised if no valid lock identifier is found.
        /// </summary>
        /// <param name="metadata">The metadata contained in the request headers of the received client request.</param>
        public void CheckLock(Metadata metadata)
        {
            if (!IsServerLocked)
            {
                // no lock identifier required if server is not locked
                return;
            }

            // get value of lock identifier metadata entry
            byte[] value = SilaClientMetadata.GetSilaClientMetadataValue(metadata, SiLAFeature.GetFullyQualifiedMetadataIdentifier("LockIdentifier"));

            Metadata_LockIdentifier lockIdentifier = null;
            try
            {
                // extract lock identifier object
                lockIdentifier = Metadata_LockIdentifier.Parser.ParseFrom(value);
            }
            catch (Exception e)
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(FrameworkError.Types.ErrorType.InvalidMetadata, "Lock identifier metadata value could not be parsed: " + e.Message));
            }

            if (lockIdentifier?.LockIdentifier == null)
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(FrameworkError.Types.ErrorType.InvalidMetadata, "Lock identifier metadata value could not be parsed (wrong message type)"));
                return;
            }

            // compare lock identifier
            if (!_currentLockIdentifier.Equals(lockIdentifier.LockIdentifier.Value))
            {
                // raise defined execution error
                var error = SiLAFeature.GetDefinedExecutionErrors().Find(match => match.Identifier == "InvalidLockIdentifier");
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateDefinedExecutionError(SiLAFeature.GetFullyQualifiedDefinedExecutionErrorIdentifier(error.Identifier), error.Description));
            }


            // if lock timeout timer is running: reset timer
            if (_lockTimeoutTimer.Enabled)
            {
                _lockTimeoutTimer.Interval = _lockTimeout.TotalMilliseconds;
                Log.Debug("Lock timeout timer reset.");
            }
        }

        #endregion

        #region Internal Methods

        private void OnLockTimeoutExpired(object source, ElapsedEventArgs e)
        {
            // unlock the server
            _lockTimeoutTimer.Enabled = false;
            _currentLockIdentifier = string.Empty;

            Log.Info($"Lock timeout timer elapsed after {_lockTimeoutTimer.Interval } ms. Server has been automatically unlocked.");
        }

        #endregion
    }
}