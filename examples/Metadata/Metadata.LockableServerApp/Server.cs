﻿namespace Sila2.Examples.Metadata.LockableServerApp
{
    using System;
    using System.Net.NetworkInformation;
    using Common.Logging;
    using Sila2.Server;
    using Grpc.Core;

    public class Server : SiLA2Server
    {
        private static ILog _log = LogManager.GetLogger<Server>();

        public Server(int portNumber, NetworkInterface networkInterface, string configFile, ServerCredentials credentials)
        : base(new ServerInformation(
            "Lockable Example Server",
            "A SiLA Server that implements the LockController feature to demonstrate the usage of SiLA Client Metadata. The locking mechanism is shown for the GreetingProvider feature, whereat the 'ProvideGreeting' command is lockable and the 'StartYear' property not.",
            "www.equicon.de",
            "1.0"),
            portNumber,
            networkInterface,
            credentials,
            configFile)
        {
            // read feature definitions
            var greetingProviderFeature = ReadFeature("features/GreetingProvider.sila.xml");
            var lockControllerFeature = ReadFeature("features/LockController.sila.xml");

            // add LockController feature implementation
            var lockControllerImpl = new LockControllerImpl
            {
                LockableFeature = greetingProviderFeature,
                SiLAFeature = lockControllerFeature
            };
            GrpcServer.Services.Add(Org.Silastandard.Core.Lockcontroller.V1.LockController.BindService(lockControllerImpl));

            // add GreetingProvider feature implementation
            var greetingProviderImpl = new GreetingProviderImpl
            {
                LockController = lockControllerImpl
            };
            GrpcServer.Services.Add(Org.Silastandard.Examples.Greetingprovider.V1.GreetingProvider.BindService(greetingProviderImpl));

            // set current year as server start year
            greetingProviderImpl.ServerStartYear = DateTime.Now.Year;

            _log.Info("Server created and initialized");
        }
    }
}