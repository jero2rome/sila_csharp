﻿namespace Sila2.Examples.Metadata.LockingClientApp
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Threading;
    using CommandLine;
    using Common.Logging;
    using Grpc.Core;

    using Discovery;
    using Utils;

    public class Program
    {
        static void Main(string[] args)
        {
            Logging.SetupCommonLogging();
            var log = LogManager.GetLogger<Program>();
            try
            {
                Channel channel;
                ClientCmdLineArgs options = null;
                Parser.Default.ParseArguments<ClientCmdLineArgs>(args)
                    .WithParsed(o => options = o)
                    .WithNotParsed((errs) => ClientCmdLineArgs.HandleParseError(errs));

                if (options.IpAddress == null && options.Port == -1)
                {
                    // use SiLA Server Discovery to find the Server
                    const string namePattern = "Lockable Example Server";
                    var server = SiLADiscovery.GetServers(3000).FirstOrDefault(s => s.Config.Name == namePattern);
                    if (server == null)
                    {
                        throw new InvalidOperationException($"No server named: {namePattern} found");
                    }

                    channel = server.Channel;
                }
                else
                {
                    channel = ServiceFinder.BuildNewChannel(IPAddress.Parse(options.IpAddress), options.Port);
                }

                // create client
                var client = new Client(channel);

                // command Get_FCPAffectedByMetadata_LockIdentifier
                var identifiers = client.GetLockAffectedIdentifiers;
                log.Debug("Identifiers affected by locking:");
                foreach (var i in identifiers)
                {
                    log.Debug($" - '{i}'");
                }

                // property CurrentYear
                log.Debug("--> request: \"Get_StartYear()\" ...");
                log.Debug($"<-- response: \"{client.GetStartYear()}\"");

                // command SayHello
                var param = "SiLA user";
                log.Debug($"--> request: \"SayHello(Name='{param}')\" ...");
                log.Debug($"<-- response: \"{client.SayHello(param)}\"");

                // check log state
                log.Debug("--> request: \"IsServerLocked()\" ...");
                var locked = client.IsServerLocked;
                if (locked != null)
                {
                    log.Debug($"<-- response: \"{(bool)locked}\"");
                }

                // lock server
                var lockIdentifier = "MY_LOCK_ID";
                int timeout = 0;
                log.Debug($"--> request: \"LockServer(LockIdentifier='{lockIdentifier}', Timeout={timeout})\" ...");
                client.LockServer(lockIdentifier ,timeout);

                // check log state
                log.Debug("--> request: \"IsServerLocked()\" ...");
                locked = client.IsServerLocked;
                if (locked != null)
                {
                    log.Debug($"<-- response: \"{(bool)locked}\"");
                }

                // try to lock server (already in lock state)
                log.Debug($"--> request: \"LockServer(LockIdentifier='{lockIdentifier}', Timeout={timeout})\" ...");
                client.LockServer(lockIdentifier, timeout);

                // try to call SiLAService feature property with metadata (not allowed)
                log.Debug($"--> request: \"Get_ImplementedFeatures()\" (with metadata lock identifier  '{lockIdentifier}') ...");
                client.CallSiLAServicePropertyWithMetadata(lockIdentifier);

                // property CurrentYear
                log.Debug("--> request: \"Get_StartYear()\" ...");
                log.Debug($"<-- response: \"{client.GetStartYear()}\"");

                // command SayHello (without required metadata)
                log.Debug($"--> request: \"SayHello(Name='{param}')\" (without metadata) ...");
                client.SayHello(param);

                // command SayHello (with wrong metadata key)
                log.Debug($"--> request: \"SayHello(Name='{param}')\" (with wrong metadata key) ...");
                client.SayHelloWithWrongLockIdentifierKey(param, lockIdentifier);

                // command SayHello (with metadata containing no Metadata_LockIdentifier but a SiLA Integer type)
                log.Debug($"--> request: \"SayHello(Name='{param}')\" (with wrong SiLA type (Integer) in metadata content) ...");
                client.SayHelloWithIntegerLockIdentifier(param, 42);

                // command SayHello with lock identifier format - not a gRPC message
                log.Debug($"--> request: \"SayHello(Name='{param}')\" (with wrong metadata content format) ...");
                client.SayHelloWithWrongFormatLockIdentifierContent(param, lockIdentifier);
                
                // command SayHello (with metadata containing invalid lock identifier)
                log.Debug($"--> request: \"SayHello(Name='{param}')\" (with metadata lock identifier  'something_else') ...");
                client.SayHelloWithLockIdentifier(param, "something_else");

                // command SayHello (with required metadata)
                log.Debug($"--> request: \"SayHello(Name='{param}')\" (with metadata lock identifier  '{lockIdentifier}') ...");
                log.Debug($"<-- response: \"{client.SayHelloWithLockIdentifier(param, lockIdentifier)}\"");

                // try unlock server without valid lock identifier
                log.Debug("--> request: \"UnlockServer(LockIdentifier='i_dont_know')\" ...");
                client.UnlockServer("i_dont_know");

                // unlock server with valid lock identifier
                log.Debug($"--> request: \"UnlockServer(LockIdentifier='{lockIdentifier}')\" ...");
                client.UnlockServer(lockIdentifier);

                // check log state 
                log.Debug("--> request: \"IsServerLocked()\" ...");
                locked = client.IsServerLocked;
                if (locked != null)
                {
                    log.Debug($"<-- response: \"{(bool)locked}\"");
                }

                // try to unlock server (not locked)
                log.Debug($"--> request: \"UnlockServer(LockIdentifier='{lockIdentifier}')\" ...");
                client.UnlockServer(lockIdentifier);

                // lock server with a timeout
                lockIdentifier = "MY_NEW_LOCK_ID";
                timeout = 5;
                log.Debug($"--> request: \"LockServer(LockIdentifier='{lockIdentifier}', Timeout={timeout})\" ...");
                client.LockServer(lockIdentifier, timeout);

                log.Debug("Waiting for 4 seconds ...");
                Thread.Sleep(4000);

                // command SayHello (with required metadata) to reset the lock timeout timer
                log.Debug($"--> request: \"SayHello(Name='{param}')\" (with metadata lock identifier  '{lockIdentifier}') -> resets the lock timeout");
                log.Debug($"<-- response: \"{client.SayHelloWithLockIdentifier(param, lockIdentifier)}\"");

                log.Debug("Waiting for 4 seconds ...");

                // check log state 
                log.Debug("--> request: \"IsServerLocked()\" ...");
                locked = client.IsServerLocked;
                if (locked != null)
                {
                    log.Debug($"<-- response: \"{(bool)locked}\"");
                }

                log.Debug("Waiting for 5 seconds to elapse the lock timeout...");
                Thread.Sleep(5000);

                // check log state 
                log.Debug("--> request: \"IsServerLocked()\" ...");
                locked = client.IsServerLocked;
                if (locked != null)
                {
                    log.Debug($"<-- response: \"{(bool)locked}\"");
                }

                Thread.Sleep(1000);
                Console.WriteLine("Shutting down connection...");

                channel.ShutdownAsync().Wait();
                Console.WriteLine("Press any key to exit...");
            }
            catch (Exception e)
            {
                log.Error("Client Error: ", e);
                return;
            }

            Console.ReadKey();
        }
    }
}
