using System.Net.NetworkInformation;

namespace Sila2.Examples.MockShaker.ServerApp
{
    using System;
    using System.Threading;
    using Utils;

    public class Program
    {
        static void Main(string[] args)
        {
            Logging.SetupCommonLogging();
            var Log = NLog.LogManager.GetCurrentClassLogger();
            try
            {
                var options = CmdLineArgs.Parse<ServerCmdLineArgs>(args);

                // create the shaker server
                Server shaker = null;
                NetworkInterface networkInterface = null;
                try
                {
                    // create SiLA2 Server
                    networkInterface = Networking.GetNetworkInterface(options.InterfaceNameOrCIDR);
                }
                catch (ArgumentNullException)
                {
                    // do nothing, network interface stays unassigned
                }
                shaker = new Server(options.Port, networkInterface, options.ConfigFile, options.Credentials);
                // start the server
                shaker.StartServer();

                Log.Info("MockShakerDevice server listening on port " + options.Port);

                ServerCli.BlockUntilStop();
                shaker.ShutdownServer().Wait();
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
        }
    }
}