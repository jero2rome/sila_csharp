﻿namespace Sila2.Examples.MockShaker.ServerApp
{
    using System.Net.NetworkInformation;
    using System;
    using Common.Logging;
    using Sila2;
    using Sila2.Server;
    using Grpc.Core;

    public class Server : SiLA2Server
    {
        private static ILog Log = LogManager.GetLogger<Server>();
        public static readonly string ServerName = "Mock Shaker";

        public Server(int portNumber, NetworkInterface networkInterface, string configFile, ServerCredentials credentials) :
            base(new ServerInformation(
                    ServerName,
                    "Simulates a shaker device with a electronic lock mechanism. In order to shake the lock clamps must be closed",
                    "www.equicon.de",
                    "1.0"),
                portNumber,
                networkInterface,
                credentials,
                configFile)
        {
            // create SiLA2 Server
            try
            {
                var shakerSimulator = new ShakerSimulator();

                ReadFeature("features/PlateHandlingControl.sila.xml");
                var plateHandlingControl = new PlateHandlingControlImpl(shakerSimulator);
                GrpcServer.Services.Add(De.Equicon.Handling.Platehandlingcontrol.V1.PlateHandlingControl.BindService(plateHandlingControl));

                var shakingControl = new ShakingControlImpl(ReadFeature("features/ShakingControl.sila.xml"), shakerSimulator);
                GrpcServer.Services.Add(De.Equicon.Mixing.Shakingcontrol.V1.ShakingControl.BindService(shakingControl));
                Log.Info("Added Services to SiLA2Server");
            }
            catch (Exception ex)
            {
                Log.Info("Server creation failed: " + ex.Message);
            }
        }
    }
}