namespace Sila2.Examples.MockShaker.ServerApp
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Xml;
    using Common.Logging;
    using Grpc.Core;
    using Org.Silastandard;
    using De.Equicon.Mixing.Shakingcontrol.V1;
    using Utils;

    internal class ShakingControlImpl : ShakingControl.ShakingControlBase
    {
        private static readonly ILog Log = LogManager.GetLogger<ShakingControlImpl>();
        private readonly ShakerSimulator _shakerSimulator;
        private readonly Feature _silaFeature;

        private readonly ObservableCommandManager<Shake_Parameters, Shake_Responses>
            _shakeCommandManager =
                new ObservableCommandManager<Shake_Parameters, Shake_Responses>(
                    TimeSpan.FromMinutes(ShakerSimulator.MaxShakingDuration));

        public ShakingControlImpl(Feature silaFeature, ShakerSimulator shakerSimulator)
        {
            _silaFeature = silaFeature;
            _shakerSimulator = shakerSimulator;
        }

        public override Task<StopShaking_Responses> StopShaking(StopShaking_Parameters request, ServerCallContext context)
        {
            _shakerSimulator.Stop();
            return Task.FromResult(new StopShaking_Responses());
        }

        public override async Task<CommandConfirmation> Shake(Shake_Parameters request,
            ServerCallContext context)
        {
            var command = await _shakeCommandManager.AddCommand(request, ShakeTask());
            return command.Confirmation;
        }

        public override async Task Shake_Info(CommandExecutionUUID request,
            IServerStreamWriter<ExecutionInfo> responseStream, ServerCallContext context)
        {
            try
            {
                await _shakeCommandManager.RegisterForInfo(request, responseStream, context.CancellationToken);
            }
            catch (Exception e)
            {
                Log.Error(e);
                throw;
            }
        }

        public override Task<Shake_Responses> Shake_Result(CommandExecutionUUID request,
            ServerCallContext context)
        {
            var command = _shakeCommandManager.GetCommand(request);
            return Task.FromResult(command.Result());
        }

        private Func<IProgress<ExecutionInfo>, Shake_Parameters, CancellationToken, Shake_Responses> ShakeTask()
        {
            return (progress, parameters, cancellationToken) =>
            {
                // check duration parameter
                var duration = TimeSpan.Zero;
                try
                {
                    duration = XmlConvert.ToTimeSpan(parameters.Duration.Value);
                }
                catch
                {
                    ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                        _silaFeature.GetFullyQualifiedCommandParameterIdentifier("Shake", "Duration"),
                        "Invalid duration notation. Specify the duration in ISO 8601 notation"));
                }

                try
                {
                    ReportProgress(progress, cancellationToken);
                    _shakerSimulator.Shake(duration).Wait(cancellationToken);
                }
                catch (Exception e)
                {
                    Log.Warn(e);
                    if (e.InnerException is ClampNotClosedException)
                    {
                        ErrorHandling.RaiseSiLAError(ErrorHandling.CreateUndefinedExecutionError("Clamp Not Closed: " + e.Message));
                    }

                    throw;
                }

                return new Shake_Responses();
            };
        }

        private async Task ReportProgress(IProgress<ExecutionInfo> progress,
            CancellationToken cancellationToken)
        {
            Log.Debug("Starting progress reporting...");
            var executionInfo = new ExecutionInfo();
            while (cancellationToken.IsCancellationRequested)
            {
                executionInfo.ProgressInfo = new Real { Value = _shakerSimulator.Progress };
                var remainingTime = _shakerSimulator.RemainingTime;
                executionInfo.EstimatedRemainingTime = new Duration
                { Seconds = (long)remainingTime.TotalSeconds, Nanos = remainingTime.Milliseconds * 100000 };
                progress.Report(executionInfo);
                await Task.Delay(2000, cancellationToken);
            }

            Log.Debug("Ending progress reporting");
        }
    }
}