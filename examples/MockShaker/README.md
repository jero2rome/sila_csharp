# Mock Shaker SiLA Example
This is a sample client and server application which offers a plate handling and 
shaking control features.

## Generate the Feature Stubs
In order to generate the stubs run the following commands on a Bash shell 
to generate the protos from features using the `code_generator.jar` and 
stubs from protos using the `protoc` compiler.

```
cd MockShaker/MockShaker
generate_stubs.cmd
```

## Running the Server

```
cd MockShaker.ServerApp
dotnet build
dotnet run -- -n <network-interface/ip-range>

```