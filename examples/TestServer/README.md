# DataTypeProvider
A simple sila server which provides a feature with a complex set of 
input and output data structures including binary types.

## TestServer.ServerApp

Launches the test server providing the DataTypeProvider feature.

## TestServer.ClientApp

Is used to demonstrate the usage of binary transfer in both directions.
