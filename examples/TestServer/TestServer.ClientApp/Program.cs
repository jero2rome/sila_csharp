﻿namespace Sila2.Examples.TestServer.ClientApp
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;

    using CommandLine;
    using Common.Logging;
    using Grpc.Core;
    using Discovery;
    using Utils;

    public class Program
    {
        static void Main(string[] args)
        {
            Logging.SetupCommonLogging();
            var logger = LogManager.GetLogger<Program>();
            try
            {
                Channel channel;
                ClientCmdLineArgs options = null;
                Parser.Default.ParseArguments<ClientCmdLineArgs>(args)
                    .WithParsed(o => options = o)
                    .WithNotParsed((errs) => ClientCmdLineArgs.HandleParseError(errs));

                var clientCredentials = options.Encryption != null
                    ? Encryption.CreateSslCredentials(options.Encryption)
                    : null;

                var channelOptions = new List<ChannelOption>
                {
                    ////{ new ChannelOption(ChannelOptions.MaxReceiveMessageLength, 20 * 1024 * 1024) },
                    ////{ new ChannelOption(ChannelOptions.MaxSendMessageLength, 2000) }
                };

                if (options.IpAddress == null && options.Port == -1)
                {
                    // use SiLA Server Discovery to find the DataTypeProvider Server
                    const string namePattern = "DataTypeProvider Server";
                    var server = SiLADiscovery.GetServers(3000, clientCredentials, channelOptions).FirstOrDefault(s => s.Config.Name == namePattern);
                    if (server == null)
                    {
                        throw new InvalidOperationException($"No server named: '{namePattern}' found");
                    }

                    channel = server.Channel;
                }
                else
                {
                    channel = ServiceFinder.BuildNewChannel(IPAddress.Parse(options.IpAddress), options.Port, clientCredentials, channelOptions);
                }

                // create client
                var client = new Client(channel);

                // property BinaryValueDirectly
                Console.WriteLine("\n--- Binary transfer ---\n");
                logger.Info("Calling 'Get_BinaryValueDirectly()' ...");
                logger.Info($"Response: '{client.GetBinaryValueDirectly()}'");

                // property BinaryValueDownload
                Console.WriteLine("\n");
                logger.Info("Calling 'Get_BinaryValueDownload()' ...");
                client.GetBinaryValueDownload().Wait();

                // command SetBinaryValue
                Console.WriteLine("\n");
                logger.Info("Calling 'SetBinaryValue' with sending the value directly ...");
                logger.Info(client.SetStringAsDirectBinaryValue("SiLA2_Test_String_Value"));

                Console.WriteLine("\n");
                logger.Info("Calling 'SetBinaryValue' with binary upload ...");
                client.SetBinaryValueUpload(System.Text.Encoding.UTF8.GetBytes("A_somewhat_longer_test_string_to_demonstrate_the_binary_upload"), 5).Wait();
                logger.Info($"Response: '{System.Text.Encoding.UTF8.GetString(client.ResponseData)}'");

                // generate random seqeunce of characters
                const int dataSize = 10 * 1024 * 1024; // 10 MB
                var sourceData = new byte[dataSize];
                Random rnd = new Random((int)DateTime.Now.Ticks);
                rnd.NextBytes(sourceData);

                // command SetBinaryValue with large binary data
                Console.WriteLine("\n");
                const int chunkSize = 1024;
                logger.Info($"Calling 'SetBinaryValue' with a random sequence of {dataSize} bytes and chunk size of {chunkSize} ...");
                client.SetBinaryValueUpload(sourceData, chunkSize).Wait();
                if (sourceData.SequenceEqual(client.ResponseData))
                {
                    logger.Info("Received response data equals sent data");
                }
                else
                {
                    logger.Error("Received response data differs from sent data!");
                }

                Console.WriteLine("\n--- Any type values ---\n");
                client.GetAnyTypeValues();

                // setting Any type values
                Console.WriteLine("\n");
                client.SetAnyTypeStringValue("My_Any_type_string_value");
                client.SetAnyTypeIntegerValue(123456789);
                client.SetAnyTypeRealValue(9.87654321);
                client.SetAnyTypeBooleanValue(true);
                client.SetAnyTypeBinaryValue("My_Any_type_binary_value_string_content");
                client.SetAnyTypeDateValue(new Org.Silastandard.Date { Day = 29, Month = 7, Year = 2021, Timezone = new Org.Silastandard.Timezone { Hours = 2, Minutes = 0 } });
                client.SetAnyTypeTimeValue(new Org.Silastandard.Time { Second = 44, Minute = 33, Hour = 22, Timezone = new Org.Silastandard.Timezone { Hours = 2, Minutes = 0 } });
                client.SetAnyTypeTimestampValue(new Org.Silastandard.Timestamp { Second = 44, Minute = 33, Hour = 22, Day = 29, Month = 7, Year = 2021, Timezone = new Org.Silastandard.Timezone { Hours = 2, Minutes = 0 } });

                System.Threading.Thread.Sleep(1000);
                Console.WriteLine("\nShutting down connection...");

                channel.ShutdownAsync().Wait();
                Console.WriteLine("Press any key to exit...");
            }
            catch (RpcException e)
            {
                logger.Error("Client Error: " + ErrorHandling.HandleException(e));
            }
            catch (Exception e)
            {
                logger.Error("Client Error: ", e);
                return;
            }

            Console.ReadKey();
        }
    }
}