﻿namespace Sila2.Examples.TestServer.ServerApp
{
    using System.Threading.Tasks;
    using Grpc.Core;
    using Google.Protobuf;
    using Org.Silastandard;

    internal class BinaryDownloadImpl : BinaryDownload.BinaryDownloadBase
    {
        /// <summary>TestServer instance</summary>
        private readonly Server _server;

        public BinaryDownloadImpl(Server server)
        {
            _server = server;
        }

        #region Overrides of BinaryDownloadBase

        public override Task<GetBinaryInfoResponse> GetBinaryInfo(GetBinaryInfoRequest request, ServerCallContext context)
        {
            var binaryTransferUUID = _server.CheckBinaryTransferUUID(request.BinaryTransferUUID, false);

            return Task.FromResult(new GetBinaryInfoResponse
            {
                BinarySize = (ulong)_server.DownloadableData[binaryTransferUUID].Length,
                LifetimeOfBinary = new Duration()    ////TODO: add lifetime of binary data
            });
        }

        public override async Task GetChunk(IAsyncStreamReader<GetChunkRequest> requestStream, IServerStreamWriter<GetChunkResponse> responseStream, ServerCallContext context)
        {
            while (await requestStream.MoveNext(context.CancellationToken))
            {
                var binaryTransferUUID = _server.CheckBinaryTransferUUID(requestStream.Current.BinaryTransferUUID, false);

                // get data chunk
                var chunk = ByteString.CopyFrom(_server.DownloadableData[binaryTransferUUID], (int)requestStream.Current.Offset, (int)requestStream.Current.Length);

                await responseStream.WriteAsync(new GetChunkResponse
                {
                    BinaryTransferUUID = binaryTransferUUID.ToString(),
                    Offset = requestStream.Current.Offset,
                    Payload = chunk,
                    LifetimeOfBinary = new Duration()    ////TODO: add lifetime of binary data
                });
            }
        }

        public override Task<DeleteBinaryResponse> DeleteBinary(DeleteBinaryRequest request, ServerCallContext context)
        {
            var binaryTransferUUID = _server.CheckBinaryTransferUUID(request.BinaryTransferUUID, false);

            _server.DownloadableData.Remove(binaryTransferUUID);   ////TODO. check whether this crashes with a parallel running binary download

            return Task.FromResult(new DeleteBinaryResponse());
        }

        #endregion
    }
}