﻿namespace Sila2.Examples.TestServer.ServerApp
{
    using System;
    using System.Threading.Tasks;
    using Grpc.Core;
    using Org.Silastandard;

    internal class BinaryUploadImpl : BinaryUpload.BinaryUploadBase
    {
        /// <summary>TestServer instance</summary>
        private readonly Server _server;

        public BinaryUploadImpl(Server server)
        {
            _server = server;
        }

        #region Overrides of BinaryUploadBase

        public override Task<CreateBinaryResponse> CreateBinary(CreateBinaryRequest request, ServerCallContext context)
        {
            // check if specified parameter is defined in a feature and has Binary type
            var feature = _server.GetFeatureOfElement(request.ParameterIdentifier);
            if (feature == null)
            {
                // specified feature does not exist
                Utils.ErrorHandling.RaiseBinaryTransferError(BinaryTransferError.Types.ErrorType.BinaryUploadFailed,
                    $"Found no implemented feature that matches the parameter identifier '{request.ParameterIdentifier}'");
            }

            var obj = feature.GetMatchingElement(request.ParameterIdentifier);
            if (obj == null)
            {
                // specified command or parameter does not exist
                Utils.ErrorHandling.RaiseBinaryTransferError(BinaryTransferError.Types.ErrorType.BinaryUploadFailed,
                    $"No command/parameter found that matches the parameter identifier '{request.ParameterIdentifier}'");
            }

            //TODO: compare size with available resources

            // create storage for data
            Guid binaryTransferUUID = Guid.NewGuid();
            _server.UploadedData[binaryTransferUUID] = new byte[request.ChunkCount][];

            return Task.FromResult(new CreateBinaryResponse
            {
                BinaryTransferUUID = binaryTransferUUID.ToString(),
                LifetimeOfBinary = new Duration()   ////TODO: add lifetime of binary data
            });
        }

        public override async Task UploadChunk(IAsyncStreamReader<UploadChunkRequest> requestStream, IServerStreamWriter<UploadChunkResponse> responseStream, ServerCallContext context)
        {
            while (await requestStream.MoveNext(context.CancellationToken))
            {
                var binaryTransferUUID = _server.CheckBinaryTransferUUID(requestStream.Current.BinaryTransferUUID, true);

                // set received data chunk
                _server.UploadedData[binaryTransferUUID][requestStream.Current.ChunkIndex] = requestStream.Current.Payload.ToByteArray();

                await responseStream.WriteAsync(new UploadChunkResponse
                {
                    BinaryTransferUUID = binaryTransferUUID.ToString(),
                    ChunkIndex = requestStream.Current.ChunkIndex,
                    LifetimeOfBinary = new Duration()    ////TODO: add lifetime of binary data
                });
            }
        }

        public override Task<DeleteBinaryResponse> DeleteBinary(DeleteBinaryRequest request, ServerCallContext context)
        {
            var binaryTransferUUID = _server.CheckBinaryTransferUUID(request.BinaryTransferUUID, false);

            _server.UploadedData.Remove(binaryTransferUUID);   ////TODO: check whether this crashes with a parallel running binary download

            return Task.FromResult(new DeleteBinaryResponse());
        }

        #endregion
    }
}