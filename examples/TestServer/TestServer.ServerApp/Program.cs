﻿namespace Sila2.Examples.TestServer.ServerApp
{
    using System;
    using System.Threading;
    using System.Net.NetworkInformation;
    using Common.Logging;
    using Utils;

    public class Program
    {
        public static void Main(string[] args)
        {
            Logging.SetupCommonLogging();
            var Log = LogManager.GetLogger<Program>();
            try
            {
                // Parse cmd line args
                var options = CmdLineArgs.Parse<ServerCmdLineArgs>(args);

                // create SiLA2 Server
                // start the gRPC Server
                Server server;
                NetworkInterface networkInterface = null;
                try
                {
                    // create SiLA2 Server
                    networkInterface = Networking.GetNetworkInterface(options.InterfaceNameOrCIDR);
                }
                catch (ArgumentNullException)
                {
                    // do nothing, network interface stays unassigned
                }
                server = new Server(options.Port, networkInterface, options.ConfigFile, options.Credentials);
                server.StartServer();
                Log.Info("DataTypeProvider Server listening on port " + options.Port);

                ServerCli.BlockUntilStop();
                server.ShutdownServer().Wait();
            }
            catch (Exception e)
            {
                Log.Error("Server creation/initialization exception: ", e);
            }
        }
    }
}