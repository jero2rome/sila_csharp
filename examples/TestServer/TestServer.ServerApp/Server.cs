﻿namespace Sila2.Examples.TestServer.ServerApp
{
    using System;
    using System.Collections.Generic;
    using System.Net.NetworkInformation;
    using Common.Logging;
    using Grpc.Core;
    using Sila2;
    using Sila2.Server;
    using Sila2.Utils;

    /// <summary>
    /// Implements a SiLA 2 Server that provides a GreetingProvider and a TemperatureController feature.
    /// </summary>
    public class Server : SiLA2Server
    {
        private static ILog Log = LogManager.GetLogger<Server>();

        public Feature SiLAFeature { get; }

        public Server(int portNumber, NetworkInterface networkInterface, string configFile, ServerCredentials credentials)
        : base(new ServerInformation(
                    "DataTypeProvider Server",
                    "Server that implements the DataTypeProvider feature which provides calls using all available SiLA Data Types for their parameters",
                    "www.equicon.de",
                    "1.0"),
                portNumber,
                networkInterface,
                credentials,
                configFile)
        {
            this.SiLAFeature = this.ReadFeature("features/DataTypeProvider.sila.xml");
            this.GrpcServer.Services.Add(Org.Silastandard.Examples.Datatypeprovider.V1.DataTypeProvider.BindService(new DataTypeProviderImpl(this)));

            this.GrpcServer.Services.Add(Org.Silastandard.BinaryUpload.BindService(new BinaryUploadImpl(this)));
            this.GrpcServer.Services.Add(Org.Silastandard.BinaryDownload.BindService(new BinaryDownloadImpl(this)));
        }

        #region Binary Transfer

        public Dictionary<Guid, byte[][]> UploadedData { get; } = new Dictionary<Guid, byte[][]>();

        public Dictionary<Guid, byte[]> DownloadableData { get; } = new Dictionary<Guid, byte[]>();

        public byte[] GetUploadedData(Guid binaryTransferUUID)
        {
            List<byte> result = new List<byte>();

            if (UploadedData.ContainsKey(binaryTransferUUID))
            {
                for (int i = 0; i < UploadedData[binaryTransferUUID].Length; i++)
                {
                    var chunk = UploadedData[binaryTransferUUID][i];
                    if (chunk == null)
                    {
                        ErrorHandling.RaiseBinaryTransferError(Org.Silastandard.BinaryTransferError.Types.ErrorType.BinaryUploadFailed,
                            $"Not all chunks have been uploaded for Binary Transfer UUID '{binaryTransferUUID}' - no chunk with index {i} found");
                    }

                    result.AddRange(chunk);
                }
            }

            return result.ToArray();
        }

        public Guid CheckBinaryTransferUUID(string binaryTransferUUIDString, bool upload)
        {
            if (!Guid.TryParse(binaryTransferUUIDString, out var binaryTransferUUID) ||
                upload && !UploadedData.ContainsKey(binaryTransferUUID) ||
                !upload && !DownloadableData.ContainsKey(binaryTransferUUID))
            {
                ErrorHandling.RaiseBinaryTransferError(Org.Silastandard.BinaryTransferError.Types.ErrorType.InvalidBinaryTransferUuid,
                    $"The given Binary Transfer UUID '{binaryTransferUUIDString}' is not valid");
            }

            return binaryTransferUUID;
        }

        #endregion
    }
}