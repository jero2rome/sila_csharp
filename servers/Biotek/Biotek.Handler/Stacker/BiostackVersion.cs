namespace Biotek.Handler.Stacker
{
    public class BiostackVersion
    {
        public const double V2 = 1.10;
        public const double V3 = 1.11;
        public const double V4 = 1.12;
    }
}