using Common.Logging;

namespace Biotek.Handler.Stacker
{
    public class DisabledStackerController : IStackerController
    {
        private static readonly ILog _log = LogManager.GetLogger<DisabledStackerController>();
        public bool RestackMode
        {
            get => throw GetDisabledException();
            set => throw GetDisabledException();
        }

        public bool StackMode
        {
            get => false;
            set => throw GetDisabledException();
        }

        public bool TeachMode
        {
            get => throw GetDisabledException();
            set => throw GetDisabledException();
        }

        public PlateState PlateState => throw GetDisabledException();

        public InstrumentType Instrument
        {
            get => throw GetDisabledException();
            set => throw GetDisabledException();
        }

        public InstrumentInterface InstrumentInterface
        {
            get => throw GetDisabledException();
            set => throw GetDisabledException();
        }
        public void JogUp(int steps)
        {
            ThrowDisabledException();
        }

        public void JogDown(int steps)
        {
            ThrowDisabledException();
        }

        public void TeachThis()
        {
            ThrowDisabledException();
        }

        public void Initialize()
        {
            _log.Info("Stacker is disabled!");
        }

        public void LoadPlate()
        {
            ThrowDisabledException();
        }

        public void PrepareNext()
        {
            ThrowDisabledException();
        }

        public void Restack()
        {
            ThrowDisabledException();
        }

        private static void ThrowDisabledException()
        {
            throw GetDisabledException();
        }

        private static StackerException GetDisabledException()
        {
            return new StackerException("Stacker is disabled!");
        }
    }
}