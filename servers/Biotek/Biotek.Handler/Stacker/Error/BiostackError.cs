using System;
using System.Collections.Generic;
using System.Linq;

namespace Biotek.Handler.Stacker
{
    public static class BiostackError
    {
        private const string ENTITY_ID0_FLAG = "%0";
        private const string ENTITY_ID1_FLAG = "%1";
        private const ushort NO_ERROR = 0x0000;
        
        public static readonly Code INVALID_REQUEST = new Code(0x8001, nameof(INVALID_REQUEST), "No error present");
        public static readonly Code NO_SENSOR_ERR = new Code(0x0200,  0xF, nameof(NO_SENSOR_ERR), $"couldn't find opto-sensor {ENTITY_ID0_FLAG}");
        public static readonly Code STACK_NOT_LOCKED_ERR = new Code(0x0300, nameof(STACK_NOT_LOCKED_ERR), "One or both of the stack locks are open");
        public static readonly Code MOTOR_VERIFY_ERR = new Code(0x0400,  0xF, nameof(MOTOR_VERIFY_ERR), $"Failed positional for sensor {ENTITY_ID0_FLAG}");
        public static readonly Code MOTOR_IN_USE_ERR = new Code(0x0550,  0xF, nameof(MOTOR_IN_USE_ERR), $"Motor {ENTITY_ID0_FLAG} is in use");
        public static readonly Code AUTOCAL_PARAMETER_ERR = new Code(0x0600, nameof(AUTOCAL_PARAMETER_ERR), "Configuration data is missing");
        public static readonly Code AUTOCAL_JIG_ERR = new Code(0x0650, nameof(AUTOCAL_JIG_ERR), "Autocal jig placement error");
        public static readonly Code INTERFACE_CHECKSUM_ERR = new Code(0x0700, nameof(INTERFACE_CHECKSUM_ERR), "Failed interface structure checksum test");
        public static readonly Code INSTRUMENT_CHECKSUM_ERR = new Code(0x0750, nameof(INSTRUMENT_CHECKSUM_ERR), "Failed instrument dimension structure checksum test");
        public static readonly Code MOTOR_NOT_HOMED_ERR = new Code(0x0800,  0xF, nameof(MOTOR_NOT_HOMED_ERR), $"Motor {ENTITY_ID0_FLAG} not homed successfully");
        public static readonly Code MALLOC_ERR = new Code(0x0900, nameof(MALLOC_ERR), "Memory allocation failure");
        public static readonly Code AUTOCAL_CHECKSUM_ERR = new Code(0x1000, nameof(AUTOCAL_CHECKSUM_ERR), "Calibration data checksum failed at power-up");
        public static readonly Code INTERNAL_RAM_TEST_ERR = new Code(0x1200, nameof(INTERNAL_RAM_TEST_ERR), "Internal RAM test error");
        public static readonly Code SERIAL_TIMEOUT_ERR = new Code(0x1300, nameof(SERIAL_TIMEOUT_ERR), "Timeout sending or receiving serial data");
        public static readonly Code SC_RX_BUF_OVERFLOW_ERR = new Code(0x1303, nameof(SC_RX_BUF_OVERFLOW_ERR), "Serial receive buffer overflow");
        public static readonly Code SC_CHECKSUM_ERR = new Code(0x1304, nameof(SC_CHECKSUM_ERR), "Serial message checksum");
        public static readonly Code SC_OBJECT_NOT_SUPPORTED_ERR = new Code(0x1307, nameof(SC_OBJECT_NOT_SUPPORTED_ERR), "Received request object not supported");
        public static readonly Code SC_MSG_BODY_SIZE_ERR = new Code(0x1308, nameof(SC_MSG_BODY_SIZE_ERR), "Message body size exceeds max limit");
        public static readonly Code SC_MAX_REQ_RUNNING_ERR = new Code(0x1309, nameof(SC_MAX_REQ_RUNNING_ERR), "Max requests currently running");
        public static readonly Code POWER_UP_CHECK_ERR = new Code(0x1500,  0xF, nameof(POWER_UP_CHECK_ERR), $"Power supply with value {ENTITY_ID0_FLAG} is out of range. 1 = +5VDC or 2 = +24VDC");

        public static readonly Code NO_PLATE_ON_CARRIER = new Code(0x1600, nameof(NO_PLATE_ON_CARRIER), "Attempted plate transfer from destination stack to source stack (or vice versa) - no plate found on stacker carrier after plate retrieval; the is also set anytime the carrier is transferred from the extended or claw position ");

        public static readonly Code NO_PLATE_ON_CARRIER_1 = new Code(0x1601, nameof(NO_PLATE_ON_CARRIER_1), "No plate found on the stacker carrier after transfer from the input stack in a dual carrier stacker.");

        public static readonly Code NO_PLATE_ON_CARRIER_2 = new Code(0x1602, nameof(NO_PLATE_ON_CARRIER_2), "Attempted plate transfer from source or destination stack to stacker carrier - no plate found on stacker carrier after transfer");

        public static readonly Code NO_PLATE_IN_GRIPPER = new Code(0x1700, nameof(NO_PLATE_IN_GRIPPER), "Attempted plate transfer from stacker carrier to instrument - no plate found in gripper after attempt to pickup plate from stacker carrier");

        public static readonly Code NO_PLATE_IN_GRIPPER_1 = new Code(0x1701, nameof(NO_PLATE_IN_GRIPPER_1), "Attempted plate transfer from instrument to claw/gripper - no plate found in gripper after attempt to pickup plate from instrument");

        public static readonly Code NOTHING_ON_CARRIER_TRAY = new Code(0x1702, nameof(NOTHING_ON_CARRIER_TRAY), "Nothing was found on the stacker carrier tray when attempting to place a lid on a plate.");
        public static readonly Code MISSING_LID = new Code(0x1703, nameof(MISSING_LID), "A lid is missing when one should have been present.");
        public static readonly Code PLATE_ON_LID_ERR = new Code(0x1704, nameof(PLATE_ON_LID_ERR), "A plate on top of a lid was detected (the plate should not be there).");
        public static readonly Code NOT_LID_OR_PLATE_ON_LID = new Code(0x1705, nameof(NOT_LID_OR_PLATE_ON_LID), "The source tray contains a plate or lidded plate (but should not).");
        public static readonly Code PLATE_LIMIT_REACHED = new Code(0x1706, nameof(PLATE_LIMIT_REACHED), "Restacking or Forwarding was stopped because MAX_PLATE limit was reached.");
        public static readonly Code MISSING_PLATE_ERR = new Code(0x1707, nameof(MISSING_PLATE_ERR), "A plate is missing when one should have been present.");
        public static readonly Code LIDS_NOT_ALLOWED_ERR = new Code(0x1708, nameof(LIDS_NOT_ALLOWED_ERR), "This operation will not work with Lid Mode selected.");
        public static readonly Code PLATE_ON_PLATE_ERR = new Code(0x1709, nameof(PLATE_ON_PLATE_ERR), "A plate on top of a plate was detected.");
        public static readonly Code LID_GEOMETRY_NOT_SET = new Code(0x170B, nameof(LID_GEOMETRY_NOT_SET), "A Set Lid Geometry command was not received since power-up.");
        public static readonly Code UNDEFINED_MOTOR = new Code(0x1800,  0xF, nameof(UNDEFINED_MOTOR), $"Undefined motor {ENTITY_ID0_FLAG}");

        public static readonly Code PLATE_IN_GRIPPER = new Code(0x1900, nameof(PLATE_IN_GRIPPER), "A plate was found in the gripper before the power-up homing sequence was run.");

        public static readonly Code PLATE_IN_GRIPPER_1 = new Code(0x1901, nameof(PLATE_IN_GRIPPER_1), "Attempted plate transfer from claw/gripper to instrument - plate still in gripper after transfer");

        public static readonly Code PLATE_IN_GRIPPER_2 = new Code(0x1902, nameof(PLATE_IN_GRIPPER_2), "A plate was still in the gripper after attempting to place the plate back on the carrier during the burn-in routine.");
        public static readonly Code PLATE_ON_CARRIER = new Code(0x2000, nameof(PLATE_ON_CARRIER), "A plate was found on the carrier during the power-up homing sequence.");
        public static readonly Code PLATE_ON_CARRIER_1 = new Code(0x2001, nameof(PLATE_ON_CARRIER_1), "Not used");
        public static readonly Code PLATE_ON_CARRIER_2 = new Code(0x2002, nameof(PLATE_ON_CARRIER_2), "Not used");

        public static readonly Code PLATE_ON_CARRIER_3 = new Code(0x2003, nameof(PLATE_ON_CARRIER_3), "Attempted plate transfer from an instrument to a source or destination stack – plate left on carrier after transfer.");

        public static readonly Code INVALID_MOTOR_AXIS = new Code(0x2100,  0xF, nameof(INVALID_MOTOR_AXIS), $"The motor {ENTITY_ID0_FLAG} is unknown to the stacker or incorrect for this action.");

        public static readonly Code INVALID_INSTRUMENT = new Code(0x2200,  0xF, nameof(INVALID_INSTRUMENT), $"Instrument {ENTITY_ID0_FLAG} number is unknown to the Stacker.");
        public static readonly Code GRIPPER_WIDTH_INVALID = new Code(0x2300, nameof(GRIPPER_WIDTH_INVALID), "Gripper width entered is outside of allowed range");
        public static readonly Code HARDWARE_NOT_PRESENT_DUAL_CARRIER = new Code(0x2401, nameof(HARDWARE_NOT_PRESENT_DUAL_CARRIER), "A dual carrier is not present.");
        public static readonly Code HARDWARE_NOT_PRESENT_ODD_SPACED_DUAL_CARRIER = new Code(0x2402, nameof(HARDWARE_NOT_PRESENT_ODD_SPACED_DUAL_CARRIER), "An odd spaced dual carrier is not present.");
        public static readonly Code HARDWARE_NOT_PRESENT_WRIST = new Code(0x2403, nameof(HARDWARE_NOT_PRESENT_WRIST), "A wrist is not installed.");
        public static readonly Code HARDWARE_NOT_PRESENT_BARCODE_READER = new Code(0x2404, nameof(HARDWARE_NOT_PRESENT_BARCODE_READER), "A barcode reader is not installed.");
        public static readonly Code HARDWARE_NOT_PRESENT_ALT_WRIST = new Code(0x2405, nameof(HARDWARE_NOT_PRESENT_ALT_WRIST), "Alternative wrist position is not selected.");
        public static readonly Code CLAW_INSTR_INTERFACE_ERR_OUT_OF_RANGE = new Code(0x2600, nameof(CLAW_INSTR_INTERFACE_ERR_OUT_OF_RANGE), "A claw interface dimension is out of range at power up.");

        public static readonly Code CLAW_INSTR_INTERFACE_ERR_INVALID_RELEASE_POSITION = new Code(0x2601, nameof(CLAW_INSTR_INTERFACE_ERR_INVALID_RELEASE_POSITION), "The current instrument’s claw release position is invalid.");

        public static readonly Code CLAW_INSTR_INTERFACE_ERR_INVALID_CLEAR_POSITION = new Code(0x2602, nameof(CLAW_INSTR_INTERFACE_ERR_INVALID_CLEAR_POSITION), "The current instrument’s claw clear position is invalid");

        public static readonly Code CLAW_INSTR_INTERFACE_ERR_INVALID_GRIP_POSITION = new Code(0x2603, nameof(CLAW_INSTR_INTERFACE_ERR_INVALID_GRIP_POSITION), "The current instrument’s claw grip position is invalid");

        public static readonly Code SCANNER_SET_ERR = new Code(0x2700, nameof(SCANNER_SET_ERR), "An error occurred in the process of setting a configuration value for the barcode scanner, or trying to reset the scanner");

        public static readonly Code SCANNER_CODE_ERR = new Code(0x2701, nameof(SCANNER_CODE_ERR), "A response from the barcode scanner during setting of a configuration value for the barcode scanner, did not contain the correct barcode selection (i.e. 0-3)");

        public static readonly Code SCANNER_CODE_TYPE_ERR = new Code(0x2702, nameof(SCANNER_CODE_TYPE_ERR), "The barcode type returned from the barcode scanner is not one of the 4 default types to be supported");

        public static readonly Code SCANNER_TYPE_MISSING_ERR = new Code(0x2703, nameof(SCANNER_TYPE_MISSING_ERR), "At least one of the 4 barcode types to be supported by the scanner is not being supported.");

        public static readonly Code SCANNER_START_STOP_DIS_ERR = new Code(0x2704, nameof(SCANNER_START_STOP_DIS_ERR), "An error occurred while attempting to disable the transmission of start and stop characters for the Codabar barcode type by the barcode scanner");
        public static readonly Code OUT_OF_RANGE_ERR = new Code(0x2800, nameof(OUT_OF_RANGE_ERR), "A parameter or calculation is out of range");

        public static readonly Code INVALID_COMBINATION_ERR = new Code(0x2900, nameof(INVALID_COMBINATION_ERR), "Invalid combination of parameters associated with carrier trays and stacks");
        public static readonly Code INVALID_INTERFACE = new Code(0x2A00,  0xF, nameof(INVALID_INTERFACE), $"Invalid interface {ENTITY_ID0_FLAG}. (0 = primary, 1 = secondary)");

        public static readonly Code INVALID_COMMAND_SET = new Code(0x2B00, nameof(INVALID_COMMAND_SET), "This command is not supported in the current command set. Usually related to the use of lidded plates.");

        public static readonly Code LID_GEOMETRY_RANGE_ERR = new Code(0x2C00, 0xFF, nameof(LID_GEOMETRY_RANGE_ERR), $"parameter {ENTITY_ID0_FLAG} in lid geometry is out of range – refer to functional spec for detail", Code.GeometryRangeFormatter);
        public static readonly Code TASK_DELETED_ERR = new Code(0xA100, nameof(TASK_DELETED_ERR), "Task has been deleted because of an error - fatal");
        public static readonly Code DEV_NOT_AVAIL_ERR = new Code(0xA300,  0xF, nameof(DEV_NOT_AVAIL_ERR), $"Motor {ENTITY_ID0_FLAG} not available - fatal");

        public static readonly Code MTR_TRUNCATION_ERR = new Code(0xAE00, 0xFF, nameof(MTR_TRUNCATION_ERR), $"({ENTITY_ID1_FLAG} = error location, {ENTITY_ID0_FLAG} = motor) profile could not be correctly truncated - fatal", Code.MotorTruncationFormatter);

        public static readonly Code MTR_PROFILE_ERR = new Code(0xAE80,  0xF, nameof(MTR_PROFILE_ERR), $"Motor {ENTITY_ID0_FLAG} profile resolution is invalid for phase correction - fatal");
        
        /// <summary>
        /// Error messages associated to error code 
        /// </summary>
        private static readonly List<Code> CODES = new List<Code>
        {
            NO_SENSOR_ERR,
            STACK_NOT_LOCKED_ERR,
            MOTOR_VERIFY_ERR,
            MOTOR_IN_USE_ERR,
            AUTOCAL_PARAMETER_ERR,
            AUTOCAL_JIG_ERR,
            INTERFACE_CHECKSUM_ERR,
            INSTRUMENT_CHECKSUM_ERR,
            MOTOR_NOT_HOMED_ERR,
            MALLOC_ERR,
            AUTOCAL_CHECKSUM_ERR,
            INTERNAL_RAM_TEST_ERR,
            SERIAL_TIMEOUT_ERR,
            SC_RX_BUF_OVERFLOW_ERR,
            SC_CHECKSUM_ERR,
            SC_OBJECT_NOT_SUPPORTED_ERR,
            SC_MSG_BODY_SIZE_ERR,
            SC_MAX_REQ_RUNNING_ERR,
            POWER_UP_CHECK_ERR,
            NO_PLATE_ON_CARRIER,
            NO_PLATE_ON_CARRIER_1,
            NO_PLATE_ON_CARRIER_2,
            NO_PLATE_IN_GRIPPER,
            NO_PLATE_IN_GRIPPER_1,
            NOTHING_ON_CARRIER_TRAY,
            MISSING_LID,
            PLATE_ON_LID_ERR,
            NOT_LID_OR_PLATE_ON_LID,
            PLATE_LIMIT_REACHED,
            MISSING_PLATE_ERR,
            LIDS_NOT_ALLOWED_ERR,
            PLATE_ON_PLATE_ERR,
            LID_GEOMETRY_NOT_SET,
            UNDEFINED_MOTOR,
            PLATE_IN_GRIPPER,
            PLATE_IN_GRIPPER_1,
            PLATE_IN_GRIPPER_2,
            PLATE_ON_CARRIER,
            PLATE_ON_CARRIER_1,
            PLATE_ON_CARRIER_2,
            PLATE_ON_CARRIER_3,
            INVALID_MOTOR_AXIS,
            INVALID_INSTRUMENT,
            GRIPPER_WIDTH_INVALID,
            HARDWARE_NOT_PRESENT_DUAL_CARRIER,
            HARDWARE_NOT_PRESENT_ODD_SPACED_DUAL_CARRIER,
            HARDWARE_NOT_PRESENT_WRIST,
            HARDWARE_NOT_PRESENT_BARCODE_READER,
            HARDWARE_NOT_PRESENT_ALT_WRIST,
            CLAW_INSTR_INTERFACE_ERR_OUT_OF_RANGE,
            CLAW_INSTR_INTERFACE_ERR_INVALID_RELEASE_POSITION,
            CLAW_INSTR_INTERFACE_ERR_INVALID_CLEAR_POSITION,
            CLAW_INSTR_INTERFACE_ERR_INVALID_GRIP_POSITION,
            SCANNER_SET_ERR,
            SCANNER_CODE_ERR,
            SCANNER_CODE_TYPE_ERR,
            SCANNER_TYPE_MISSING_ERR,
            SCANNER_START_STOP_DIS_ERR,
            OUT_OF_RANGE_ERR,
            INVALID_COMBINATION_ERR,
            INVALID_INTERFACE,
            INVALID_COMMAND_SET,
            LID_GEOMETRY_RANGE_ERR,
            TASK_DELETED_ERR,
            DEV_NOT_AVAIL_ERR,
            MTR_TRUNCATION_ERR,
            MTR_PROFILE_ERR
        };

        public class Code
        {
            internal readonly ushort Value;
            private readonly string name;
            private readonly ushort range;
            private readonly string messageFormat;
            private readonly Func<Code, ushort, string> messageProvider;

            internal Code(ushort value, string name, string messageFormat) : this(value, 0, name, messageFormat)
            {
            }

            internal Code(ushort value, ushort range, string name, string messageFormat) : this(value, range, name, messageFormat, DefaultMessageFormatter)
            {
            }
            
            internal Code(ushort value, ushort range, string name, string messageFormat, Func<Code, ushort, string> messageProvider)
            {
                this.Value = value;
                this.range = range;
                this.name = name;
                this.messageFormat = messageFormat;
                this.messageProvider = messageProvider;
            }

            public bool IsInRange(ushort errorCode)
            {
                return Value >= errorCode && errorCode <= Value + range;
            }

            public string ToString(ushort errorCode)
            {
                return $"{this.name} (0x{errorCode:X4}): {this.messageProvider.Invoke(this, errorCode)}";
            }

            private static string DefaultMessageFormatter(Code code, ushort errorCode)
            {
                var hexString = errorCode.ToString("X4");
                return code.messageFormat.Replace(ENTITY_ID0_FLAG, hexString[3].ToString());
            }

            internal static string GeometryRangeFormatter(Code code, ushort errorCode)
            {
                var parameter = (errorCode & 0xFF00) + (errorCode & 0x00FF);
                return code.messageFormat.Replace(ENTITY_ID0_FLAG, parameter.ToString());
            }

            internal static string MotorTruncationFormatter(Code code, ushort errorCode)
            {
                var hexString = errorCode.ToString("X4");
                return code.messageFormat
                    .Replace(ENTITY_ID0_FLAG, hexString[3].ToString())
                    .Replace(ENTITY_ID1_FLAG, hexString[2].ToString());
            }
        }

        public static Code FindCode(ushort errorCode)
        {
            var codes = CODES.FindAll(c => c.IsInRange(errorCode));
            if (codes.Count == 0)
            {
                return null;
            }

            return codes.First();
        }

        public static StackerException Create(ushort errorCode)
        {
            var code = FindCode(errorCode);
            if (code == null)
            {
                return new StackerException("Unknown error code: 0x" + errorCode.ToString("X4"));
            }

            return new StackerInstrumentException(code, errorCode);
        }
    }
}