using System;

namespace Biotek.Handler.Stacker
{
    public class StackerInfo
    {
        public float FunctionalVersion { get; }
        public float BaseCodeVersion { get; }
        public ushort BaseCodeChecksum { get; }
        public byte DipSwitchSettings { get; }
        public byte PlateStatus { get; }
        public ushort StackedHeight { get; }
        public ulong BasecodePartNumber { get; }
        public bool LidHandlingModeEnabled { get; }


        internal StackerInfo(Message message)
        {
            var index = 0;
            byte[] vals =
            {
                (byte) message.Body[index++],
                (byte) message.Body[index++],
                (byte) message.Body[index++],
                (byte) message.Body[index++]
            };
            FunctionalVersion = BitConverter.ToSingle(vals, 0);

            byte[] vals2 =
            {
                (byte) message.Body[index++],
                (byte) message.Body[index++],
                (byte) message.Body[index++],
                (byte) message.Body[index++]
            };
            BaseCodeVersion = BitConverter.ToSingle(vals2, 0);

            BaseCodeChecksum = Message.CharArrayToUInt16(message.Body[index++], message.Body[index++]);

            if (FunctionalVersion < BiostackVersion.V3)
            {
                return;
            }

            // Only available in version 1.11 onwards
            DipSwitchSettings = (byte) message.Body[index++];
            PlateStatus = (byte) message.Body[index++];
            StackedHeight = Message.CharArrayToUInt16(message.Body[index++], message.Body[index++]);

            if (FunctionalVersion < BiostackVersion.V4)
            {
                return;
            }
            BasecodePartNumber = Message.CharArrayToUInt(
                message.Body[index++],
                message.Body[index++], 
                message.Body[index++],
                message.Body[index++]
                );
            LidHandlingModeEnabled = (byte) message.Body[index++] == 1;
        }
    }
}