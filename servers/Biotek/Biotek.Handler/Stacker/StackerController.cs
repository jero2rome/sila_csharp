using System;
using System.Diagnostics;
using System.Threading;
using Common.Logging;

namespace Biotek.Handler.Stacker
{
    public class StackerController : IStackerController
    {
        private const int RESET_TIMEOUT_MS = 60000;
        private static readonly ILog Log = LogManager.GetLogger<StackerController>();
        
        public bool StackMode { get; set; }

        public bool RestackMode { get; set; }

        public bool TeachMode
        {
            get => _teachMode;
            set => ToggleTeachMode(value);
        }

        public PlateState PlateState { get; private set; } = PlateState.FirstPlate;
        public InstrumentType Instrument { get; set; }
        public InstrumentInterface InstrumentInterface { get; set; }

        private readonly CommunicationController _comsController;
        private StackerInfo _info;
        private bool _teachMode = false;
        
        public StackerController(string port)
        {
            _comsController = new CommunicationController(port);
        }

        public void JogUp(int nSteps)
        {
            if (!TeachMode)
            {
                throw new StackerException("Cannot Jog the robot as teach mode is disabled");
            }

            Jog(Axis.Claw, -nSteps);
        }

        public void JogDown(int nSteps)
        {
            if (!TeachMode)
            {
                throw new StackerException("Cannot Jog the robot as teach mode is disabled");
            }

            Jog(Axis.Claw, nSteps);
        }

        public void TeachThis()
        {
            if (!TeachMode)
            {
                throw new StackerException("Cannot teach as teach mode is disabled");
            }

            _comsController.SendReceiveMessage(Command.STACKER_SAVE_CLAW_INTERFACE_POS);
            HomeAxis(Axis.Claw);
            HomeAxis(Axis.Gripper);
        }

        public void Initialize()
        {
            PlateState = PlateState.FirstPlate;
        }
        
        private void InitializeStacker()
        {
            GetStackerInfo();
            Home();            
        }
        
        private void ToggleTeachMode(bool teachMode)
        {
            if (teachMode == _teachMode)
            {
                return;
            }

            if (!_teachMode)
            {
                Log.Debug($"Teach mode enabled");
                InitializeStacker();
                SelectInstrument(Instrument, InstrumentInterface);
                HomeAxis(Axis.Claw);
                MoveAxisToPosition(Axis.Gripper, GripperPosition.PlateClear);
                _teachMode = true;
            }
            else
            {
                Log.Debug($"Teach mode disabled");
                HomeAxis(Axis.Claw);
                HomeAxis(Axis.Gripper);
                _teachMode = false;
            }
        }

        private void GetStackerInfo()
        {
            var response = _comsController.SendReceiveMessage(Command.STACKER_GET_INFO_EXCHANGE);
            _info = new StackerInfo(response);
            Log.Debug($"Stacker Info: {_info}");
        }

        private void Home()
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            _comsController.SendReceiveMessage(Command.STACKER_HOME_ALL_AXES);
            while (stopwatch.ElapsedMilliseconds < RESET_TIMEOUT_MS)
            {
                if (!IsStatusErrored())
                {
                    break;
                }
                Thread.Sleep(1000);
            }

            if (stopwatch.ElapsedMilliseconds >= RESET_TIMEOUT_MS)
            {
                throw new StackerException($"Stacker did not home before timeout (${RESET_TIMEOUT_MS / 1000} seconds).");
            }
        }
        
        private void SelectInstrument(InstrumentType instrument, InstrumentInterface instrumentInterface)
        {
            var msgBody = new char[18];
            msgBody[0] = Convert.ToChar((int) instrument & 0x00FF); // Lo byte
            msgBody[1] = Convert.ToChar((int) instrument >> 8); // Hi byte
            msgBody[2] = Convert.ToChar(instrumentInterface);
            _comsController.SendReceiveMessage(Command.STACKER_SELECT_INSTRUMENT, msgBody);
        }

        private void HomeAxis(Axis axis)
        {
            var msgBody = new char[18]; // Body is short device type and 16 reserved for future bytes

            msgBody[0] = Convert.ToChar((int) axis & 0x00FF); // Lo byte
            msgBody[1] = Convert.ToChar((int) axis >> 8); // Hi byte

            _comsController.SendReceiveMessage(Command.STACKER_HOME_ONE_AXIS, msgBody);
        }
        
        private void ResetStacker()
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            _comsController.SendReceiveMessage(Command.STACKER_RESET_STACKER);
            while (stopwatch.ElapsedMilliseconds < RESET_TIMEOUT_MS)
            {
                if (!IsStatusErrored())
                {
                    break;
                }
                Thread.Sleep(1000);
            }

            if (stopwatch.ElapsedMilliseconds >= RESET_TIMEOUT_MS)
            {
                throw new StackerException($"Stacker did not reset before timeout (${RESET_TIMEOUT_MS / 1000} seconds).");
            }
        }
        
        private bool IsStatusErrored()
        {
            try
            {
                _comsController.SendReceiveMessage(Command.STACKER_GET_SYSTEM_STATUS);
            }
            catch (StackerInstrumentException e)
            {
                return true;
            }
            return false;
        }
        
        /// <summary>
        /// Sends a selected device (axis) to a pre-defined destination (position), based on a selection.
        /// This only applies to the Claw, Wrist, and Gripper devices (and Carrier in Command Set 1.11)
        /// when present in the instrument.
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="gripperPosition"></param>
        /// <exception cref="NotImplementedException"></exception>
        private void MoveAxisToPosition(Axis axis, GripperPosition gripperPosition)
        {
            var body = new char[20];
            body[0] = Convert.ToChar((int) axis & 0x00FF); // Lo byte
            body[1] = Convert.ToChar((int) axis >> 8); // Hi byte
            body[2] = Convert.ToChar((int) gripperPosition & 0x00FF); // Lo byte
            body[3] = Convert.ToChar((int) gripperPosition >> 8); // Hi byte
            _comsController.SendReceiveMessage(Command.STACKER_SEND_AXIS_TO_POS, body);
        }


        /// <summary>
        /// Moves a selected axis (device) a given number of full motor steps.
        /// </summary>
        /// <param name="axis">Axis to move</param>
        /// <param name="numSteps">Number of steps to move +ive or -ive depending on direction</param>
        /// <exception cref="NotImplementedException"></exception>
        private void Jog(Axis axis, int numSteps)
        {
            // The body has 1 short, 1 long, and 16 reserved bytes
            var body = new char[22];
            body[0] = Convert.ToChar((int) axis & 0x00FF); // Lo byte
            body[1] = Convert.ToChar((int) axis >> 8); // Hi byte
            body[2] = Convert.ToChar(numSteps & 0x000000FF);
            body[3] = Convert.ToChar((numSteps & 0x0000FF00) >> 8);
            body[4] = Convert.ToChar((numSteps & 0x00FF0000) >> 16);
            body[5] = Convert.ToChar((numSteps & 0xFF000000) >> 24);
            _comsController.SendReceiveMessage(Command.STACKER_MOVE_AXIS_N_STEPS, body);
        }

        /// <summary>
        /// TODO: 
        /// </summary>
        /// <param name="height"></param>
        /// <exception cref="NotImplementedException"></exception>
        private void SetStackedHeight(int height)
        {
            throw new NotImplementedException("TODO: STACKER_SET_STACKED_HEIGHT - 0 is default");
        }

        /// <summary>
        /// Move plate from the Stacker 2 input to the carrier and then to the instrument.
        /// This function can change the PlateState to NoMorePlate if a NO_PLATE_ON_CARRIER error is encountered
        /// </summary>
        private void StackerV2ToInstrument()
        {
            try
            {
                _comsController.SendReceiveMessage(Command.STACKER_IN_TO_CARRIER);
            }
            catch (StackerInstrumentException e)
            {
                if (e.Code != BiostackError.NO_PLATE_ON_CARRIER_1 && e.Code != BiostackError.NO_PLATE_ON_CARRIER_2)
                {
                    throw;
                } 
                Log.Info($"No more plate");
                PlateState = PlateState.NoMorePlate;
                return;
            }
            _comsController.SendReceiveMessage(Command.STACKER_CARRIER_TO_INSTR);
        }

        public void LoadPlate()
        {
            if (PlateState == PlateState.FirstPlate)
            {
                if (IsStatusErrored()) // todo validate that this command can be executed if the stacker is not initialized
                {
                    Log.Warn("Stacker status is errored, resetting Stacker");
                    ResetStacker();
                }
                InitializeStacker();
            }
            if (_info.FunctionalVersion < BiostackVersion.V3)
            {
                if (PlateState == PlateState.MiddlePlate)
                {
                    _comsController.SendReceiveMessage(Command.STACKER_INSTR_TO_OUT);
                    if (PlateState == PlateState.NoMorePlate)
                    {
                        return;
                    }
                }
                if (PlateState == PlateState.FirstPlate)
                {
                    PlateState = PlateState.MiddlePlate;
                }
                StackerV2ToInstrument();
                return;
            }
            switch (PlateState)
            {
                case PlateState.FirstPlate:
                    break;
                case PlateState.MiddlePlate:
                    ExchangeAll();
                    break;
                case PlateState.LastPlate:
                    ExchangeLast();
                    StoreLast();
                    PlateState = PlateState.NoMorePlate;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        //TODO: modularize for different software versions
        /// <summary>
        /// Prepares the next plate 
        /// </summary>
        /// <exception cref="StackerInstrumentException"></exception>
        public void PrepareNext()
        {
            if (_info.FunctionalVersion < BiostackVersion.V3) // The Biostack 2 only have one carrier, so it cannot prepare the next plate
            {
                return;
            }
            
            if (PlateState == PlateState.FirstPlate)
            {
                QueueSource();
                PlateState = PlateState.MiddlePlate;
                return;
            }

            if (PlateState == PlateState.MiddlePlate)
            {
                try
                {
                    StoreAndQueue();
                }
                catch (StackerInstrumentException e)
                {
                    if (e.Code != BiostackError.NO_PLATE_ON_CARRIER_1) // todo should we ignore MISSING_LID error as well?
                    {
                        throw;
                    } 
                    Log.Info($"Last plate reached");
                    PlateState = PlateState.LastPlate;
                }
            }
        }

        public void Restack()
        {
            if (IsStatusErrored())
            {
                ResetStacker(); // todo should this function take the responsibility of making sure that the status is not errored? 
            }
            try
            {
                while (true)
                {
                    _comsController.SendReceiveMessage(Command.STACKER_OUT_TO_IN);
                }
            }
            catch (StackerInstrumentException e)
            {
                if (e.Code != BiostackError.NO_PLATE_ON_CARRIER)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Load First (1) Move a plate from the source stack to the instrument’s carrier (test for the presence of
        /// a plate in the source tray, and retract the stacker’s carrier when done).  If a barcode reader is
        /// installed and a read is indicated, the barcode of the plate being loaded on the instrument shall be read.
        ///
        /// - If no plates are in the input stack then this command will return a 0x1601 or 0x1602 error.
        /// </summary>
        private void Load()
        {
            Log.Debug("Loading first plate to instrument");
            CarrierTransferCommand(1);
        }

        /// <summary>
        /// Queue Source (2) Move a plate from the source stack to the stacker carrier source tray and check for
        /// the presence of a plate in the source tray.
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        private void QueueSource()
        {
            Log.Debug("Moving plate from source stack to stacker carrier");
            CarrierTransferCommand(2);
        }

        /// <summary>
        /// Exchange All (3) A fast exchange of plates between the stacker’s carrier and the instrument.
        /// This consists of moving a plate from the instrument’s carrier to the stacker’s carrier destination tray,
        /// moving a plate from the stacker carrier’s source tray to the instrument, and retracting the stacker’s
        /// carrier.
        ///
        ///     - If a barcode reader is installed and a read is indicated, the barcode of the plate being
        ///     loaded on the instrument shall be read.
        ///     - In Command Set 1.12 when lids are present, error response NO_PLATE_ON_CARRIER_1 (0x1601)
        ///     indicates the input stack is empty.
        ///     - The Store and Queue (4) command should not be issued, but the Exchange Last (5)
        ///     then Store Last (6) commands should be issued to finish the process.
        ///     - The 0x1601 response is not treated as an error by the basecode.
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        private void ExchangeAll()
        {
            Log.Debug("Exchanging plates");
            CarrierTransferCommand(3);
        }

        /// <summary>
        /// Store and Queue (4) A fast exchange of plates between the stacker’s carrier and the stacks.
        /// This consists of moving a plate from the stacker’s carrier destination tray to the destination
        /// stack and a plate from the source stack to the stacker carrier source tray (checks for presence
        /// of a source tray plate).  Error response NO_PLATE_ON_CARRIER_1 (0x1601) or MISSING_LID (0x1703)
        /// indicates the input stack is empty.  The MISSING_LID response is an error and the process should
        /// be stopped; otherwise, the Exchange Last (5) then Store Last (6) commands should be issued to finish
        /// the process.  The 0x1601 response is not treated as an error by the basecode.
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        private void StoreAndQueue()
        {
            Log.Debug("Storing and queuing plates");
            CarrierTransferCommand(4);
        }

        /// <summary>
        /// Exchange Last (5) A fast move of a plate from the instrument’s carrier to the stacker’s carrier
        /// destination tray.  A new plate is not loaded on the instrument and no plate is removed from the
        /// source stack.  Does nothing in Command Set 1.12 when lids are present.
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        private void ExchangeLast()
        {
            Log.Debug("Exchanging last plate");
            CarrierTransferCommand(5);
        }

        /// <summary>
        /// Store Last (6) A fast move of a plate from the stacker’s carrier destination tray to the destination
        /// stack.  No plate is removed from the source stack.
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        private void StoreLast()
        {
            Log.Debug("Storing last plate");
            CarrierTransferCommand(6);
        }

        private void CarrierTransferCommand(int action)
        {
            const int defaultInterface = 0;
            const int dontReadBarcode = 0;
            var body = new char[8];
            body[0] = Convert.ToChar(action);
            body[1] = Convert.ToChar(defaultInterface);
            body[2] = Convert.ToChar(dontReadBarcode);
            _comsController.SendReceiveMessage(Command.STACKER_DUAL_CAR_CNTRL_BC, body);
        }
    }
}