using Biotek.Handler.Stacker;

namespace Biotek.Handler.Washer
{
    public class SynchronousStackerController : IStackerController
    {
        private readonly object _comLock = new object();
        private readonly IStackerController _stackerController;
        
        public bool RestackMode
        {
            get
            {
                lock (_comLock) return _stackerController.RestackMode;
            }
            set
            {
                lock (_comLock) _stackerController.RestackMode = value;  
            }
        }

        public bool StackMode
        {
            get
            {
                lock (_comLock) return _stackerController.StackMode;
            }
            set
            {
                lock (_comLock) _stackerController.StackMode = value;  
            }
        }

        public bool TeachMode
        {
            get
            {
                lock (_comLock) return _stackerController.TeachMode;
            }
            set
            {
                lock (_comLock) _stackerController.TeachMode = value;  
            }
        }

        public PlateState PlateState
        {
            get
            {
                lock (_comLock) return _stackerController.PlateState;
            }
        }

        public Stacker.InstrumentType Instrument
        {
            get
            {
                lock (_comLock) return _stackerController.Instrument;
            }
            set
            {
                lock (_comLock) _stackerController.Instrument = value;  
            }
        }

        public InstrumentInterface InstrumentInterface
        {
            get
            {
                lock (_comLock) return _stackerController.InstrumentInterface;
            }
            set
            {
                lock (_comLock) _stackerController.InstrumentInterface = value;  
            }
        }

        public SynchronousStackerController(IStackerController stackerController)
        {
            this._stackerController = stackerController;
        }
        public void JogUp(int steps)
        {
            lock (_comLock)
            {
                _stackerController.JogUp(steps);
            }
        }

        public void JogDown(int steps)
        {
            lock (_comLock)
            {
                _stackerController.JogDown(steps);
            }
        }

        public void TeachThis()
        {
            lock (_comLock)
            {
                _stackerController.TeachThis();
            }
        }

        public void Initialize()
        {
            lock (_comLock)
            {
                _stackerController.Initialize();
            }
        }

        public void LoadPlate()
        {
            lock (_comLock)
            {
                _stackerController.LoadPlate();
            }
        }

        public void PrepareNext()
        {
            lock (_comLock)
            {
                _stackerController.PrepareNext();
            }
        }

        public void Restack()
        {
            lock (_comLock)
            {
                _stackerController.Restack();
            }
        }
    }
}