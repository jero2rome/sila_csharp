namespace Biotek.Handler.Washer
{
    /// <summary>
    /// Interface according to BTILHCRunner version 2.20.3.0
    /// </summary>
    public interface IBtilhcRunner
    {
        short LHC_SetCommunications(string com);
        short LHC_TestCommunications();
        short LHC_SetRunnerThreading(short boolean);
        short LHC_PauseProtocol();
        short LHC_AbortProtocol();
        short LHC_LoadProtocolFromFlash(string protocol);
        short LHC_ValidateProtocol(bool validate);
        short LHC_OverrideValidation(short boolean);
        short LHC_RunProtocol();
        short LHC_SetProductName(string product);
        short LHC_GetProtocolStatus();
        short LHC_GetLastErrorCode();
        string LHC_GetErrorString(short errorCode);
        string LHC_GetOnBoardProtocolName(short index);
        short LHC_GetOnBoardProtocolCount(ref short count);
    }
}