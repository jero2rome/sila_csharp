﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Biotek.Handler.Washer
{
    /// <summary>
    /// This is the interface definition to be exposed to SiLA to interact with the instrument
    /// </summary>
    public interface IWasherController : IDisposable
    {
        // TODO: add further API methods which SiLA must be able to interact with
        void Initialize();
        Task RunProtocol(string protocolName, CancellationToken cancellationToken);
        IEnumerable<string> GetProtocols();
    }
}