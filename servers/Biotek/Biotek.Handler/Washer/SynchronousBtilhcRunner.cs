namespace Biotek.Handler.Washer
{
    public class SynchronousBtilhcRunner : IBtilhcRunner
    {
        private readonly object _comLock = new object();
        private readonly IBtilhcRunner _btilhcRunner;

        public SynchronousBtilhcRunner(IBtilhcRunner btilhcRunner)
        {
            _btilhcRunner = btilhcRunner;
        }

        public short LHC_SetCommunications(string com)
        {
            lock (_comLock)
            {
                return _btilhcRunner.LHC_SetCommunications(com);
            }
        }

        public short LHC_TestCommunications()
        {
            lock (_comLock)
            {
                return _btilhcRunner.LHC_TestCommunications();
            }
        }

        public short LHC_SetRunnerThreading(short boolean)
        {
            lock (_comLock)
            {
                return _btilhcRunner.LHC_SetRunnerThreading(boolean);
            }
        }

        public short LHC_PauseProtocol()
        {
            lock (_comLock)
            {
                return _btilhcRunner.LHC_PauseProtocol();
            }
        }

        public short LHC_AbortProtocol()
        {
            lock (_comLock)
            {
                return _btilhcRunner.LHC_AbortProtocol();
            }
        }

        public short LHC_LoadProtocolFromFlash(string protocol)
        {
            lock (_comLock)
            {
                return _btilhcRunner.LHC_LoadProtocolFromFlash(protocol);
            }
        }

        public short LHC_ValidateProtocol(bool validate)
        {
            lock (_comLock)
            {
                return _btilhcRunner.LHC_ValidateProtocol(validate);
            }
        }

        public short LHC_OverrideValidation(short boolean)
        {
            lock (_comLock)
            {
                return _btilhcRunner.LHC_OverrideValidation(boolean);
            }
        }

        public short LHC_RunProtocol()
        {
            lock (_comLock)
            {
                return _btilhcRunner.LHC_RunProtocol();
            }
        }

        public short LHC_SetProductName(string product)
        {
            lock (_comLock)
            {
                return _btilhcRunner.LHC_SetProductName(product);
            }
        }

        public short LHC_GetProtocolStatus()
        {
            lock (_comLock)
            {
                return _btilhcRunner.LHC_GetProtocolStatus();
            }
        }

        public short LHC_GetLastErrorCode()
        {
            lock (_comLock)
            {
                return _btilhcRunner.LHC_GetLastErrorCode();
            }
        }

        public string LHC_GetErrorString(short errorCode)
        {
            lock (_comLock)
            {
                return _btilhcRunner.LHC_GetErrorString(errorCode);
            }
        }

        public string LHC_GetOnBoardProtocolName(short index)
        {
            lock (_comLock)
            {
                return _btilhcRunner.LHC_GetOnBoardProtocolName(index);
            }
        }

        public short LHC_GetOnBoardProtocolCount(ref short count)
        {
            lock (_comLock)
            {
                return _btilhcRunner.LHC_GetOnBoardProtocolCount(ref count);
            }
        }
    }
}