namespace Biotek.Handler.Washer
{
    public class WasherInternalException : WasherException
    {
        public WasherInternalException(string message) : base(message)
        {
        }
    }
}