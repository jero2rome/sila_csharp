using System;
using System.Threading.Tasks;
using Biotek.Handler.Stacker;
using Biotek.Handler.Washer;
using Common.Logging;
using Grpc.Core;
using Sila2.Ch.Unitelabs.Core.Initializationcontroller.V1;
using Sila2.Utils;

namespace Biotek.Sila
{
    public class InitializationControllerImpl : InitializationController.InitializationControllerBase
    {
        private readonly IWasherController _washerController;
        private readonly IStackerController _stackerController;
        private readonly ILog Log = LogManager.GetLogger<InitializationControllerImpl>();

        public InitializationControllerImpl(IWasherController washerController, IStackerController stackerController)
        {
            _washerController = washerController;
            _stackerController = stackerController;
        }

        public override Task<Initialize_Responses> Initialize(Initialize_Parameters request, ServerCallContext context)
        {
            try
            {
                _stackerController.Initialize();
                _washerController.Initialize();
            }
            catch (Exception e)
            {
                Log.Error(e);
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateUndefinedExecutionError(e.Message));
            }
            return Task.FromResult(new Initialize_Responses());
        }
    }
}