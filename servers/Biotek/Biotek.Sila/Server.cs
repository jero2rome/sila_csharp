using Biotek.Handler.Stacker;
using Biotek.Handler.Washer;
using Common.Logging;
using Sila2.Ch.Unitelabs.Core.Initializationcontroller.V1;
using Sila2.Ch.Unitelabs.Core.Programcontroller.V1;
using Sila2.Server;
using StackerController = Sila2.Ch.Unitelabs.None.Stackercontroller.V1.StackerController;
using Grpc.Core;

namespace Biotek.Sila
{
    using System.Net.NetworkInformation;
    using Sila2;

    /// <summary>
    /// Implements a SiLA 2 Server that provides a GreetingProvider and a TemperatureController feature.
    /// </summary>
    public class Server : SiLA2Server
    {
        private static readonly ILog Logger = LogManager.GetLogger<Server>();

        public Server(int portNumber, NetworkInterface networkInterface, string configFile, IWasherController washerController, IStackerController stackerController, ServerCredentials credentials)
            : base(new ServerInformation(
                    "BioTek Server",
                    "Biotek Server that implements the ability to interact with Biotek Liquid Handlers and stackers",
                    "www.biotek.com",
                    "1.0"),
                portNumber,
                networkInterface,
                credentials,
                configFile)
        {
            ReadFeature("features/ProgramController.sila.xml");
            var programControllerImpl = new ProgramControllerImpl(washerController, stackerController);
            GrpcServer.Services.Add(ProgramController.BindService(programControllerImpl));
            ReadFeature("features/InitializationController.sila.xml");
            var initializationControllerImpl = new InitializationControllerImpl(washerController, stackerController);
            GrpcServer.Services.Add(InitializationController.BindService(initializationControllerImpl));
            ReadFeature("features/StackerController.sila.xml");
            var stackerControllerImpl = new StackerControllerImpl(stackerController);
            GrpcServer.Services.Add(StackerController.BindService(stackerControllerImpl));
        }
    }
}