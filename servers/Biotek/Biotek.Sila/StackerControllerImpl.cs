using System;
using System.Threading.Tasks;
using Biotek.Handler.Stacker;
using Common.Logging;
using Grpc.Core;
using Sila2.Ch.Unitelabs.None.Stackercontroller.V1;
using Sila2.Utils;
using Boolean = Sila2.Org.Silastandard.Boolean;
using StackerController = Sila2.Ch.Unitelabs.None.Stackercontroller.V1.StackerController;


namespace Biotek.Sila
{
    public class StackerControllerImpl : StackerController.StackerControllerBase
    {
        private static readonly ILog Log = LogManager.GetLogger<StackerControllerImpl>();
        private readonly IStackerController _stackerController;

        public StackerControllerImpl(IStackerController stackerController)
        {
            _stackerController = stackerController;
        }

        public override Task<TeachMode_Responses> TeachMode(TeachMode_Parameters request, ServerCallContext context)
        {
            Utils.InvokeAction(() => _stackerController.TeachMode = !_stackerController.TeachMode);
            return Task.FromResult(new TeachMode_Responses());
        }

        public override Task<TeachThis_Responses> TeachThis(TeachThis_Parameters request, ServerCallContext context)
        {
            Utils.InvokeAction(() => _stackerController.TeachThis());
            return Task.FromResult(new TeachThis_Responses());
        }

        public override Task<JogUp_Responses> JogUp(JogUp_Parameters request, ServerCallContext context)
        {
            Utils.InvokeAction(() => _stackerController.JogUp((int) request.NumberOfSteps.Value));
            return Task.FromResult(new JogUp_Responses());
        }

        public override Task<JogDown_Responses> JogDown(JogDown_Parameters request, ServerCallContext context)
        {
            Utils.InvokeAction(() => _stackerController.JogDown((int) request.NumberOfSteps.Value));
            return Task.FromResult(new JogDown_Responses());
        }

        public override Task<EnableStacker_Responses> EnableStacker(EnableStacker_Parameters request, ServerCallContext context)
        {
            Utils.InvokeAction(() => _stackerController.StackMode = true);
            return Task.FromResult(new EnableStacker_Responses());
        }

        public override Task<DisableStacker_Responses> DisableStacker(DisableStacker_Parameters request, ServerCallContext context)
        {
            Utils.InvokeAction(() => _stackerController.StackMode = false);
            return Task.FromResult(new DisableStacker_Responses());
        }

        public override Task<EnableRestacking_Responses> EnableRestacking(EnableRestacking_Parameters request, ServerCallContext context)
        {
            Utils.InvokeAction(() => _stackerController.RestackMode = true);
            return Task.FromResult(new EnableRestacking_Responses());
        }

        public override Task<DisableRestacking_Responses> DisableRestacking(DisableRestacking_Parameters request, ServerCallContext context)
        {
            Utils.InvokeAction(() => _stackerController.RestackMode = false);
            return Task.FromResult(new DisableRestacking_Responses());
        }

        public override Task<Get_StackMode_Responses> Get_StackMode(Get_StackMode_Parameters request, ServerCallContext context)
        {
            try
            {
                return Task.FromResult(new Get_StackMode_Responses
                {
                    StackMode = new Boolean
                    {
                        Value = _stackerController.StackMode
                    }
                });
            }
            catch (Exception e)
            {
                Log.Warn(e);
                ErrorHandling.RaiseSiLAError(
                    ErrorHandling.CreateUndefinedExecutionError(
                        ErrorHandling.FlattenExceptionsMessageToString(e)));
                throw;
            }
        }

        public override Task<Get_RestackingMode_Responses> Get_RestackingMode(Get_RestackingMode_Parameters request, ServerCallContext context)
        {
            try
            {
                return Task.FromResult(new Get_RestackingMode_Responses
                {
                    RestackingMode = new Boolean
                    {
                        Value = _stackerController.RestackMode
                    }
                });
            }
            catch (Exception e)
            {
                Log.Warn(e);
                ErrorHandling.RaiseSiLAError(
                    ErrorHandling.CreateUndefinedExecutionError(
                        ErrorHandling.FlattenExceptionsMessageToString(e)));
                throw;
            }
        }
    }
}