using System;
using Common.Logging;
using Common.Logging.Configuration;
using Sila2.Utils;

namespace Biotek.Sila
{
    public static class Utils
    {
        private static readonly ILog Log = LogManager.GetLogger<Biotek.Sila.Server>();

        /// <summary>
        /// Wraps the call and generates a SiLA error for all exceptions
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public static void InvokeAction(ArgUtils.Action action)
        {
            try
            {
                action.Invoke();
            }
            catch (Exception e)
            {
                Log.Warn(e);
                ErrorHandling.RaiseSiLAError(
                    ErrorHandling.CreateUndefinedExecutionError(
                        ErrorHandling.FlattenExceptionsMessageToString(e)));
            }
        }
    }
}