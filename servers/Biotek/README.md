# Biotek LHC SiLA Server
SiLA Sever to interact with the LHC Biotek liquid handling software. This is in development 
but allows for remote control of Biotek liquid handlers.

## Supported Instruments
In principle this SiLA server should be able to work with the following list of liquid handler's from BioTek.

|   Model    | Tested/Not Tested | `--type` flag value | 
| ---------  | ----------------- |---------------------|
| 405 LS     | Yes               | `405 TS/LS` |
| 405 TS     | Yes               | `405 TS/LS` |
| 50 TS      | No                | `50 TS` |
| EL406      | No                | `EL406` |
| ELx405     | No                | `ELx405` |
| MicroFlo Select | No           | `MicroFlo Select` |
| MultiFlo   | No                | `MultiFlo` |
| MultiFloFX | No                |  `MultiFloFX` |

If you have tested one of the instruments that are NOT tested then please feel free to create an request via [issues](https://gitlab.com/SiLA2/sila_csharp/issues) to update this README.md.

## Installation
Open up PowerShell as Administrator,  You must ensure `Get-ExecutionPolicy` is not `Restricted`. We suggest using `Bypass` to bypass the policy to get things installed or `AllSigned` for quite a bit more security.

Run `Get-ExecutionPolicy`. If it returns `Restricted`, then run `Set-ExecutionPolicy AllSigned` or `Set-ExecutionPolicy Bypass -Scope Process`.

1. Install Chocolatey (skip this step if you have Chocolatey installed already)

```PowerShell
Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
```

2. Add the SiLA NuGet repository to the Chocolatey sources (skip this step if done so already)

```PowerShell
choco source add -n=sila -s="https://nexus.unitelabs.ch/repository/nuget-sila-development/"
```

3. Install the Biotek SiLA Driver

```PowerShell
choco install sila-biotek -y
```

When prompted to install press **A** and then **Enter** to skip all prompts and install quietly 

## Running The Server with Instrument + Stacker
Assuming the server is installed here are the steps to start a server and remotely command a Biotek instrument model EL 406, on COM4 and a Biostack on COM5.

1. Start the SiLA Browser. Instructions can be found [here](https://gitlab.com/SiLA2/sila_base/wikis/SiLA%20Browser%20Quickstart)

2. Make sure the Stacker/Washer are operational and configured according to the program loaded which you plan to run.

3. Connect the PC to the stacker and/or washer via USB: 
    - PC -> Stacker (via USB)
    - PC -> Instrument (via USB)

4. Identify the COM port on which the instrument is connected to the PC. This is best done via the LHC software

    e.g.
        - Stacker (COM5)
        - Instrument (COM4)

5. Start the Biotek.ServerApp with Stacker enabled

```PowerShell
Biotek.ServerApp.exe -t "EL406" --washer-com-port="COM4" --stacker-com-port="COM5" --dll-path 'C:\Program Files (x86)\BioTek\Liquid Handling Control 2.22\BTILHCRunner.dll' -n <network-interface> -w
```

Note: for `--dll-path` please specify the correct path to the `BTILHCRunner.dll` that corresponds to your LHC installation

5. Open the SiLA Browser and Scan Network to find the server

6. Call the Initialize command on the *InitializationController*

7. Call the *Run Program* command with the name of the program to run (the programs are listed in the *List Programs* property) 



## Deployment
To deploy this server the following steps are required, in Admin mode

1. Build the Server application

```PowerShell
cd Biotek.ServerApp
dotnet restore
dotnet zip -f net472 -r win-x86
```

2. Pack the choco package

```PowerShell
cd ../chocolatey
choco pack
```

3. Make sure to add the API key for writing to the SiLA NuGet repository

```PowerShell
choco apikey -s "https://nexus.unitelabs.ch/repository/nuget-sila-development" -k="<api-key-goes-here>"
```

4. Publish the Chocolatey NuGet package on the SiLA NuGet repository

```PowerShell
choco push .\sila-biotek.0.2.5.nupkg -s "https://nexus.unitelabs.ch/repository/nuget-sila-development/" --force
```