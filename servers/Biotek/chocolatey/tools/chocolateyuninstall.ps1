﻿$ErrorActionPreference = 'Stop';
Write-Warning "Will back up files to $appDataDir"
 
if ([System.IO.Directory]::Exists($configFile)) {
    Copy-Item $configFile -Destination $appDataDir -Recurse -Force
    Write-Warning "To prevent accidental data loss the server configuration has been backed up $appDataDir"
} else {
    Write-Warning "There was no '$configFile' found to backup"
}