# SiLA Servers
The SiLA servers listed are real open source implementations, meant for sharing and adoption increase.

| Name                                     | Description                                                 |
| -----------------------------------------| ----------------------------------------------------------- |
| [Biotek](servers/Biotek)        | Server for Biotek instruments that use the LHC runner and Biostack stackers |
| [RobotControl](servers/RobotControl)      | Server which implements SiLA features for robot arms and mobile lab robots |

## Arguments
The SiLA Java servers supports the following arguments by default.
There might also be server specific arguments.


| Short flag | Long flag            | Default value  | Description |
|------------|----------------------|----------------|-------------|
| -p         | --port"              | 50052          | Port to connect server to. |
| -c         | --config             | None           | Path to file to store/load server configuration. |
| -n         | --network-interface  | None           | Network interface/IP Address/ Mask to use for discovery (e.g. lo, wlp1s0, eth0, 192.168.1.0/24, 192.168.1.0)")] |
| -s         | --simulation"        | N/A            | Run the server in simulation |
| -l         | --list-interfaces",  | N/A            | List available network interfaces |
| -w         | --save",             | N/A            | Save the command line arguments to config.json in path of executable |

## Building and Running
SiLA C# Building instructions are [here](..//README.md).

To install the package, simply use `dotnet`:
```bash
dotnet build
```
    
Once the server is compiled, the server DLL can be found in the `bin/Debug/netcoreapp2.0` usually name as follow `<SERVER_NAME>.ServerApp.dll` 

The server can then be executed using .NET 2.0 with the following command:

```bash
dotnet  bin/Debug/netcoreapp2.0/<SERVER_NAME>.ServerApp.dll
```
    
Display help & usage:
```bash
dotnet bin/Debug/netcoreapp2.0/<SERVER_NAME>.ServerApp.dll -h
```
You can run the server and client on any host on your local network with discovery enabled:
```bash
dotnet bin/Debug/netcoreapp2.0/<SERVER_NAME>.ServerApp.dll -n local
```
The jar is usually packaged using the spring boot packager but this can be done differently in your own code.

### Running Multiple SiLA Servers

If you want to run multiple instance on the same machine, the port will be assigned automatically. 
You will have to define a different configuration file though by providing the path with the `-c` flag,
as you want to have different UUID and SiLA Server Names for different instances.

### SiLA Server persistence (UUID and Name)
To persist both `name` and `uuid` pertaining to SiLAServer Name and UUID, we use an optional configuration 
argument `-c [FILE.json]`.This config file will allow for persistence through reboots of the SiLA 2 Server, 
if the flag correctly points to the same `config.json` file. This will be a JSON file, ending with file extension `.json`.

To use this option you specify argument `-c [./PATH/config.json]`. Remember a file will be created in a relative path 
to your current execution path. In our example a `config.json` file will be created in `./config.json`, rather 
then the location of the `.dll`
 
Example:

```bash
dotnet <SERVER_NAME>.dll -c config.json
```
