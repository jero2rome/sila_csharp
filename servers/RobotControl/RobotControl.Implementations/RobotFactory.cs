using System;
using RobotControl.Implementations;

namespace RobotControl
{
    /// <summary>
    /// Factory to create instance of type IRobotController 
    /// </summary>
    public class RobotFactory
    {
        private readonly RobotSimulator robotSimulator;

        //TODO real implementation

        public RobotFactory()
        {
            robotSimulator = new RobotSimulator();
        }

        public void Create(out IRobotController robotController, out IRobotModelsManager robotManager,
            bool simulation = true)
        {
            if (!simulation)
                throw new InvalidOperationException(
                    "Robot cannot start in Real mode, as there is no support available");
            robotController = robotSimulator;
            robotManager = robotSimulator;
            //TODO: implementation for REAL mode
        }
    }
}