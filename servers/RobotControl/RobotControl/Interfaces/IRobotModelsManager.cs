using System.Collections.Generic;
using Sila2.Ch.Unitelabs.Robot.Devicecalibrationservice.V1;
using Sila2.Ch.Unitelabs.Robot.Platecalibrationservice.V1;

// Rename to RobotCalibrationAndTeachingService
namespace RobotControl
{
    /// <summary>
    /// Interface to be implemented by robot component responsible for storing Location, Device, and Labware data
    ///
    /// Useful when:
    /// - Calibrating a device with repect to the robot and its reference poses (some optional: for mobile robots) 
    /// - Calibrating plate types into the robot to be able to pick them up correctly 
    /// - Calibrate the robot's pose to reach a device (optional: only for mobile robots)
    /// </summary>
    public interface IRobotModelsManager
    {
        List<string> TaughtPositions { get; }
        Dictionary<string, DataType_PlateDefinition.Types.PlateDefinition_Struct> PlateTypes { get; }
        
        /// <summary>
        /// Set a Device with specified numSites, height and approach distance or creates it if it does not exist already
        /// </summary>
        /// <param name="deviceName">name of the device</param>
        /// <param name="numSites">number of sites</param>
        /// <param name="height">approach height</param>
        /// <param name="approach">approach distance to the Device</param>
        void SetDevice(string deviceName, int numSites, int height, int approach);

        /// <summary>
        /// Update a Device with specified numSites, height and approach distance
        /// </summary>
        /// <param name="deviceName">name of the device</param>
        /// <param name="numSites">number of sites</param>
        /// <param name="height">approach height</param>
        /// <param name="approach">approach distance to the Device</param>
        void DeleteDevice(string deviceName);

        /// <summary>
        /// Get list of devices
        /// </summary>
        /// <returns>devices</returns>
        List<DataType_DeviceProperties.Types.DeviceProperties_Struct> Devices();

        /// <summary>
        /// This determines the transformation between Device tag and Instrument Nest
        /// tag [Symbol] Output is 3x3 matrix (Rotation) and 1x3 matrix (Translation).  
        /// </summary>
        /// <param name="deviceName">name of device</param>
        void CalibrateMarkers(string deviceName);
        
        /// <summary>
        /// Store the current pose of the robot arm as the target pose for the given site of the Device
        /// </summary>
        /// <param name="deviceName">name of device</param>
        /// <param name="siteIndex">index of site in device</param>
        /// <param name="delidSite">true if the site is a de-lid site, false if its a plate site</param>
        void CalibrateSite(string deviceName, int siteIndex, bool delidSite = false);

        /// <summary>
        /// Assigns a location, as the current physical position/pose of the robot to the specified Device
        /// If there is no location already defined that uses this device, it will be automatically created.
        ///
        /// TODO: Should also handle the case for manual teaching of robot
        /// </summary>
        /// <param name="deviceName">name of device</param>
        void TeachDevice(string deviceName);
        
        void TeachPosition(string positionValue);

        /// <summary>
        /// Delete taught location given device name
        /// </summary>
        /// <param name="deviceName">name of device</param>
        void DeleteLocation(string deviceName);
        
        // Plate Type - still talking about models here ?
        void CreatePlateType(DataType_PlateDefinition.Types.PlateDefinition_Struct plateType);
        void DeletePlateType(string plateTypeName);
    }
}