using System;
using System.Threading.Tasks;
using Common.Logging;
using Grpc.Core;
using Sila2.Utils;

namespace RobotControl.Impl
{
    using Sila2.Ch.Unitelabs.Robot.Devicecalibrationservice.V1;
    using SiLAFramework = Sila2.Org.Silastandard;

    public class DeviceCalibrationServiceImpl : DeviceCalibrationService.DeviceCalibrationServiceBase
    {
        private static ILog _log = LogManager.GetLogger<DeviceCalibrationServiceImpl>();
        private readonly IRobotModelsManager _robotModelsManager;

        public DeviceCalibrationServiceImpl(IRobotModelsManager robotModelsManager)
        {
            _robotModelsManager = robotModelsManager;
        }

        public override Task<CreateDevice_Responses> CreateDevice(CreateDevice_Parameters request,
            ServerCallContext context)
        {
            var deviceProperties = request.DeviceProperties.DeviceProperties;
            try
            {
                SynchronizationController.Execute(() =>
                {
                    _robotModelsManager.SetDevice(
                        deviceProperties.DeviceName.Value,
                        (int) deviceProperties.NumberofSites.Value,
                        (int) deviceProperties.AccessHeight.Value,
                        (int) deviceProperties.ApproachDistance.Value); 
                });
            }
            catch (Exception e)
            {
                _log.Error(e);
                var error = ErrorHandling.CreateDefinedExecutionError("UnhealthyState", e.Message);
                ErrorHandling.RaiseSiLAError(error);
            }

            return Task.FromResult(new CreateDevice_Responses());
        }

        public override Task<UpdateDevice_Responses> UpdateDevice(UpdateDevice_Parameters request,
            ServerCallContext context)
        {
            try
            {
                SynchronizationController.Execute(() =>
                {
                    var deviceProperties = request.DeviceProperties.DeviceProperties;
                    _robotModelsManager.SetDevice(
                        deviceProperties.DeviceName.Value,
                        (int) deviceProperties.NumberofSites.Value,
                        (int) deviceProperties.AccessHeight.Value,
                        (int) deviceProperties.ApproachDistance.Value);

                });
            }
            catch (Exception e)
            {
                _log.Error(e);
                var error = ErrorHandling.CreateDefinedExecutionError("UnhealthyState", e.Message);
                ErrorHandling.RaiseSiLAError(error);
            }

            return Task.FromResult(new UpdateDevice_Responses());
        }

        public override Task<CalibrateSite_Responses> CalibrateSite(CalibrateSite_Parameters request,
            ServerCallContext context)
        {
            CalibrateSite(request.Site.Site);

            return Task.FromResult(new CalibrateSite_Responses());
        }

        public override Task<CalibrateDelidSite_Responses> CalibrateDelidSite(CalibrateDelidSite_Parameters request, ServerCallContext context)
        {
            CalibrateSite(request.Site.Site, true);
            return Task.FromResult(new CalibrateDelidSite_Responses());
        }

        private void CalibrateSite(DataType_Site.Types.Site_Struct site, bool delidSite = false)
        {
            try
            {
                SynchronizationController.Execute(() =>
                {
                    _robotModelsManager.CalibrateSite(
                        site.Device.Value,
                        (int) site.SiteIndex.Value, delidSite);
                });
            }
            catch (Exception e)
            {
                _log.Error(e);
                var error = ErrorHandling.CreateDefinedExecutionError("UnhealthyState", e.Message);
                ErrorHandling.RaiseSiLAError(error);
            }
        }

        public override Task<CalibrateMarkers_Responses> CalibrateMarkers(CalibrateMarkers_Parameters request,
            ServerCallContext context)
        {
            try
            {
                SynchronizationController.Execute(() =>
                {
                    _robotModelsManager.CalibrateMarkers(
                        request.DeviceName.Value);
                });
            }
            catch (Exception e)
            {
                _log.Error(e);
                var error = ErrorHandling.CreateDefinedExecutionError("UnhealthyState", e.Message);
                ErrorHandling.RaiseSiLAError(error);
            }

            return Task.FromResult(new CalibrateMarkers_Responses());
        }

        public override Task<DeleteDevice_Responses> DeleteDevice(DeleteDevice_Parameters request,
            ServerCallContext context)
        {
            try
            {
                SynchronizationController.Execute(() =>
                {
                    _robotModelsManager.DeleteDevice(
                        request.DeviceName.Value);
                });
            }
            catch (Exception e)
            {
                _log.Error(e);
                var error = ErrorHandling.CreateDefinedExecutionError("UnhealthyState", e.Message);
                ErrorHandling.RaiseSiLAError(error);
            }

            return Task.FromResult(new DeleteDevice_Responses());
        }

        public override Task<Get_DeviceProperties_Responses> Get_DeviceProperties(
            Get_DeviceProperties_Parameters request, ServerCallContext context)
        {
            var response = new Get_DeviceProperties_Responses();
            try
            {
                SynchronizationController.Execute(() =>
                {
                    _robotModelsManager.Devices().ForEach(
                        device => response.DeviceProperties.Add(new DataType_DeviceProperties { DeviceProperties = device}));
                });
            }
            catch (Exception e)
            {
                _log.Error(e);
                var error = ErrorHandling.CreateDefinedExecutionError("UnhealthyState", e.Message);
                ErrorHandling.RaiseSiLAError(error);
            }

            return Task.FromResult(response);
        }
    }
} 