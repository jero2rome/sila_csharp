using System;
using System.Threading.Tasks;
using Common.Logging;
using Grpc.Core;
using Sila2.Ch.Unitelabs.Core.Initializationcontroller.V1;
using Sila2.Utils;

namespace RobotControl.Impl
{
    public class InitializationControllerImpl : InitializationController.InitializationControllerBase
    {
        private static readonly ILog _log = LogManager.GetLogger<InitializationControllerImpl>();
        private readonly IRobotController _robotController;

        public InitializationControllerImpl(IRobotController robotController)
        {
            _robotController = robotController;
        }
        
        public override Task<Initialize_Responses> Initialize(Initialize_Parameters request, ServerCallContext context)
        {
            try
            {
                SynchronizationController.Execute(() =>
                {
                    _robotController.Initialize().Wait();
                    _log.Info("Initialized!");
                });
            }
            catch (Exception e)
            {
                _log.Error(e);
                ErrorHandling.RaiseSiLAError(
                    ErrorHandling.CreateUndefinedExecutionError("Failed to initialize robot, " + e.Message));
            }

            return Task.FromResult(new Initialize_Responses());
        }
    }
}