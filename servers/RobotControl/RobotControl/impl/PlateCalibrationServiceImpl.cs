using System;
using System.Threading.Tasks;
using Common.Logging;
using Grpc.Core;
using Sila2.Ch.Unitelabs.Robot.Platecalibrationservice.V1;
using Sila2.Utils;

namespace RobotControl.Impl
{
    public class PlateCalibrationServiceImpl : Sila2.Ch.Unitelabs.Robot.Platecalibrationservice.V1.PlateCalibrationService.PlateCalibrationServiceBase
    {
        private static readonly ILog _log = LogManager.GetLogger<PlateCalibrationServiceImpl>();
        private readonly IRobotModelsManager _robotModelsManager;

        public PlateCalibrationServiceImpl(IRobotModelsManager robotModelsManager)
        {
            _robotModelsManager = robotModelsManager;
        }

        public override Task<CreatePlateType_Responses> CreatePlateType(CreatePlateType_Parameters request,
            ServerCallContext context)
        {
            try
            {
                var newPlateType = request.PlateType.PlateDefinition;
                _robotModelsManager.CreatePlateType(newPlateType);
                return Task.FromResult(new CreatePlateType_Responses());
            }
            catch (Exception e)
            {
                _log.Warn(e);
                var error = ErrorHandling.CreateUndefinedExecutionError(e.Message);
                ErrorHandling.RaiseSiLAError(error);
            }

            return null;
        }

        public override Task<DeletePlateType_Responses> DeletePlateType(DeletePlateType_Parameters request,
            ServerCallContext context)
        {
            try
            {
                _robotModelsManager.DeletePlateType(request.PlateTypeName.Value);
                return Task.FromResult(new DeletePlateType_Responses());
            }
            catch (Exception e)
            {
                _log.Warn(e);
                var error = ErrorHandling.CreateUndefinedExecutionError(e.Message);
                ErrorHandling.RaiseSiLAError(error);
            }
            return null;
            
        }

        public override Task<Get_PlateTypes_Responses> Get_PlateTypes(Get_PlateTypes_Parameters request,
            ServerCallContext context)
        {
            try
            {
                var response = new Get_PlateTypes_Responses();
                foreach (var plateType in _robotModelsManager.PlateTypes.Values)
                {
                    response.PlateTypes.Add(new DataType_PlateDefinition {PlateDefinition = plateType});
                }
                return Task.FromResult(response);
            }
            catch (Exception e)
            {
                _log.Warn(e);
                var error = ErrorHandling.CreateUndefinedExecutionError(e.Message);
                ErrorHandling.RaiseSiLAError(error);
            }
            return null;
        }
    }
}