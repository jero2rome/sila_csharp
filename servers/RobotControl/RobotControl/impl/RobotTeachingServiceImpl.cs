using System;
using System.Threading.Tasks;
using Common.Logging;
using Grpc.Core;
using Sila2.Utils;

namespace RobotControl.Impl
{
    using Sila2.Ch.Unitelabs.Robot.Robotteachingservice.V1;

    public class RobotTeachingServiceImpl : RobotTeachingService.RobotTeachingServiceBase
    {
        private static readonly ILog _log = LogManager.GetLogger<RobotControllerImpl>();

        private readonly IRobotController _robotController;
        private readonly IRobotModelsManager _robotManager;

        public RobotTeachingServiceImpl(IRobotController robotController, IRobotModelsManager robotManager)
        {
            _robotController = robotController;
            _robotManager = robotManager;
        }

        public override Task<TeachMode_Responses> TeachMode(TeachMode_Parameters request, ServerCallContext context)
        {
            try
            {
                SynchronizationController.Execute(() => { _robotController.TeachMode = request.On.Value; });
            }
            catch (Exception e)
            {
                _log.Error($"Failed to set Teach mode: {request.On.Value}", e);
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateUndefinedExecutionError(e.Message));
            }

            return Task.FromResult(new TeachMode_Responses());
        }

        public override Task<Get_TeachMode_Responses> Get_TeachMode(Get_TeachMode_Parameters request, ServerCallContext context)
        {
            var response = new Get_TeachMode_Responses();
            try
            {
                response.TeachMode = new Sila2.Org.Silastandard.Boolean {Value = _robotController.TeachMode};
            }
            catch (Exception e)
            {
                _log.Error(e);
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateUndefinedExecutionError(e.Message));
            }

            return Task.FromResult(response);
        }

        public override Task<Get_TaughtPositions_Responses> Get_TaughtPositions(Get_TaughtPositions_Parameters request, ServerCallContext context)
        {
            var response = new Get_TaughtPositions_Responses();
            try
            {
                foreach (var position in _robotManager.TaughtPositions)
                {
                    response.TaughtPositions.Add(new Sila2.Org.Silastandard.String {Value = position});
                }
            }
            catch (Exception e)
            {
                _log.Error(e);
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateUndefinedExecutionError(e.Message));
            }

            return Task.FromResult(new Get_TaughtPositions_Responses(response));
        }

        public override Task<TeachPosition_Responses> TeachPosition(TeachPosition_Parameters request,
            ServerCallContext context)
        {
            Execute(() => { _robotManager.TeachPosition(request.Position.Value); });
            return Task.FromResult(new TeachPosition_Responses());
        }

        public override Task<TeachDevice_Responses> TeachDevice(TeachDevice_Parameters request, ServerCallContext context)
        {
            Execute(() => { _robotManager.TeachDevice(request.Device.Value); });
            return Task.FromResult(new TeachDevice_Responses());
        }

        public override Task<GetCalibrationPlate_Responses> GetCalibrationPlate(GetCalibrationPlate_Parameters request, ServerCallContext context)
        {
            var orientation = Orientation.Landscape;
            if (string.Compare(request.Orientation.Value, "Landscape") == 0)
            {
                orientation = Orientation.Portrait;
            }
            
            Execute(() => { _robotController.GripCalibrationPlate(orientation); });

            return Task.FromResult(new GetCalibrationPlate_Responses());
        }
        
        private Task Execute(Action action)
        {
            try
            {
                SynchronizationController.Execute(action.Invoke);
            }
            catch (Exception e)
            {
                _log.Warn(e);
                //TODO: Narrow down and catch the correct exception
                var error = ErrorHandling.CreateDefinedExecutionError("UnhealthyState", e.Message);
                ErrorHandling.RaiseSiLAError(error);
            }
            return Task.CompletedTask;
        }
        
    }
}