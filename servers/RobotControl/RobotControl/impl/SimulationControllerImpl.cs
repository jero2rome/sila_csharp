using System;
using System.Threading.Tasks;
using Common.Logging;
using Grpc.Core;
using Sila2.Utils;

namespace RobotControl.Impl
{
    using Sila2.Org.Silastandard.Core.Simulationcontroller.V1;
    using SiLAFramework = Sila2.Org.Silastandard;

    [Serializable]
    public class ModeCouldNotStartException : Exception
    {
        public readonly SiLAFramework.SiLAError SiLAError = new SiLAFramework.SiLAError
            {DefinedExecutionError = new SiLAFramework.DefinedExecutionError { ErrorIdentifier= "ModeCouldNotStart"}};

        public ModeCouldNotStartException(string message) : base(message)
        {
            SiLAError.DefinedExecutionError.Message = message;
        }

        public ModeCouldNotStartException(string message, string action) : base(message)
        {
            SiLAError.DefinedExecutionError.Message = message;
        }
    }

    public class ModeChangedEventArgs : EventArgs
    {
        public bool SimulationMode { set; get; }
    }

    /// <summary>
    /// Feature responsible for changing instrument from Real to Simulation mode
    /// </summary>
    public sealed class SimulationControllerImpl : SimulationController.SimulationControllerBase
    {
        private ILog _log = LogManager.GetLogger<SimulationControllerImpl>();
        public event EventHandler<ModeChangedEventArgs> ModeChanged;
        private SiLAFramework.Boolean _simulationMode = new SiLAFramework.Boolean { Value = false};

        public override Task<StartSimulationMode_Responses> StartSimulationMode(StartSimulationMode_Parameters request,
            ServerCallContext context)
        {
            SetSimulationMode(true);
            return Task.FromResult(new StartSimulationMode_Responses());
        }

        public override Task<StartRealMode_Responses> StartRealMode(StartRealMode_Parameters request,
            ServerCallContext context)
        {
            SetSimulationMode(false);
            return Task.FromResult(new StartRealMode_Responses());
        }

        public override Task<Get_SimulationMode_Responses> Get_SimulationMode(Get_SimulationMode_Parameters request,
            ServerCallContext context)
        {
            _log.Debug($"Simulation Mode: {_simulationMode}");
            return Task.FromResult(new Get_SimulationMode_Responses
                {SimulationMode = this._simulationMode});
        }

        private void SetSimulationMode(bool simulation)
        {
            try
            {
                if (simulation == _simulationMode.Value)
                {
                    var mode = _simulationMode.Value ? "Simulation" : "Real";
                    throw new ModeCouldNotStartException($"Instrument is already in {mode} mode");
                }

                var modeChangedArgs = new ModeChangedEventArgs {SimulationMode = simulation};
                OnModeChanged(modeChangedArgs);
                // TODO: does this just get called or block
                _simulationMode.Value = simulation;
            }
            catch (ModeCouldNotStartException e)
            {
                _log.Error(e);
                ErrorHandling.RaiseSiLAError(e.SiLAError);
            }
            catch (Exception e)
            {
                _log.Error(e);
                throw;
            }
        }

        private void OnModeChanged(ModeChangedEventArgs e)
        {
            EventHandler<ModeChangedEventArgs> handler = ModeChanged;
            handler?.Invoke(this, e);
        }
    }
}