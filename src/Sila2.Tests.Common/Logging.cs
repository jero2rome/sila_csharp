namespace Sila2.Tests.Common
{
    using System;
    using NLog.Targets;
    using Xunit.Abstractions;
    public static class Logging
    {
        /// <summary>
        /// Forward logs to unit test output
        ///
        /// TODO: Add better logging formatting by loading from config or other
        /// </summary>
        /// <param name="output">The output facade</param>
        public static void SetupLogForwarding(ITestOutputHelper output)
        {
            MethodCallTarget target = new MethodCallTarget("SiLATester", (logEvent, parms) =>
            {
                output.WriteLine(logEvent.FormattedMessage);
                Console.WriteLine(logEvent.FormattedMessage);
            });
            NLog.Config.SimpleConfigurator.ConfigureForTargetLogging(target, NLog.LogLevel.Trace);
        }
    }
}