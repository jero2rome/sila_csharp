﻿using System;
using System.IO;
using Grpc.Core;
using Grpc.Core.Logging;
using Sila2.Utils;

namespace Sila2.Tests.Common
{
    public static class GrpcConfigurator
    {
        
        /// <summary>
        /// setting the certificate authority to the ca.crt path
        /// </summary>
        public static SslCredentials CreateTestSslCredentials()
        {
            GrpcEnvironment.SetLogger(new ConsoleLogger());
            var caPath = Path.Combine(FileUtils.GetAssemblyPath(), "encryption", "ca.crt");
            return Encryption.CreateSslCredentials(caPath);
        }

        /// <summary>
        /// Configures the gRPC environment variables, by changing the console verbosity
        /// </summary>
        public static void EnableDebugTrace()
        {
            Environment.SetEnvironmentVariable("GRPC_VERBOSITY", "DEBUG");
            Environment.SetEnvironmentVariable("GRPC_TRACE", "connectivity_state");
        }

        /// <summary>
        /// Creates server credentials from test cert and key
        /// </summary>
        /// <returns></returns>
        public static ServerCredentials CreateTestServerCredentials()
        {
            string serverCertPath = Path.Combine(FileUtils.GetAssemblyPath(), "encryption", "server.crt");
            string serverKeyPath = Path.Combine(FileUtils.GetAssemblyPath(), "encryption", "server.key");
            return Encryption.CreateServerCredentials(serverCertPath, serverKeyPath);
        }
    }
}