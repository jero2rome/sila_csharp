﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Xsl;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;

namespace Sila2.Tools
{
    public class CodeGenTask : Task
    {
        [Required] public string FeatureFile { get; set; }
        [Required] public string OutputDir { get; set; }
        [Output] public TaskItem ProtoFile { get; set; }

        public override bool Execute()
        {
            try
            {
                GenerateProto(FeatureFile);
                return true;
            }
            catch (Exception e)
            {
                Log.LogErrorFromException(e, true, true, null);
                return false;
            }
        }

        private void GenerateProto(string featureFile)
        {
            var featureAbsPath = Path.Combine(Directory.GetCurrentDirectory(), featureFile);
            var protoFilename = Path.GetFileNameWithoutExtension(featureAbsPath).Replace("sila", "proto");
            var protoAbsPath = Path.Combine(OutputDir, protoFilename);
            ProtoFile = new TaskItem(protoFilename);

            ValidateSchema(featureAbsPath);
            ApplyXslTransformation("fdl2proto.xsl", featureAbsPath, protoAbsPath);
        }

        private static void ApplyXslTransformation(string xslFilename, string inputFilename, string outputFilename)
        {
            var xslt = new XslCompiledTransform();
            var xslReader = XmlReader.Create(GetResourceStream(xslFilename));
            xslt.Load(xslReader, new XsltSettings(), new XmlResourceResolver());
            xslt.Transform(inputFilename, outputFilename);
        }

        private static void ValidateSchema(string featureFilename)
        {
            var settings = new XmlReaderSettings {ValidationType = ValidationType.Schema};
            foreach (var resName in Assembly.GetExecutingAssembly().GetManifestResourceNames()
                .Where(resName => resName.EndsWith(".xsd")))
            {
                var resourceStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resName);
                settings.Schemas.Add(null, XmlReader.Create(resourceStream));
            }

            new XmlDocument().Load(XmlReader.Create(featureFilename, settings));
        }

        private static Stream GetResourceStream(string filename)
        {
            var assembly = Assembly.GetExecutingAssembly();
            return assembly.GetManifestResourceStream(assembly.GetManifestResourceNames()
                .Single(res => res.EndsWith($".{filename}")));
        }

        private class XmlResourceResolver : XmlResolver
        {
            public override object GetEntity(Uri absoluteUri, string role, Type ofObjectToReturn)
            {
                return GetResourceStream(absoluteUri.Segments[absoluteUri.Segments.Length - 1]);
            }
        }
    }
}