namespace Sila2.Utils
{
    using System;
    using System.IO;

    /// <summary>
    /// Utilities for file handling
    /// </summary>
    public class FileUtils
    {
        private static string UnixHomeVar = "HOME";
        private static string WinHomeVar = "USERPROFILE";
        private static string ReferenceHomeVar = "${HOME}";

        /// <summary>
        /// Deletes a all files/directories of a given directory path
        /// </summary>
        /// <param name="path">Path whose content is to be deleted</param>
        public static void Empty(string path)
        {
            var di = new DirectoryInfo(path);
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }
            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                dir.Delete(true);
            }
        }

        /// <summary>
        /// Delete a file if it exists
        /// </summary>
        /// <param name="path">Path to file</param>
        public static void DeleteIfExists(string path)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }
        }

        /// <summary>
        /// Resolve tha HOME directory andpath separators based on OS
        /// </summary>
        /// <param name="path"></param>
        public static string ResolvePath(string path)
        {
            if (Environment.OSVersion.Platform == PlatformID.Unix ||
                Environment.OSVersion.Platform == PlatformID.MacOSX)
            {
                path = path.Replace('\\', '/');
            }
            else
            {
                path = path.Replace('/', '\\');
            }
            path = path.Replace(ReferenceHomeVar, GetHomePath());
            return path;
        }

        public static string GetHomePath()
        {
            if (Environment.OSVersion.Platform == PlatformID.Unix ||
                Environment.OSVersion.Platform == PlatformID.MacOSX)
            {
                return Environment.GetEnvironmentVariable(UnixHomeVar);
            }
            else
            {
                return Environment.GetEnvironmentVariable(WinHomeVar);
            }
        }

        public static string GetAssemblyPath()
        {
            return Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        }
    }
}