namespace Sila2.Utils
{
    using Microsoft.Extensions.Configuration;
    using Common.Logging;
    using Common.Logging.Configuration;

    public static class Logging
    {

        public static IConfiguration LoadConfigXml(string fileName)
        {
            return new ConfigurationBuilder()
                .SetBasePath(FileUtils.GetAssemblyPath())
                .AddXmlFile(fileName, optional: true, reloadOnChange: false)
                .Build();
        }

        /// <summary>
        /// Configures the applications logging, to consume Sila2.Tests.Common.Logging abstractins
        /// as specified by the config file
        /// </summary>
        public static void SetupCommonLogging(string configFile = "common_logging.config")
        {
            var config = LoadConfigXml(configFile);
            var logConfiguration = new LogConfiguration();
            config.GetSection("common:logging").Bind(logConfiguration);
            LogManager.Configure(logConfiguration);
        }
    }
}