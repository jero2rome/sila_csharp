﻿namespace Sila2.Discovery
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.NetworkInformation;
    using System.Threading.Tasks;
    using Common.Logging;
    using Grpc.Core;
    using Makaretu.Dns;
    using Utils;
    using Org.Silastandard.Core.Silaservice.V1;

    /// <summary>
    /// Class to query for sila services via mDNS queries.
    /// </summary>
    public static class ServiceFinder
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(ServiceFinder).FullName);

        /// <summary>
        /// Queries for a service of the given type thats name and host name contain the given patterns and returns the all matching services unless the given time exceeds.
        /// </summary>
        /// <param name="timeout">Max time (in milli-seconds) to discover </param>
        /// <param name="credentials">Credentials for connecting to an encrypted gRPC channel</param>
        /// <param name="channelOptions">gRPC channel configurations, such as CA cert or overriding target names for testing</param>
        /// <returns>The list of services matching the given conditions or and empty list if nothing has been discovered within the given time.</returns>
        public static List<Channel> QueryAllServices(int timeout = 10000, SslCredentials credentials = null,
            IEnumerable<ChannelOption> channelOptions = null)
        {
            return QueryAllServicesAsync(timeout, credentials, channelOptions).Result;
        }

        /// <summary>
        /// Asynchronously queries for a service of the given type thats name and host name contain the given patterns and returns the all matching services unless the given time exceeds.
        /// </summary>
        /// <returns>The list of services matching the given conditions or and empty list if nothing has been discovered within the given time.</returns>
        public static async Task<List<Channel>> QueryAllServicesAsync(int time = 10000,
            SslCredentials credentials = null, IEnumerable<ChannelOption> channelOptions = null,
            params NetworkInterface[] interfaceFilter)
        {
            // list of services found during the browsing time
            var services = new List<Channel>();

            using (var mdns = new MulticastService(Networking.CreateFilterFunc(interfaceFilter)))
            {
                // attach the event handler for processing responses.
                mdns.AnswerReceived += (s, e) =>
                {
                    // The first Answer will be a PTRRecord
                    var pointers = e.Message.Answers.OfType<PTRRecord>().Where(p => p.Name.Contains(DiscoveryConstants.ServiceName));
                    foreach (var pointer in pointers)
                    {
                        // Ask for the service instance details
                        mdns.SendQuery(pointer.DomainName, type: DnsType.SRV);
                    }

                    // The second Answer will be a SRVRecord (also contain A and AAAA records)
                    var servers = e.Message.Answers.OfType<SRVRecord>().Where(p => p.Name.Contains(DiscoveryConstants.ServiceName));
                    foreach (var server in servers)
                    {
                        var A_record = e.Message.AdditionalRecords.OfType<ARecord>().FirstOrDefault();
                        if (A_record != null)
                        {
                            _log.Trace($"Discovered SiLA Service at {A_record.Address}:{server.Port}");
                            services.Add(BuildNewChannel(A_record.Address, server.Port, credentials, channelOptions));
                        }
                    }
                };

                mdns.Start();
                // Ask for PTRRecords (the devices will only respond to SRV that are directed exactly to them)
                mdns.SendQuery(string.Join(".", DiscoveryConstants.ServiceName, DiscoveryConstants.ServiceDomainName), type: DnsType.PTR);
                await Task.Delay(time);
                mdns.Stop();
            }

            return services;
        }

        /// <summary>
        /// Utility function to create gRPC channel from IP and port from ServerDiscoveryInfo
        /// </summary>
        /// <param name="host"></param>
        /// <param name="port"></param>
        /// <param name="credentials">Credentials for connecting to an encrypted gRPC channel</param>
        /// <param name="channelOptions">gRPC channel configurations, such as CA cert or overriding target names for testing</param>
        /// <param name="fallback">Whether or not the channel should fallback from a secure to an insecure connection.</param>
        /// <returns></returns>
        public static Channel BuildNewChannel(IPAddress host, int port, SslCredentials credentials = null, IEnumerable<ChannelOption> channelOptions = null, bool fallback = true)
        {
            var target = $"{host}:{port}";

            if (credentials == null)
            {
                return new Channel(target, ChannelCredentials.Insecure, channelOptions);
            }

            Channel channel = new Channel(target, credentials, channelOptions);
            // having the fallback by default may lead to downgrade attacks, the api user should at least have the option to disable it
            if (!fallback || CheckChannelConnection(channel))
            {
                return channel;
            }

            _log.Warn("Creating a secure channel failed, falling back to insecure.");
            return new Channel(target, ChannelCredentials.Insecure);
        }

        private static bool CheckChannelConnection(Channel channel)
        {
            try
            {
                var serviceClient = new SiLAService.SiLAServiceClient(channel);
                serviceClient.Get_ServerUUID(new Get_ServerUUID_Parameters());
                return true;
            }
            catch (RpcException)
            {
                return false;
            }
        }
    }
}