using System;
using System.Collections.Generic;
using System.Linq;
using Sila2.Org.Silastandard.Core.Silaservice.V1;
using SiLAFramework = Sila2.Org.Silastandard;

namespace Sila2.Discovery
{
    using Grpc.Core;
    using Common.Logging;
    using Utils;

    /// <summary>
    /// Exception to be used if Server is not found during discovery
    /// </summary>
    public class ServerNotFoundException : Exception
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message">reason why server was not found</param>
        /// <returns></returns>
        public ServerNotFoundException(string message) : base(message)
        {
        }
    }

    /// <summary>
    /// Utility class to be used to discover and find SiLA Servers using the 
    /// ServiceFinder class.
    /// </summary>
    public static class SiLADiscovery
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(SiLADiscovery).FullName);

        /// <summary>
        /// Query a list of SiLA2 servers with valid SiLAService
        /// </summary>
        /// <param name="timeout">duration of Query period</param>
        /// <param name="credentials">Ssl credentials created</param>
        /// <param name="channelOptions">gRPC channel configurations, such as CA cert or overriding target names for testing</param>
        /// <returns>"List of valid SiLA2 servers</returns>
        public static List<ServerData> GetServers(int timeout = 10000, SslCredentials credentials = null, IEnumerable<ChannelOption> channelOptions = null)
        {
            return ServiceFinder.QueryAllServices(timeout, credentials, channelOptions)
                .Where(channel =>
                {
                    try
                    {
                        GetServerData(channel);
                        return true;
                    }
                    catch (Exception)
                    {
                        Log.Warn($"Invalid server found at channel: {channel.Target}");
                        return false;
                    }
                })
                .Select(GetServerData).ToList();
        }
        
        /// <summary>
        /// Load all server properties 
        /// </summary>
        public static ServerData GetServerData(Channel channel)
        {
            var silaService = new SiLAService.SiLAServiceClient(channel);
            var config = GetServerConfig(silaService);
            var info = GetServerInfo(silaService);
            var features = GetImplementedFeatures(silaService);
            var server = new ServerData(channel, config, info, features);
            return server;
        }
        
        private static List<Feature> GetImplementedFeatures(SiLAService.SiLAServiceClient silaService)
        {
            var result = new List<Feature>();
            silaService.Get_ImplementedFeatures(new Get_ImplementedFeatures_Parameters())
                .ImplementedFeatures
                .ToList()
                .ForEach(feature => result.Add(GetFeatureDefinition(silaService, feature.Value)));
            return result;
        }

        private static Feature GetFeatureDefinition(SiLAService.SiLAServiceClient silaService, string featureIdentifier)
        {
            GetFeatureDefinition_Responses response;
            response = silaService.GetFeatureDefinition(new GetFeatureDefinition_Parameters
            {
                FeatureIdentifier = new SiLAFramework.String
                    {
                        Value = featureIdentifier
                    }
            });
            return FeatureGenerator.ReadFeatureFromXml(response.FeatureDefinition.Value);
        }

        private static ServerConfig GetServerConfig(SiLAService.SiLAServiceClient silaService)
        {
            var name = silaService.Get_ServerName(new Get_ServerName_Parameters()).ServerName.Value;
            var uuid = Guid.Parse(silaService.Get_ServerUUID(new Get_ServerUUID_Parameters()).ServerUUID.Value);
            var config = new ServerConfig(name, uuid);
            return config;
        }

        private static ServerInformation GetServerInfo(SiLAService.SiLAServiceClient silaService)
        {
            var type = silaService.Get_ServerType(new Get_ServerType_Parameters()).ServerType.Value;
            var description = silaService.Get_ServerDescription(new Get_ServerDescription_Parameters()).ServerDescription.Value;
            var vendorUri = silaService.Get_ServerVendorURL(new Get_ServerVendorURL_Parameters()).ServerVendorURL.Value;
            var version = silaService.Get_ServerVersion(new Get_ServerVersion_Parameters()).ServerVersion.Value;
            var info = new ServerInformation(type, description, vendorUri, version);
            return info;
        }
    }
}