﻿namespace Sila2
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// This class extends the Feature class auto-generated from the FeatureDefinition schema.
    /// </summary>
    public partial class Feature
    {
        /// <summary>
        /// Gets the namespace to be used when calling the gRPC service.
        /// </summary>
        public string Namespace => $"sila2.{this.Originator}.{this.Category}.{this.Identifier.ToLower()}.v{Version.Parse(this.FeatureVersion).Major}";

        #region Fully qualified identifiers

        /// <summary>
        /// Gets the fully qualified identifier assembled from several feature properties as specified in the SiLA 2 standard.
        /// </summary>
        public string FullyQualifiedIdentifier => $"{this.Originator}/{this.Category}/{this.Identifier}/v{Version.Parse(this.FeatureVersion).Major}";

        /// <summary>
        /// Returns the fully qualified identifier of the given command.
        /// </summary>
        /// <param name="commandIdentifier">The identifier of the command.</param>
        /// <returns>The fully qualified command identifier.</returns>
        public string GetFullyQualifiedCommandIdentifier(string commandIdentifier)
        {
            var fullyQualifiedIdentifier = $"{this.FullyQualifiedIdentifier}/Command/{commandIdentifier}";
            if (null == GetMatchingElement(fullyQualifiedIdentifier))
            {
                throw new ArgumentException($"'{commandIdentifier}' is not a command of the feature");
            }

            return fullyQualifiedIdentifier;
        }

        /// <summary>
        /// Returns the fully qualified identifier of the given parameter of the given command.
        /// </summary>
        /// <param name="commandIdentifier">The identifier of the command.</param>
        /// <param name="parameterIdentifier">The identifier of the parameter.</param>
        /// <returns>The fully qualified command parameter identifier.</returns>
        public string GetFullyQualifiedCommandParameterIdentifier(string commandIdentifier, string parameterIdentifier)
        {
            var fullyQualifiedIdentifier = $"{this.GetFullyQualifiedCommandIdentifier(commandIdentifier)}/Parameter/{parameterIdentifier}";
            if (null == GetMatchingElement(fullyQualifiedIdentifier))
            {
                throw new ArgumentException($"'{parameterIdentifier}' is not parameter of the feature command '{commandIdentifier}'");
            }

            return fullyQualifiedIdentifier;
        }

        /// <summary>
        /// Returns the fully qualified identifier of the given response of the given command.
        /// </summary>
        /// <param name="commandIdentifier">The identifier of the command.</param>
        /// <param name="responseIdentifier">The identifier of the response.</param>
        /// <returns>The fully qualified command response identifier.</returns>
        public string GetFullyQualifiedCommandResponseIdentifier(string commandIdentifier, string responseIdentifier)
        {
            var fullyQualifiedIdentifier = $"{this.GetFullyQualifiedCommandIdentifier(commandIdentifier)}/Response/{responseIdentifier}";
            if (null == GetMatchingElement(fullyQualifiedIdentifier))
            {
                throw new ArgumentException($"'{responseIdentifier}' is not a response of the feature command '{commandIdentifier}'");
            }

            return fullyQualifiedIdentifier;
        }

        /// <summary>
        /// Returns the fully qualified identifier of the given metadata.
        /// </summary>
        /// <param name="commandIdentifier">The identifier of the command.</param>
        /// <param name="intermediateResponseIdentifier">The identifier of the intermediate response.</param>
        /// <returns>The fully qualified intermediate command response identifier.</returns>
        public string GetFullyQualifiedIntermediateCommandResponseIdentifier(string commandIdentifier, string intermediateResponseIdentifier)
        {
            var fullyQualifiedIdentifier = $"{this.GetFullyQualifiedCommandIdentifier(commandIdentifier)}/IntermediateResponse/{intermediateResponseIdentifier}";
            if (null == GetMatchingElement(fullyQualifiedIdentifier))
            {
                throw new ArgumentException($"'{intermediateResponseIdentifier}' is not an intermediate response of the feature command '{commandIdentifier}'");
            }

            return fullyQualifiedIdentifier;
        }

        /// <summary>
        /// Returns the fully qualified identifier of the given defined execution error.
        /// </summary>
        /// <param name="definedExecutionErrorIdentifier">The identifier of the defined execution error.</param>
        /// <returns>The fully qualified defined execution error identifier.</returns>
        public string GetFullyQualifiedDefinedExecutionErrorIdentifier(string definedExecutionErrorIdentifier)
        {
            var fullyQualifiedIdentifier = $"{this.FullyQualifiedIdentifier}/DefinedExecutionError/{definedExecutionErrorIdentifier}";
            if (null == GetMatchingElement(fullyQualifiedIdentifier))
            {
                throw new ArgumentException($"'{definedExecutionErrorIdentifier}' is not a defined execution error of the feature");
            }

            return fullyQualifiedIdentifier;
        }

        /// <summary>
        /// Returns the fully qualified identifier of the given property.
        /// </summary>
        /// <param name="propertyIdentifier">The identifier of the property.</param>
        /// <returns>The fully qualified property identifier.</returns>
        public string GetFullyQualifiedPropertyIdentifier(string propertyIdentifier)
        {
            var fullyQualifiedIdentifier = $"{this.FullyQualifiedIdentifier}/Property/{propertyIdentifier}";
            if (null == GetMatchingElement(fullyQualifiedIdentifier))
            {
                throw new ArgumentException($"'{propertyIdentifier}' is not a property of the feature");
            }

            return fullyQualifiedIdentifier;
        }

        /// <summary>
        /// Returns the fully qualified identifier of the given data type.
        /// </summary>
        /// <param name="dataTypeIdentifier">The identifier of the data type.</param>
        /// <returns>The fully qualified data type identifier.</returns>
        public string GetFullyQualifiedDataTypeIdentifier(string dataTypeIdentifier)
        {
            var fullyQualifiedIdentifier = $"{this.FullyQualifiedIdentifier}/DataType/{dataTypeIdentifier}";
            if (null == GetMatchingElement(fullyQualifiedIdentifier))
            {
                throw new ArgumentException($"'{dataTypeIdentifier}' is not a data type of the feature");
            }

            return fullyQualifiedIdentifier;
        }

        /// <summary>
        /// Returns the fully qualified identifier of the given metadata.
        /// </summary>
        /// <param name="metadataIdentifier">The identifier of the metadata.</param>
        /// <returns>The fully qualified metadata identifier.</returns>
        public string GetFullyQualifiedMetadataIdentifier(string metadataIdentifier)
        {
            var fullyQualifiedIdentifier = $"{this.FullyQualifiedIdentifier}/Metadata/{metadataIdentifier}";
            if (null == GetMatchingElement(fullyQualifiedIdentifier))
            {
                throw new ArgumentException($"'{metadataIdentifier}' is not a metadata of the feature");
            }

            return fullyQualifiedIdentifier;
        }

        #endregion

        #region Lists of elements of a certain type

        /// <summary>
        /// Returns all DefinedExecutionErrors that are defined in the Feature definition.
        /// </summary>
        /// <returns>A list of all DefinedExecutionErrors.</returns>
        public List<FeatureDefinedExecutionError> GetDefinedExecutionErrors()
        {
            var result = new List<FeatureDefinedExecutionError>();

            foreach (var item in Items)
            {
                if (item is FeatureDefinedExecutionError error)
                {
                    result.Add(error);
                }
            }

            return result;
        }

        /// <summary>
        /// Returns all commands that are defined in the Feature definition.
        /// </summary>
        /// <returns>A list of all commands.</returns>
        public List<FeatureCommand> GetDefinedCommands()
        {
            var result = new List<FeatureCommand>();

            foreach (var item in this.Items)
            {
                if (item is FeatureCommand command)
                {
                    result.Add(command);
                }
            }

            return result;
        }

        /// <summary>
        /// Returns all properties that are defined in the Feature definition.
        /// </summary>
        /// <returns>A list of all properties.</returns>
        public List<FeatureProperty> GetDefinedProperties()
        {
            var result = new List<FeatureProperty>();

            foreach (var item in this.Items)
            {
                if (item is FeatureProperty property)
                {
                    result.Add(property);
                }
            }

            return result;
        }

        /// <summary>
        /// Returns all data types that are defined in the Feature definition.
        /// </summary>
        /// <returns>A list of all data types.</returns>
        public List<SiLAElement> GetDefinedDataTypes()
        {
            var result = new List<SiLAElement>();

            foreach (var item in this.Items)
            {
                if (!(item is FeatureDefinedExecutionError) &&
                    !(item is FeatureCommand) &&
                    !(item is FeatureProperty) &&
                    !(item is FeatureMetadata) &&
                    item is SiLAElement dataType)
                {
                    result.Add(dataType);
                }
            }

            return result;
        }

        /// <summary>
        /// Returns all metadata that is defined in the Feature definition.
        /// </summary>
        /// <returns>A list of all metadata.</returns>
        public List<FeatureMetadata> GetDefinedMetadata()
        {
            var result = new List<FeatureMetadata>();

            foreach (var item in this.Items)
            {
                if (item is FeatureMetadata metadata)
                {
                    result.Add(metadata);
                }
            }

            return result;
        }

        #endregion

        /// <summary>
        /// Returns the feature element specified by the given fully qualified identifier.
        /// </summary>
        /// <returns>The specified feature element or null if not existing.</returns>
        public object GetMatchingElement(string fullyQualifiedIdentifier)
        {
            if (!fullyQualifiedIdentifier.StartsWith(this.FullyQualifiedIdentifier))
            {
                // feature identifier does not match
                return null;
            }

            var identifier = fullyQualifiedIdentifier.Substring(this.FullyQualifiedIdentifier.Length).Trim();

            if (string.IsNullOrEmpty(identifier))
            {
                // given identifier is only a feature identifier
                return null;
            }

            const string commandToken = "/Command/";
            if (identifier.StartsWith(commandToken))
            {
                identifier = identifier.Substring(commandToken.Length);

                // check if command exists
                var cmdList = this.GetDefinedCommands().Where(c => identifier.StartsWith(c.Identifier)).ToList();
                if (cmdList.Count == 0)
                {
                    // no command with specified identifier found in the specified feature
                    return null;
                }

                var cmd = cmdList[0];

                // if identifier ends with the command identifier, return FeatureCommand object
                if (cmd.Identifier == identifier)
                {
                    return cmd;
                }

                identifier = identifier.Substring(cmd.Identifier.Length);
                const string parameterToken = "/Parameter/";
                if (identifier.StartsWith(parameterToken))
                {
                    identifier = identifier.Substring(parameterToken.Length);

                    // check if parameter exists
                    var param = cmd.Parameter.FirstOrDefault(p => p.Identifier == identifier);
                    if (param == null)
                    {
                        // no parameter with specified identifier found in the specified command
                        return null;
                    }

                    return param;
                }

                const string responseToken = "/Response/";
                if (identifier.StartsWith(responseToken))
                {
                    identifier = identifier.Substring(responseToken.Length);

                    // check if response exists
                    var response = cmd.Response.FirstOrDefault(r => r.Identifier == identifier);
                    if (response == null)
                    {
                        // no response with specified identifier found in the specified command
                        return null;
                    }

                    return response;
                }

                const string intermediateResponseToken = "/IntermediateResponse/";
                if (identifier.StartsWith(intermediateResponseToken))
                {
                    identifier = identifier.Substring(intermediateResponseToken.Length);

                    // check if intermediate response exists
                    var intermediateResponse = cmd.IntermediateResponse.FirstOrDefault(r => r.Identifier == identifier);
                    if (intermediateResponse == null)
                    {
                        // no intermediate response with specified identifier found in the specified command
                        return null;
                    }

                    return intermediateResponse;
                }

                // no valid element found within command
                return null;
            }

            const string propertyToken = "/Property/";
            if (identifier.StartsWith(propertyToken))
            {
                identifier = identifier.Substring(propertyToken.Length);

                // check if property exists and identifier ends with the property identifier
                return this.GetDefinedProperties().FirstOrDefault(p => p.Identifier == identifier);
            }

            const string definedExecutionErrorToken = "/DefinedExecutionError/";
            if (identifier.StartsWith(definedExecutionErrorToken))
            {
                identifier = identifier.Substring(definedExecutionErrorToken.Length);

                // check if defined execution error exists and identifier ends with the defined execution error identifier
                return this.GetDefinedExecutionErrors().FirstOrDefault(e => e.Identifier == identifier);
            }

            const string dataTypeToken = "/DataType/";
            if (identifier.StartsWith(dataTypeToken))
            {
                identifier = identifier.Substring(dataTypeToken.Length);

                // check if data type exists and identifier ends with the data type identifier
                return this.GetDefinedDataTypes().FirstOrDefault(t => t.Identifier == identifier);
            }

            const string metadataToken = "/Metadata/";
            if (identifier.StartsWith(metadataToken))
            {
                identifier = identifier.Substring(metadataToken.Length);

                // check if metadata exists and identifier ends with the metadata identifier
                return this.GetDefinedMetadata().FirstOrDefault(m => m.Identifier == identifier);
            }

            // not a feature element identifier
            return null;
        }

        #region Overrides of Object

        /// <summary>
        /// Lists the Featrue Definition content in a readable multiline manner.
        /// </summary>
        /// <returns>The feature definition multiline text string.</returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            foreach (var property in this.GetType().GetProperties())
            {
                switch (property.Name)
                {
                    case "Items":
                        break;

                    default:
                        sb.Append(property.Name);

                        sb.Append(": ");
                        if (property.GetIndexParameters().Length > 0)
                        {
                            sb.Append("Indexed Property cannot be used");
                        }
                        else
                        {
                            sb.Append(property.GetValue(this, null));
                        }

                        sb.Append(Environment.NewLine);
                        break;
                }
            }

            // items
            sb.Append(Environment.NewLine);
            foreach (var item in this.Items)
            {
                if (item is FeatureCommand command)
                {
                    sb.AppendFormat("<Command> \"{0}\": {1}\n", command.Identifier, command.Description);
                    if (command.Parameter != null)
                    {
                        sb.AppendLine("   Parameters:");
                        foreach (var parameter in command.Parameter)
                        {
                            sb.AppendFormat("   - \"{0}\" [{1}]: {2}\n", parameter.Identifier, GetDataTypeString(parameter.DataType), parameter.Description);
                        }
                    }

                    if (command.Response != null)
                    {
                        sb.AppendLine("   Responses:");
                        foreach (var response in command.Response)
                        {
                            sb.AppendFormat("   - \"{0}\" [{1}]: {2}\n", response.Identifier, GetDataTypeString(response.DataType), response.Description);
                        }
                    }

                    if (command.IntermediateResponse != null)
                    {
                        sb.AppendLine("   Intermediate Responses:");
                        foreach (var intermediateResponse in command.IntermediateResponse)
                        {
                            sb.AppendFormat("   - \"{0}\" [{1}]: {2}\n", intermediateResponse.Identifier, GetDataTypeString(intermediateResponse.DataType), intermediateResponse.Description);
                        }
                    }

                    if (command.DefinedExecutionErrors != null)
                    {
                        sb.AppendLine("   Defined Execution Errors:");
                        foreach (var error in command.DefinedExecutionErrors)
                        {
                            sb.AppendFormat("   - \"{0}\"\n", error);
                        }
                    }

                }
                else if (item is FeatureDefinedExecutionError error)
                {
                    sb.AppendFormat("<DefinedExecutionError> \"{0}\": {1}\n", error.Identifier, error.Description);
                }
                else if (item is FeatureProperty property)
                {
                    sb.AppendFormat("<Property> \"{0}\" {1} [{2}]: {3}\n", property.Identifier, property.Observable, GetDataTypeString(property.DataType), property.Description);
                }
                else if (item is FeatureMetadata metaData)
                {
                    sb.AppendFormat("<MetaData> \"{0}\" [{1}]: {2}\n", metaData.Identifier, GetDataTypeString(metaData.DataType), metaData.Description);
                }
                else if (item is SiLAElement dataType)
                {
                    sb.AppendFormat("<DataTypeDefinition> \"{0}\" [{1}]: {2}\n", dataType.Identifier, GetDataTypeString(dataType.DataType), dataType.Description);
                }
            }

            return sb.ToString();
        }

        #endregion

        /// <summary>
        /// Returns a readable string describing the structure of the given SiLA data type.
        /// </summary>
        /// <param name="type">The SiLA data type</param>
        /// <returns>The data type string</returns>
        private static string GetDataTypeString(DataTypeType type)
        {
            if (type.Item is BasicType)
            {
                // basic type
                return type.Item.ToString();
            }

            if (type.Item is ConstrainedType constrainedType)
            {
                // constrained type
                return $"constrained {GetDataTypeString(constrainedType.DataType)}";
            }

            if (type.Item is StructureType structureType)
            {
                // structure type
                return $"structured {structureType.Element}";
            }

            if (type.Item is ListType listType)
            {
                // list type
                return $"list of {GetDataTypeString(listType.DataType)}";
            }

            // custom type
            return type.Item.ToString();
        }
    }
}