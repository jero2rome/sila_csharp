﻿namespace Sila2.Server
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Threading.Tasks;
    using System.Net;
    using System.Net.Sockets;
    using Common.Logging;
    using Grpc.Core;
    using SiLAService = Org.Silastandard.Core.Silaservice.V1.SiLAService;
    using System.Net.NetworkInformation;
    using Discovery;
    using Utils;

    /// <summary>
    /// Base class for all SiLA 2 Servers.
    /// </summary>
    public class SiLA2Server
    {
        private static readonly ILog _logger = LogManager.GetLogger<SiLA2Server>();

        #region Members

        /// <summary>The gRPC server.</summary>
        public Server GrpcServer { get; }

        /// <summary>The list of implemented features.</summary>
        private readonly List<Feature> _implementedFeatures;

        /// <summary>Discovery listener</summary>
        private readonly ServiceAnnouncer _announcer;
        #endregion

        #region Utils

        /// <summary>
        /// Check if the specified TCP port is available
        /// If the port is already in use, an SocketException is thrown
        /// </summary>
        private static void PortAvailableOrThrow(int port)
        {
            TcpListener tcpListener = new TcpListener(IPAddress.Parse("0.0.0.0"), port);
            tcpListener.Start();
            tcpListener.Stop();
        }

        private static IConfigWrapper<ServerConfig> CreateConfigWrapper(string serverType, string configFilePath)
        {
            var defaultConfig = new ServerConfig(serverType, Guid.NewGuid());
            if (configFilePath == null)
            {
                return new NonPersistentConfigWrapper<ServerConfig>(defaultConfig);
            }
            else
            {
                return new PersistentConfigWrapper<ServerConfig>(configFilePath, defaultConfig);
            }
        }

        #endregion

        #region Constructors and destructors
        
        /// <summary>
        /// Creates a new instance of the SiLA2Server class, adds the SiLAService Feature implementation, creates a secure gRPC Server and registers the server for Server discovery.
        /// </summary>
        /// <param name="serverInformation">Structure containing server information.</param>
        /// <param name="portNumber">The port number the server is listening at.</param>
        /// <param name="networkInterface">Network interface to serve on, if none is specified Discovery will be disabled on the server.</param>
        /// <param name="serverCredentials">gRPC server credentials to encrypt the communication channel</param>
        /// <param name="configFile">Configuration file where server config is to be located.</param>
        public SiLA2Server(ServerInformation serverInformation, int portNumber, NetworkInterface networkInterface, ServerCredentials serverCredentials, string configFile = null)
        {
            // C# gRPC does not check if the port is already in use, so we check it manually
            PortAvailableOrThrow(portNumber);

            IConfigWrapper<ServerConfig> configWrapper = CreateConfigWrapper(serverInformation.Type, configFile);
            ServerConfig config = configWrapper.GetConfig();


            _implementedFeatures = new List<Feature>();
            GrpcServer = new Server();

            // add mandatory SiLAService feature
            ////"https://gitlab.com/SiLA2/sila_base/raw/master/feature_definitions/org/sila-standard/core/SiLAService.sila.xml"
            Feature feature = ReadFeature("SiLAService.sila.xml", typeof(SiLAServiceImpl));
            // add service to gRPC server
            GrpcServer.Services.Add(SiLAService.BindService(new SiLAServiceImpl { SiLAServer = this, SiLAFeature = feature, ConfigWrapper = configWrapper, ServerInfo = serverInformation }));

            // create gRPC server such that it uses only the specified network interface
            if (networkInterface != null)
            {
                // configure gRPC server to listen on these addresses
                foreach (var v4addr in Networking.ListInterNetworkAddresses(networkInterface))
                {
                    GrpcServer.Ports.Add(new ServerPort(v4addr.ToString(), portNumber, serverCredentials));
                }

                // register for Server Discovery
                _announcer = new ServiceAnnouncer($"{config.Uuid}", (ushort)portNumber, networkInterface);

                // logger output
                _logger.Info("Running on the following endpoints:");
                foreach ((IPAddress address, int port) in _announcer.Endpoints)
                {
                    _logger.Info($"{address}:{port}");
                }
            }
            else
            {
                _logger.Warn("Server is running without Discovery!");
                // if no interface was specified, the SiLA server will (try to) run on all of them
                GrpcServer.Ports.Add(new ServerPort("0.0.0.0", portNumber, serverCredentials));
            }
        }

        /// <summary>
        /// Creates a new instance of the SiLA2Server class, adds the SiLAService Feature implementation, creates a gRPC Server and registers the server for Server discovery.
        /// </summary>
        /// <param name="serverInformation">Structure containing server information.</param>
        /// <param name="portNumber">The port number the server is listening at.</param>
        /// <param name="configFile">Configuration file where server config is to be located.</param>
        /// <param name="networkInterface">Network interface to serve on, if none is specified Discovery will be disabled on the server.</param>
        public SiLA2Server(ServerInformation serverInformation, int portNumber, NetworkInterface networkInterface, string configFile = null) 
            : this(serverInformation, portNumber, networkInterface, ServerCredentials.Insecure, configFile)
        {
        }

        /// <summary>
        /// Creates a new instance of the SiLA2Server class, adds the SiLAService Feature implementation, creates a secure gRPC Server and registers the server for Server discovery.
        /// </summary>
        /// <param name="serverInformation">Structure containing server information.</param>
        /// <param name="portNumber">The port number the server is listening at.</param>
        /// <param name="networkInterface">Network interface to serve on, if none is specified Discovery will be disabled on the server.</param>
        /// <param name="certificateFilePath">The file path of the SSL certificate/certificate chain to be used in securing the connection.</param>
        /// <param name="keyFilePath">The file path of the private key corresponding to the certificate/certificate chain at <paramref name="certificateFilePath"/></param>
        /// <param name="configFile">Configuration file where server config is to be located.</param>
        public SiLA2Server(ServerInformation serverInformation, int portNumber, NetworkInterface networkInterface, string certificateFilePath, string keyFilePath, string configFile = null) 
            : this(serverInformation, portNumber, networkInterface, Encryption.CreateServerCredentials(certificateFilePath, keyFilePath), configFile)
        {
        }

        #endregion

        #region gRPC server

        public void StartServer()
        {
            _logger.Info("Starting gRPC server");
            GrpcServer.Start();
            _logger.Info("Starting service announcer");
            if (_announcer != null)
            {
                _announcer.Start();
                _logger.Info($"Announcing as '{_announcer.Profile.FullyQualifiedName}'");
            }
        }

        public async Task ShutdownServer()
        {
            _announcer?.Dispose();
            _logger.Info("Shutting down gRPC server");
            await GrpcServer.ShutdownAsync();
        }

        #endregion

        #region Feature discovery

        /// <summary>
        /// Method for extracting a <see cref="Feature"/> from an EmbeddedResource.
        /// </summary>
        /// <param name="resourceName">The name of the embedded resource.
        ///
        /// The pattern is: '[assembly default namespace].[directory].[filename]'
        /// For example: 'sila2.features.SiLAService.xml'.
        /// </param>
        /// <param name="implementationType">A type from the assembly that contains the EmbeddedResource.</param>
        /// <exception cref="FileNotFoundException">Thrown when the embedded resource stream can't be found.</exception>
        /// <exception cref="ApplicationException">Thrown when reading the feature from the Stream fails.</exception>
        /// <returns>The deserialized feature object.</returns>
        public Feature ReadFeature(string resourceName, Type implementationType)
        {
            var fullyQualifiedResourceName = implementationType.Module.Assembly.GetManifestResourceNames()
                .Single(name => name.EndsWith(resourceName));
            var featureStream = implementationType.Module.Assembly.GetManifestResourceStream(fullyQualifiedResourceName);
            // get the embedded resource from the assembly of the implementation
            if (featureStream == null)
            {
                var resourcesJointList =
                    string.Join(", ", implementationType.Module.Assembly.GetManifestResourceNames());
                throw new FileNotFoundException($"Feature resource '{resourceName}' was not found" +
                                                $" in the list of embedded resources: [{resourcesJointList}]." +
                                                " Did you set its BuildAction to 'EmbeddedResource'?");
            }

            try
            {
                var feature = FeatureGenerator.ReadFeatureFromStream(featureStream);
                _implementedFeatures.Add(feature);
                return feature;
            }
            catch (Exception e)
            {
                _logger.Error("Unable to parse Xml from embedded resource stream.", e);
                throw new ApplicationException("Unable to parse Xml from embedded resource stream.", e);
            }
        }

        /// <summary>
        /// Reads and deserializes the feature definition from the given file.
        /// (it simply does the call 'GrpcServer.Services.Add([grpc_stub_class_name].BindService(new [feature_implementation_class_name(this, [implemented_feature_object])))' )
        /// </summary>
        /// <param name="featureDefinitionFile">The file containing the XML Feature Defintion.</param>
        /// <returns>The deserialized feature object.</returns>
        public Feature ReadFeature(string featureDefinitionFile)
        {
            // deserialize the given feature defintion
            Feature feature;
            if (Uri.IsWellFormedUriString(featureDefinitionFile, UriKind.Absolute))
            {
                feature = FeatureGenerator.ReadFeatureFromOnlineResource(featureDefinitionFile);
            }
            else
            {
                feature = FeatureGenerator.ReadFeatureFromFile(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, featureDefinitionFile));
            }

            _implementedFeatures.Add(feature);

            return feature;
        }

        public void AddFeature(string featureDefinitonFile, ServerServiceDefinition serviceDefinition)
        {
            ReadFeature(featureDefinitonFile);
            GrpcServer.Services.Add(serviceDefinition);
        }

        /// <summary>
        /// Gets the feature object with the given feature identifier.
        /// </summary>
        /// <param name="featureIdentifier">The (full quallified) feature identifier.</param>
        /// <returns>The feature object.</returns>
        public Feature GetFeature(string featureIdentifier)
        {
            return _implementedFeatures.First(f => f.FullyQualifiedIdentifier == featureIdentifier);
        }

        /// <summary>
        /// Gets the feature object that belongs to the given fully qualified element identifier.
        /// Elements can be:
        ///  * command
        ///  * command parameter
        ///  * command response
        ///  * intermediate command response
        ///  * execution error
        ///  * property
        ///  * data type
        ///  * metadata idnetifier
        /// </summary>
        /// <param name="fullyQualifiedIdentifier">The full quallified identifier of the feature element.</param>
        /// <returns>The feature object containing the specified parameter.</returns>
        public Feature GetFeatureOfElement(string fullyQualifiedIdentifier)
        {
            return _implementedFeatures.FirstOrDefault(f => fullyQualifiedIdentifier.StartsWith(f.FullyQualifiedIdentifier));
        }

        /// <summary>
        /// Gets a list of fully quallified identifiers of the implemented features.
        /// </summary>
        public List<string> ImplementedFeatures
        {
            get { return _implementedFeatures.Select(feature => feature.FullyQualifiedIdentifier).ToList(); }
        }

        #endregion

        #region Test state stuff

        /// <summary>
        /// Alernative way to add a feature implmenatation (using reflection).
        /// Reads the feature definition, constrcuts the implementation instance with initial properties, creates the gRPC service and adds it to the gRPC server.
        /// -> it simply does the call 'GrpcServer.Services.Add([grpc_stub_class_name].BindService(new [feature_implementation_class_name]{ SiLAFeature = [implemented_feature_object], SiLAServer = this)))'
        /// </summary>
        /// <param name="feature">The Feature object</param>
        /// <param name="grpcStub">The type of the gRPC stub of the feature</param>
        /// <param name="implementation">The feature implmentation type -> has to implement the IFeatureImplementation interface.</param>
        /// <returns></returns>
        public Feature AddFeatureImplementationByReflection(Feature feature, Type grpcStub, Type implementation)
        {
            // check if IFeatureImplementation interface has been implemented
            if (!implementation.GetInterfaces().Contains(typeof(IFeatureImplementation)))
            {
                throw new Exception("Given feature implementation type does not implement the IFeatureImplementation interface");
            }

            // instantiate the implementation class
            ConstructorInfo ci = implementation.GetConstructor(new Type[] { });
            if (ci == null)
            {
                throw new ApplicationException("Unable to get constructor of the feature implementation " + implementation);
            }

            object impl = ci.Invoke(new object[] { });

            // set implementation members
            implementation.InvokeMember("SiLAFeature", BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetProperty, Type.DefaultBinder, impl, new[] { impl });
            implementation.InvokeMember("SiLAServer", BindingFlags.Instance | BindingFlags.Public | BindingFlags.SetProperty, Type.DefaultBinder, impl, new object[] { this });

            // invoke the BindService method
            ServerServiceDefinition service = (ServerServiceDefinition)grpcStub.InvokeMember("BindService", BindingFlags.InvokeMethod | BindingFlags.Public | BindingFlags.Static, null, null, new[] { impl });

            // add service to gRPC server
            GrpcServer.Services.Add(service);

            _implementedFeatures.Add(feature);

            return feature;
        }

        #endregion
    }
}