﻿namespace Sila2.Server
{
    using System;
    using System.IO;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Serialization;
    using Grpc.Core;
    using SiLAFramework = Org.Silastandard;
    using Org.Silastandard.Core.Silaservice.V1;
    using Common.Logging;
    using Utils;

    sealed class SiLAServiceImpl : SiLAService.SiLAServiceBase
    {
        private static readonly ILog Logger = LogManager.GetLogger<SiLAServiceImpl>();
        public IConfigWrapper<ServerConfig> ConfigWrapper { get; set; }
        public ServerInformation ServerInfo { get; set; }

        public Feature SiLAFeature { get; set; }
        public SiLA2Server SiLAServer { get; set; }

        public override Task<GetFeatureDefinition_Responses> GetFeatureDefinition(GetFeatureDefinition_Parameters request, ServerCallContext context)
        {
            CheckMetadata(context);

            Logger.Info("Request -> ");

            // lookup feature by given qualified identifier
            if (!this.SiLAServer.ImplementedFeatures.Contains(request.FeatureIdentifier.Value))
            {
                // get error identifier and message from defined execution error specified in the feature definition (SiLAService Feature)
                var error = SiLAFeature.GetDefinedExecutionErrors().Find(match => match.Identifier == "UnimplementedFeature");
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateDefinedExecutionError(
                    SiLAFeature.GetFullyQualifiedDefinedExecutionErrorIdentifier(error.Identifier),
                    $"Feature definition '{request.FeatureIdentifier.Value}' is probably missing. Check that the requested feature definition is valid and available"));
            }

            // get feature from server
            Feature feature = this.SiLAServer.GetFeature(request.FeatureIdentifier.Value);

            // serialize Feature definition
            var serializer = new XmlSerializer(typeof(Feature));
            var stringBuilder = new StringBuilder();
            using (var stringWriter = new ExtentedStringWriter(stringBuilder, new UTF8Encoding(false)))
            {
                serializer.Serialize(stringWriter, feature);
            }

            return Task.FromResult(new GetFeatureDefinition_Responses { FeatureDefinition = new SiLAFramework.String { Value = stringBuilder.ToString().Replace(Environment.NewLine, string.Empty) } });
        }

        public override Task<SetServerName_Responses> SetServerName(SetServerName_Parameters request, ServerCallContext context)
        {
            CheckMetadata(context);

            var config = ConfigWrapper.GetConfig();

            Logger.Info($"Request ->  Response = \"{request.ServerName}\"");

            config.Name = request.ServerName.Value;
            ConfigWrapper.SetConfig(config);

            return Task.FromResult(new SetServerName_Responses());
        }

        public override Task<Get_ServerName_Responses> Get_ServerName(Get_ServerName_Parameters request, ServerCallContext context)
        {
            CheckMetadata(context);

            ServerConfig config = null;
            try
            {
                config = ConfigWrapper.GetConfig();
                Logger.Info($"Request ->  Response = \"{(config != null ? config.Name : string.Empty)}\"");
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }
            return Task.FromResult(new Get_ServerName_Responses { ServerName = new SiLAFramework.String { Value = config != null ? config.Name : string.Empty } });
        }

        public override Task<Get_ServerUUID_Responses> Get_ServerUUID(Get_ServerUUID_Parameters request, ServerCallContext context)
        {
            CheckMetadata(context);

            var config = ConfigWrapper.GetConfig();
            Logger.Trace($"Request ->  Response = \"{config.Uuid}\"");

            return Task.FromResult(new Get_ServerUUID_Responses { ServerUUID = new SiLAFramework.String { Value = config.Uuid.ToString() } });
        }

        public override Task<Get_ServerType_Responses> Get_ServerType(Get_ServerType_Parameters request, ServerCallContext context)
        {
            CheckMetadata(context);

            Logger.Info($"Request ->  Response = \"{ServerInfo.Type}\"");

            return Task.FromResult(new Get_ServerType_Responses { ServerType = new SiLAFramework.String { Value = ServerInfo.Type } });
        }

        public override Task<Get_ServerDescription_Responses> Get_ServerDescription(Get_ServerDescription_Parameters request, ServerCallContext context)
        {
            CheckMetadata(context);

            Logger.Info($"Request ->  Response = \"{ServerInfo.Description}\"");

            return Task.FromResult(new Get_ServerDescription_Responses { ServerDescription = new SiLAFramework.String { Value = ServerInfo.Description } });
        }

        public override Task<Get_ServerVendorURL_Responses> Get_ServerVendorURL(Get_ServerVendorURL_Parameters request, ServerCallContext context)
        {
            CheckMetadata(context);

            Logger.Info($"Request ->  Response = \"{ServerInfo.VendorURI}\"");

            return Task.FromResult(new Get_ServerVendorURL_Responses { ServerVendorURL = new SiLAFramework.String { Value = ServerInfo.VendorURI } });
        }

        public override Task<Get_ServerVersion_Responses> Get_ServerVersion(Get_ServerVersion_Parameters request, ServerCallContext context)
        {
            CheckMetadata(context);

            Logger.Info($"Request ->  Response = \"{ServerInfo.Version}\"");

            return Task.FromResult(new Get_ServerVersion_Responses { ServerVersion = new SiLAFramework.String { Value = ServerInfo.Version } });
        }

        public override Task<Get_ImplementedFeatures_Responses> Get_ImplementedFeatures(Get_ImplementedFeatures_Parameters request, ServerCallContext context)
        {
            CheckMetadata(context);

            Logger.Info("Request -> ");

            Get_ImplementedFeatures_Responses response = new Get_ImplementedFeatures_Responses();
            foreach (var f in this.SiLAServer.ImplementedFeatures)
            {
                response.ImplementedFeatures.Add(new SiLAFramework.String { Value = f });
            }

            return Task.FromResult(response);
        }

        /// <summary>
        /// Check if request headers contain sila client meta data and issue an framework error in that case.
        /// <param name="context">The context of the gRPC call to be checked.</param>
        /// </summary>
        private void CheckMetadata(ServerCallContext context)
        {
            if (SilaClientMetadata.GetAllSilaClientMetadataIdentifiers(context.RequestHeaders).Count > 0)
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(SiLAFramework.FrameworkError.Types.ErrorType.NoMetadataAllowed, "Requests of the SiLAService feature MUST NOT contain SiLA Client Metadata"));
            }
        }

        private sealed class ExtentedStringWriter : StringWriter
        {
            public ExtentedStringWriter(StringBuilder builder, Encoding desiredEncoding)
                : base(builder)
            {
                this.Encoding = desiredEncoding;
            }

            public override Encoding Encoding { get; }
        }
    }
}