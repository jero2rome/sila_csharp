using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Common.Logging;
using Grpc.Core;
using Sila2.Org.Silastandard;
using Sila2.Utils;

namespace Sila2
{
    /**
     * Class responsible for managing the observable commands utilizing the
     * ObservableCommandWrapper
     */
    public class ObservableCommandManager<TParameter, TResponse> : IDisposable
    {
        private static readonly ILog _log = LogManager.GetLogger<ObservableCommandManager<TParameter, TResponse>>();

        private readonly Dictionary<Guid, ObservableCommandWrapper<TParameter, TResponse>> _commands =
            new Dictionary<Guid, ObservableCommandWrapper<TParameter, TResponse>>();

        private readonly TimeSpan _lifetimeOfExecution;

        /// <summary>
        /// Constructor of Observable command manager
        /// </summary>
        /// <param name="lifetimeOfExecution">The the duration during which a Command Execution UUID is valid.</param>
        public ObservableCommandManager(TimeSpan lifetimeOfExecution)
        {
            if (lifetimeOfExecution <= TimeSpan.Zero)
            {
                throw new InvalidOperationException(
                    $"Lifetime of execution must be greater than 0 but {lifetimeOfExecution} was specified");
            }

            _lifetimeOfExecution = lifetimeOfExecution;
        }

        /// <summary>
        /// Add an observable command execution wrapped as a task to the manager command list
        ///
        /// Note: only use this method if progress and remaining execution time shall be updated
        /// within the command execution and propagated back out to the ExecutionInfo stream
        /// 
        /// </summary>
        /// <param name="parameter">The parameter of the observable command</param>
        /// <param name="func">Function reference which will be invoked internally with parameters</param>
        /// <returns>Observable command wrapper as a task</returns>
        public Task<ObservableCommandWrapper<TParameter, TResponse>> AddCommand(
            TParameter parameter,
            Func<IProgress<ExecutionInfo>, TParameter, CancellationToken, TResponse> func)
        {
            return AddCommand(new ObservableCommandWrapper<TParameter, TResponse>(parameter, _lifetimeOfExecution, func));
        }

        /// <summary>
        /// Add an observable command execution wrapped as a task to the manager command list
        ///
        /// Note: only use this method if progress and remaining execution time shall be updated
        /// within the command execution and propagated back out to the ExecutionInfo stream
        /// 
        /// </summary>
        /// <param name="parameter">The parameter of the observable command</param>
        /// <param name="func">Function reference which will be invoked internally with parameters</param>
        /// <returns>Observable command wrapper as a task</returns>
        public Task<ObservableCommandWrapper<TParameter, TResponse>> AddCommand(
            TParameter parameter,
            Func<IProgress<ExecutionInfo>, TParameter, Guid, CancellationToken,TResponse> func)
        {
            return AddCommand(new ObservableCommandWrapper<TParameter, TResponse>(parameter, _lifetimeOfExecution, func));
        }

        private Task<ObservableCommandWrapper<TParameter, TResponse>> AddCommand(
            ObservableCommandWrapper<TParameter, TResponse> command)
        {
            _log.Debug($"Adding command: {command.Id}");
            _commands.Add(command.Id, command);
            // TODO: this execution should be handled by the task scheduler
            var response = command.Execute()
                .ContinueWith(task => Task.FromResult(command)).Unwrap();
            command.CommandExpired += onCommandExpired;
            return response;
        }

        private void onCommandExpired(object sender, CommandExpiredArgs args)
        {
            RemoveCommand(args.CommandId);
        }

        private void RemoveCommand(Guid commandId)
        {
            _log.Info($"Removing command: {commandId}");
            if (_commands.TryGetValue(commandId, out var command))
            {
                command.Dispose();
                _commands.Remove(commandId);
            }
            else
            {
                _log.Warn($"No such command with ID {commandId} was found");
            }
        }

        /// <summary>
        /// Gets the observable command wrapper of a given command execution UUID from an Info stream
        /// </summary>
        /// <param name="commandExecutionUuid"></param>
        /// <returns></returns>
        public ObservableCommandWrapper<TParameter, TResponse> GetCommand(CommandExecutionUUID commandExecutionUuid)
        {
            var commandId = Guid.Parse(commandExecutionUuid.Value);
            return GetCommand(commandId);
        }

        private ObservableCommandWrapper<TParameter, TResponse> GetCommand(Guid commandId)
        {
            if (!_commands.TryGetValue(commandId, out var command))
            {
                var message = $"No such command with ID {commandId} was found";
                _log.Warn(message);
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(
                    FrameworkError.Types.ErrorType.InvalidCommandExecutionUuid,
                    message));
            }

            return command;
        }

        /// <summary>
        /// Cleans up all observable command wrappers held by the manager. Tasks will be cancelled.
        /// </summary>
        public void Dispose()
        {
            _log.Info("Cleaning up manager");
            foreach (var observableCommandWrapper in _commands.Reverse())
            {
                RemoveCommand(observableCommandWrapper.Key);
            }
        }

        /// <summary>
        /// Registers response stream for command execution info updates for a given command UUID
        /// </summary>
        /// <param name="request">Request containing command ID to register</param>
        /// <param name="responseStream">client response stream to register command execution info updates to</param>
        /// <param name="cancellationToken">cancellation token from open client connection</param>
        /// <returns></returns>
        public async Task RegisterForInfo(CommandExecutionUUID request,
            IServerStreamWriter<ExecutionInfo> responseStream,
            CancellationToken cancellationToken)
        {
            var command = GetCommand(request);
            await command.AddInfoObserver(responseStream, cancellationToken);
            try
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    GetCommand(request);
                    await Task.Delay(Constants.AWAIT_PERIOD, cancellationToken);
                }
            }
            catch (TaskCanceledException)
            {
                _log.Debug($"Cancelled command: {request.Value}");
            }
        }
    }
}
