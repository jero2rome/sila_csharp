using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Common.Logging;
using Grpc.Core;
using Sila2.Org.Silastandard;
using Sila2.Utils;

namespace Sila2
{
    /// <summary>
    /// Arguments of the event triggered upon command expiration
    /// </summary>
    public class CommandExpiredArgs : EventArgs
    {
        /// <summary>
        /// Identifier of expired command
        /// </summary>
        public Guid CommandId { get; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="id">identifier of expired command</param>
        public CommandExpiredArgs(Guid id)
        {
            CommandId = id;
        }
    }

    /// <summary>
    /// Class which
    ///
    /// TODO: Handle response stream cleanup once closed, think about this!
    ///       When client closes info stream, when lifetimeExecutionUUID expires, etc.. 
    /// </summary>
    /// <typeparam name="TParameter"></typeparam>
    /// <typeparam name="TResponse"></typeparam>
    ///
    public sealed class ObservableCommandWrapper<TParameter, TResponse> : IDisposable
    {
        private static readonly ILog _log = LogManager.GetLogger<ObservableCommandWrapper<TParameter, TResponse>>();
        private readonly Task<TResponse> _task;

        private readonly HashSet<Tuple<IServerStreamWriter<ExecutionInfo>, CancellationToken>> _infoResponseTuples =
            new HashSet<Tuple<IServerStreamWriter<ExecutionInfo>, CancellationToken>>();

        private readonly ExecutionInfo _executionInfo = new ExecutionInfo();
        private readonly Progress<ExecutionInfo> _progress = new Progress<ExecutionInfo>();
        private readonly CancellationTokenSource _taskTokenSource = new CancellationTokenSource();
        private readonly object _expirationTimeLock = new object(); //TODO: is this lock even necessary?
        private readonly object _executionInfoLock = new object();
        private SiLAError _commandError;
        private DateTime _expirationTime;
        private Task _expirationTask;

        /// <summary>
        /// Command Execution Identifier
        /// </summary>
        public Guid Id { get; }

        /// <summary>
        /// Command confirmation including command execution UUID and lifetime of execution 
        /// </summary>
        public CommandConfirmation Confirmation => GetCommandConfirmation();

        private bool IsCompleted =>
            _executionInfo.CommandStatus == ExecutionInfo.Types.CommandStatus.FinishedSuccessfully ||
            _executionInfo.CommandStatus == ExecutionInfo.Types.CommandStatus.FinishedWithError;

        /// <summary>
        /// Event used to listen when a command has expired
        /// </summary>
        public event EventHandler<CommandExpiredArgs> CommandExpired;

        /// <summary>
        /// Constructs an observable command wrapper which manages the execution state of a specified task/func.
        /// Additionally, handles state changes to registered client streams and additional logic
        /// </summary>
        /// <param name="parameter">Input parameter of Observable Command request</param>
        /// <param name="lifetimeOfExecution">The lifetime of execution that the command is guaranteed to exist fort</param>
        /// <param name="func">A wrapped Func to be passed as input which encapsulates the execution of the observable command</param>
        public ObservableCommandWrapper(
            TParameter parameter,
            TimeSpan lifetimeOfExecution,
            Func<IProgress<ExecutionInfo>, TParameter, CancellationToken, TResponse> func)
        {
            Id = Guid.NewGuid();
            RegisterExecutionLifetime(lifetimeOfExecution);
            _task = new Task<TResponse>(() => func.Invoke(_progress, parameter, _taskTokenSource.Token));
            lock (_executionInfoLock)
            {
                _executionInfo = new ExecutionInfo { CommandStatus = ExecutionInfo.Types.CommandStatus.Waiting };
            }
        }

        /// <summary>
        /// Constructs an observable command wrapper which manages the execution state of a specified task/func.
        /// Additionally, handles state changes to registered client streams and additional logic
        /// </summary>
        /// <param name="parameter">Input parameter of Observable Command request</param>
        /// <param name="lifetimeOfExecution">The lifetime of execution that the command is guaranteed to exist fort</param>
        /// <param name="func">A wrapped Func to be passed as input which encapsulates the execution of the observable command</param>
        public ObservableCommandWrapper(
            TParameter parameter,
            TimeSpan lifetimeOfExecution,
            Func<IProgress<ExecutionInfo>, TParameter, Guid, CancellationToken, TResponse> func)
        {
            Id = Guid.NewGuid();
            RegisterExecutionLifetime(lifetimeOfExecution);
            _task = new Task<TResponse>(() => func.Invoke(_progress, parameter, Id, _taskTokenSource.Token));
            lock (_executionInfoLock)
            {
                _executionInfo = new ExecutionInfo { CommandStatus = ExecutionInfo.Types.CommandStatus.Waiting };
            }
        }

        private CommandConfirmation GetCommandConfirmation()
        {
            return new CommandConfirmation
            {
                CommandExecutionUUID = new CommandExecutionUUID { Value = Id.ToString() },
                LifetimeOfExecution = GetLifetimeOfExecution()
            };
        }

        /// <summary>
        /// Wraps the command with appropriate state notifications
        /// </summary>
        /// TODO: Handle progress and remaining time
        /// <returns></returns>
        public Task Execute()
        {
            _log.Debug($"Starting command: {Id}");
            var notificationTask = SetStateAndNotify(ExecutionInfo.Types.CommandStatus.Running);
            _task.ContinueWith(HandleCommandError,
                TaskContinuationOptions.OnlyOnFaulted | TaskContinuationOptions.ExecuteSynchronously);
            _task.ContinueWith(HandleCommandSuccess,
                TaskContinuationOptions.OnlyOnRanToCompletion | TaskContinuationOptions.ExecuteSynchronously);
            _task.Start();
            return notificationTask;
        }

        /// <summary>
        /// Adds a registered client response stream and cancellation token to receive updates of the ExecutionInfo of
        /// the executing command/task
        /// </summary>
        /// <param name="responseStream">gRPC response stream connected to a client</param>
        /// <param name="infoStreamCancellationToken">cancellation token of the connection</param>
        /// <returns>Async task of writing execution info to the response stream</returns>
        public Task AddInfoObserver(IServerStreamWriter<ExecutionInfo> responseStream,
            CancellationToken infoStreamCancellationToken)
        {
            var responseTuple =
                new Tuple<IServerStreamWriter<ExecutionInfo>, CancellationToken>(responseStream,
                    infoStreamCancellationToken);
            _infoResponseTuples.Add(responseTuple);
            infoStreamCancellationToken.Register(RemoveResponse(responseTuple));
            _log.Debug($"Added {responseTuple.Item1} stream: {responseTuple.Item1.GetHashCode()}");
            _progress.ProgressChanged += ProgressOnProgressChanged;
            return responseStream.WriteAsync(getSiLAExecutionInfo());
        }

        /// <summary>
        /// Removes the response tuple when the response stream is not needed anymore
        /// </summary>
        /// <param name="responseTuple"></param>
        /// <returns></returns>
        private Action RemoveResponse(Tuple<IServerStreamWriter<ExecutionInfo>, CancellationToken> responseTuple)
        {
            return () =>
            {
                _log.Debug($"Removing {responseTuple.Item1} stream: {responseTuple.Item1.GetHashCode()}");
                var removed = _infoResponseTuples.Remove(responseTuple);
                if (!removed)
                    throw new InvalidOperationException($"Failed to remove {responseTuple.Item2.GetHashCode()}");
            };
        }

        /// <summary>
        /// Query the result of the executed command, throws a SiLA error in the form of a <see cref="RpcException"/>
        /// if the result is requested when the command has not completed.
        /// </summary>
        /// <returns>The result of the executed Func</returns>
        public TResponse Result()
        {
            if (!IsCompleted)
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateFrameworkError(
                    FrameworkError.Types.ErrorType.CommandExecutionNotFinished,
                    "The command execution has not been finished yet"));
            }

            if (_commandError != null)
            {
                ErrorHandling.RaiseSiLAError(_commandError);
            }

            return _task.Result;
        }

        private async void ProgressOnProgressChanged(object sender, ExecutionInfo e)
        {
            lock (_executionInfoLock)
            {
                //TODO: This is very delicate. Race condition can happen if this check is not done.
                if (_executionInfo.CommandStatus == ExecutionInfo.Types.CommandStatus.FinishedSuccessfully ||
                    _executionInfo.CommandStatus == ExecutionInfo.Types.CommandStatus.FinishedWithError ||
                    _executionInfo?.ProgressInfo?.Value == 1)
                {
                    return;
                }
                _executionInfo.ProgressInfo = e.ProgressInfo;
                _executionInfo.EstimatedRemainingTime = e.EstimatedRemainingTime;
            }
            await NotifyState();
        }

        private ExecutionInfo getSiLAExecutionInfo()
        {
            lock (_executionInfoLock)
            {
                var executionInfo = new ExecutionInfo
                {
                    CommandStatus = _executionInfo.CommandStatus,
                    ProgressInfo = _executionInfo.ProgressInfo ?? new Real(),
                    EstimatedRemainingTime = _executionInfo.EstimatedRemainingTime ?? new Duration(),
                    UpdatedLifetimeOfExecution = GetLifetimeOfExecution()
                };
                return executionInfo;
            }
        }

        private Duration GetLifetimeOfExecution()
        {
            lock (_expirationTimeLock)
            {
                var lifetimeOfExecution = _expirationTime - DateTime.Now;
                return new Duration
                {
                    Seconds = lifetimeOfExecution.Seconds,
                    Nanos = lifetimeOfExecution.Milliseconds * 10000
                };
            }
        }

        /// <summary>
        /// Registers a timer for lifetime of execution, extends it if the Wrapper task has not been completed yet
        /// </summary>
        /// <param name="lifetimeOfExecution">The lifetime of execution of the observable command</param>
        private void RegisterExecutionLifetime(TimeSpan lifetimeOfExecution)
        {
            _expirationTime = DateTime.Now + lifetimeOfExecution;
            _log.Debug(
                $"Extending lifetime of execution: {lifetimeOfExecution} Current Time: {DateTime.Now.Second}, Expiration time: {_expirationTime.Second}");

            _expirationTask = Task.Delay(lifetimeOfExecution).ContinueWith(async task =>
                {
                    if (IsCompleted)
                    {
                        _log.Debug($"Command {Id} expired!");
                        CommandExpired?.Invoke(this, new CommandExpiredArgs(Id));
                    }
                    else
                    {
                        // Extend the lifetime of execution
                        RegisterExecutionLifetime(lifetimeOfExecution);
                        await NotifyState();
                    }
                }, TaskContinuationOptions.OnlyOnRanToCompletion);
        }

        private Task HandleCommandSuccess(Task<TResponse> task)
        {
            return SetStateAndNotify(ExecutionInfo.Types.CommandStatus.FinishedSuccessfully);
        }

        private Task HandleCommandError(Task<TResponse> task)
        {
            _log.Warn($"Error occured during observable command {Id} task execution {task.Exception}");
            if (task.Exception?.InnerException is RpcException exception)
            {
                _commandError = ErrorHandling.RetrieveSiLAError(exception);
            }
            if (_commandError == null)
            {
                string errorMessage;
                if (task.Exception?.InnerException != null)
                {
                    if (task.Exception?.InnerException is OperationCanceledException)
                    {
                        errorMessage = "Observable command has been cancelled";
                    }
                    else
                    {
                        errorMessage = "An unknown error occured: " + ErrorHandling.FlattenExceptionsMessageToString(task.Exception);
                    }
                }
                else
                {
                    errorMessage = task.Exception?.Message.Length > 0
                        ? task.Exception.Message
                        : $"An unknown error occured ({task.Exception?.GetType().Name})";
                }
                _commandError = ErrorHandling.CreateUndefinedExecutionError(errorMessage);
            }

            return SetStateAndNotify(ExecutionInfo.Types.CommandStatus.FinishedWithError);
        }

        private Task SetStateAndNotify(ExecutionInfo.Types.CommandStatus status)
        {
            lock (_executionInfoLock)
            {
                _executionInfo.CommandStatus = status;
            }
            return NotifyState();
        }

        private Task NotifyState()
        {
            if (_taskTokenSource.IsCancellationRequested)
                return Task.CompletedTask;

            var executionInfo = getSiLAExecutionInfo();
            _log.Debug($"Command execution info: {executionInfo}");
            var tasks = new List<Task>();
            lock (_executionInfoLock)
            {
                foreach (var (serverStreamWriter, infoStreamCancellationToken) in _infoResponseTuples)
                {
                    if (infoStreamCancellationToken.IsCancellationRequested)
                        continue;
                    tasks.Add(serverStreamWriter.WriteAsync(executionInfo));
                }
            }
            return Task.WhenAll(tasks);
        }

        /// <summary>
        /// Cleans up the resoures of the observable command wrapper. Verifies that the underlying
        /// command execution has terminated and terminates it if not already.
        /// </summary>
        public void Dispose()
        {
            _log.Info($"Cleaning up command: {Id}...");
            if (!_taskTokenSource.IsCancellationRequested)
            {
                _taskTokenSource.Cancel();
            }

            try
            {
                _task.Wait();
            }
            catch (Exception)
            {
                // ignored
            }

            _infoResponseTuples.Clear();
            _log.Info("Cleaned!");
        }
    }
}