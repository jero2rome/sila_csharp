﻿namespace Sila2.Utils
{
    using Google.Protobuf;
    using SiLAFramework = Org.Silastandard;

    /// <summary>
    /// Provides methods to create and handle Any type objects.
    /// </summary>
    public class AnyType
    {
        #region Creation

        /// <summary>Creates an Any type object with a string type of the given value. </summary>
        /// <param name="value">The string value</param>
        /// <returns>The Any type object</returns>
        public static SiLAFramework.Any CreateAnyTypeObject(string value)
        {
            return new SiLAFramework.Any { Type = GenerateBasicDataTypeString(BasicType.String), Payload = ByteString.CopyFrom(new SiLAFramework.String { Value = value }.ToByteArray()) };
        }

        /// <summary>Creates an Any type object with an integer type of the given value. </summary>
        /// <param name="value">The integer value</param>
        /// <returns>The Any type object</returns>
        public static SiLAFramework.Any CreateAnyTypeObject(int value)
        {
            return new SiLAFramework.Any { Type = GenerateBasicDataTypeString(BasicType.Integer), Payload = ByteString.CopyFrom(new SiLAFramework.Integer { Value = value }.ToByteArray()) };
        }

        /// <summary>Creates an Any type object with a double type of the given value. </summary>
        /// <param name="value">The double value</param>
        /// <returns>The Any type object</returns>
        public static SiLAFramework.Any CreateAnyTypeObject(double value)
        {
            return new SiLAFramework.Any { Type = GenerateBasicDataTypeString(BasicType.Real), Payload = ByteString.CopyFrom(new SiLAFramework.Real { Value = value }.ToByteArray()) };
        }

        /// <summary>Creates an Any type object with a booelan type of the given value. </summary>
        /// <param name="value">The boolean value</param>
        /// <returns>The Any type object</returns>
        public static SiLAFramework.Any CreateAnyTypeObject(bool value)
        {
            return new SiLAFramework.Any { Type = GenerateBasicDataTypeString(BasicType.Boolean), Payload = ByteString.CopyFrom(new SiLAFramework.Boolean { Value = value }.ToByteArray()) };
        }

        /// <summary>Creates an Any type object with a Binary type of the given value. </summary>
        /// <param name="value">The Binary value object</param>
        /// <returns>The Any type object</returns>
        public static SiLAFramework.Any CreateAnyTypeObject(SiLAFramework.Binary value)
        {
            return new SiLAFramework.Any { Type = GenerateBasicDataTypeString(BasicType.Binary), Payload = ByteString.CopyFrom(value.ToByteArray()) };
        }

        /// <summary>Creates an Any type object with a Date type of the given value. </summary>
        /// <param name="value">The Date value object</param>
        /// <returns>The Any type object</returns>
        public static SiLAFramework.Any CreateAnyTypeObject(SiLAFramework.Date value)
        {
            return new SiLAFramework.Any { Type = GenerateBasicDataTypeString(BasicType.Date), Payload = ByteString.CopyFrom(value.ToByteArray()) };
        }

        /// <summary>Creates an Any type object with a Time type of the given value. </summary>
        /// <param name="value">The Time value object</param>
        /// <returns>The Any type object</returns>
        public static SiLAFramework.Any CreateAnyTypeObject(SiLAFramework.Time value)
        {
            return new SiLAFramework.Any { Type = GenerateBasicDataTypeString(BasicType.Time), Payload = ByteString.CopyFrom(value.ToByteArray()) };
        }

        /// <summary>Creates an Any type object with a Timestamp type of the given value. </summary>
        /// <param name="value">The Timestamp value object</param>
        /// <returns>The Any type object</returns>
        public static SiLAFramework.Any CreateAnyTypeObject(SiLAFramework.Timestamp value)
        {
            return new SiLAFramework.Any { Type = GenerateBasicDataTypeString(BasicType.Timestamp), Payload = ByteString.CopyFrom(value.ToByteArray()) };
        }

        /// <summary>Generates the XML data type string for the given SiLA Basic type. </summary>
        /// <param name="basicDataType">The SiLA Basic data type</param>
        /// <returns>The XML type string according to the schema AnyTypeDataType.xsd</returns>
        public static string GenerateBasicDataTypeString(BasicType basicDataType)
        {
            string typeStringXML;
            var value = new DataTypeType { Item = basicDataType };
            var xmlSerializer = new System.Xml.Serialization.XmlSerializer(value.GetType());
            using (var textWriter = new System.IO.StringWriter())
            {
                xmlSerializer.Serialize(textWriter, value);
                typeStringXML = textWriter.ToString();
            }

            return typeStringXML;
        }

        #endregion

        private static string _schemaValidationError;

        /// <summary>Validates the given Any type XML type string against the AnyTypeDataType.xsd schema and extracts the encoded data type. </summary>
        /// <param name="typeStringXml">The XML type string</param>
        /// <returns>The defined SiLA data type</returns>
        /// <exception cref="System.Xml.Schema.XmlSchemaValidationException"></exception>
        public static DataTypeType ExtractAnyType(string typeStringXml)
        {
            _schemaValidationError = string.Empty;

            // validate XML string against the schema
            using (var stringReader = new System.IO.StringReader(typeStringXml))
            {
                // create an XML reader using the FeatureDefinition schema for validating
                var settings = new System.Xml.XmlReaderSettings
                {
                    DtdProcessing = System.Xml.DtdProcessing.Parse,
                    MaxCharactersFromEntities = 1024,
                    ValidationType = System.Xml.ValidationType.Schema
                };
                settings.ValidationEventHandler += ValidationCallBack;

                FeatureGenerator.AddSchemaFromResource(settings, "AnyTypeDataType.xsd");
                FeatureGenerator.AddSchemaFromResource(settings, "DataTypes.xsd");
                FeatureGenerator.AddSchemaFromResource(settings, "Constraints.xsd");

                using (var xmlReader = System.Xml.XmlReader.Create(stringReader, settings))
                {
                    // parse the file
                    while (xmlReader.Read()) { }
                }
            }

            if (!string.IsNullOrEmpty(_schemaValidationError))
            {
                throw new System.Xml.Schema.XmlSchemaValidationException("Schema validation error: " + _schemaValidationError);
            }

            // convert the XML content into a data type object
            try
            {
                var serializer = new System.Xml.Serialization.XmlSerializer(typeof(DataTypeType));
                using (var reader = new System.IO.StringReader(typeStringXml))
                {
                    var dataType = (DataTypeType)serializer.Deserialize(reader);
                    reader.Close();
                    return dataType;
                }
            }
            catch (System.Exception ex)
            {
                var errorMessage = $"Invalid data type definition (error while deserializing XML: {ex.Message} {(ex.InnerException != null ? ex.InnerException.Message : string.Empty)}";
                throw new System.Xml.Schema.XmlSchemaValidationException(errorMessage);
            }
        }

        private static void ValidationCallBack(object sender, System.Xml.Schema.ValidationEventArgs args)
        {
            _schemaValidationError = args.Message;
        }
    }
}
