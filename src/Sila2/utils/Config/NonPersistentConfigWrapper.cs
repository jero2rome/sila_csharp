namespace Sila2.Utils
{
    using System;
    using Common.Logging;

    /// <summary>
    /// Class responsible for reading and writing ServerConfigs to local in process cache
    /// Non persistent over multiple server lifetimes
    /// </summary>
    public class NonPersistentConfigWrapper<TConfig> : IConfigWrapper<TConfig>
    {
        private static readonly ILog log = LogManager.GetLogger<NonPersistentConfigWrapper<TConfig>>();
        private TConfig cache;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="config">config to load into the cache</param>
        public NonPersistentConfigWrapper(TConfig config)
        {
            cache = config;
        }

        /// <summary>
        /// Writes the specified config into the db
        /// </summary>
        /// <param name="config"></param>
        public void SetConfig(TConfig config)
        {
            log.Info($"Updating Server configuration: {config}");
            cache = config;
        }

        /// <summary>
        /// Get the server configuration
        /// </summary>
        /// <returns></returns>
        public TConfig GetConfig()
        {
            if (cache == null)
            {
                throw new InvalidOperationException("Cannot get chache, must call readConfiguration first!");
            }
            log.Trace($"config: {cache}");
            return cache;
        }
    }
}