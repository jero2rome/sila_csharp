namespace Sila2.Utils
{
    using System;
    using System.IO;
    using Common.Logging;
    using System.Runtime.Serialization.Json;

    /// <summary>
    /// Class responsible for reading and writing ServerConfigs to a specified file
    /// </summary>
    public class PersistentConfigWrapper<TConfig> : IConfigWrapper<TConfig>
    {
        private static readonly ILog log = LogManager.GetLogger<PersistentConfigWrapper<TConfig>>();
        private static readonly object fileLock = new object();
        private readonly string dbFile;
        private readonly DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(TConfig));
        private ServerConfig defaultConfig;


        /// <summary>
        /// Constructor creates config file if does not exist and sets the default configuration data
        /// </summary>
        public PersistentConfigWrapper(string configFile, TConfig defaultConfig)
        {
            dbFile = Path.GetFullPath(configFile);
            try
            {
                SetDb(configFile);
                log.Info($"Creating default ServerConfig");
                SetConfig(defaultConfig);
            }
            catch (FileLoadException e)
            {
                log.Warn(e.Message);
            }
        }
        
        /// <summary>
        /// Constructor that creates file if not exists already
        /// </summary>
        public PersistentConfigWrapper(string configFile)
        {
            dbFile = Path.GetFullPath(configFile);
        }

        /// <summary>
        /// Writes the specified config into the db
        /// </summary>
        public void SetConfig(TConfig config)
        {
            lock (fileLock)
            {
                using (var fileStream = File.Create(dbFile))
                {
                    log.Debug($"writing {typeof(TConfig)}: {config} to {dbFile}");
                    serializer.WriteObject(fileStream, config);
                }
            }
            log.Info($"Updating Server configuration {config}");
        }

        /// <summary>
        /// Get the server configuration
        /// </summary>
        /// <returns></returns>
        public TConfig GetConfig()
        {
            lock (fileLock)
            {
                var config = ReadConfiguration();
                if (config == null)
                {
                    throw new InvalidOperationException("Cannot get cache, must call readConfiguration first!");
                }
                return config;
            }
        }

        /// <summary>
        /// Loads the server configuration from the db i.e. file
        /// </summary>
        private TConfig ReadConfiguration()
        {
            using (var fileStream = File.Open(dbFile, FileMode.Open))
            {
                var config = (TConfig) serializer.ReadObject(fileStream);
                return config;
            }
        }

        /// <summary>
        /// Creates a database for the specified file if not already created
        /// </summary>
        /// <exception cref="FileLoadException">The file could not be created as it already exists</exception>
        private void SetDb(string filename)
        {
            CreateFile(filename);
        }

        /// <summary>
        /// Create a file if does not exist already and populate it with an empty JSON object
        /// </summary>
        /// <param name="dbFile"></param>
        /// <exception cref="FileLoadException">The file could not be created as it already exists</exception>
        private static void CreateFile(string dbFile)
        {
            if (File.Exists(dbFile)) throw new FileLoadException($"Config file already exists: {dbFile}");
            log.Info($"Creating server configuration file: {dbFile}");
            var dir = Path.GetDirectoryName(dbFile);
            if (!string.IsNullOrEmpty(dir))
            {
                Directory.CreateDirectory(dir);   
            }
            using (var fileStream = File.Create(dbFile))
            {
                using (var streamWriter = new StreamWriter(fileStream))
                {
                    streamWriter.Write("{}");
                }
            }
        }
    }
}