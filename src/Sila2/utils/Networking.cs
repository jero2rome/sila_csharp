namespace Sila2.Utils
{
    using System;
    using System.Net;
    using System.Net.NetworkInformation;
    using System.Linq;
    using System.Collections.Generic;
    using System.Net.Sockets;
    using Common.Logging;

    /// <summary>
    /// Networking utility library listing network interfaces and finding 
    /// interfaces based on name or CIDR
    /// </summary>
    public class Networking
    {
        private static ILog Log = LogManager.GetLogger<Networking>();

        /// <summary>
        /// Finds the network interface that has a matching interface name or matching unicast address
        /// </summary>
        /// <param name="interfaceNameOrCIDR">interface name to find or the IPv4/IPv6 address or CIDR mask. </param>
        /// <returns>the matching found network interface</returns>
        public static NetworkInterface GetNetworkInterface(string interfaceNameOrCIDR)
        {    
            try
            {
                return GetNetworkInterfaceByCIDR(interfaceNameOrCIDR);
            }
            catch(NetworkInterfaceNotFoundException e)
            {
                Log.Warn($"Could not get network interface by IP address or CIDR: {e.Message}");
            }
            catch(FormatException)
            {
                Log.Warn($"{interfaceNameOrCIDR} is not an IP address or CIDR mask");
            }
            
            return GetNetworkInterfaceByName(interfaceNameOrCIDR);
        }

        public static NetworkInterface GetNetworkInterfaceByName(string interfaceName)
        {
            if(interfaceName == null)
            {
                throw new NetworkInterfaceNotFoundException("No network interface name was specified");
            }

            foreach (var nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                //if(interfaceName.Contains(nic.Name))
                if(interfaceName == nic.Name)
                {
                    if(nic.OperationalStatus != OperationalStatus.Up)
                    {
                        Log.Warn($"Interface {nic.Name} is not Up!");
                    }
                    Log.Info($"Interface {nic.Name} found!");
                    return nic;
                }
            }
            throw new NetworkInterfaceNotFoundException($"Interface {interfaceName} not found for Server Discovery");
        }

        /// <summary>
        /// Finds the first network interface that has a matching unicast address-
        /// </summary>
        /// <param name="mask">IPv4/IPv6 address or CIDR mask.</param>
        /// <returns>a matching interface</returns>
        public static NetworkInterface GetNetworkInterfaceByCIDR(string mask)
        {
            if(mask.Contains('/'))
            {
                IPNetwork ipnetwork = IPNetwork.Parse(mask);
                foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
                {
                    var unicastAddresses = nic.GetIPProperties().UnicastAddresses;
                    if (!unicastAddresses.Any(ua => ipnetwork.Contains(ua.Address))) continue;
                    Log.Info($"Interface {nic.Name} found!");
                    return nic;
                }
            }
            else
            {
                IPAddress address = IPAddress.Parse(mask);
                foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
                {
                    var unicastAddresses = nic.GetIPProperties().UnicastAddresses;
                    if(unicastAddresses.Select(ua => ua.Address).Contains(address))
                    {
                        Log.Info($"Interface {nic.Name} found!");
                        return nic;
                    }
                }
            }
            throw new NetworkInterfaceNotFoundException($"No network interface found for IP address/CIDR Mask {mask}");
        }

        /// <summary>
        /// Finds a valid network interface on the system which is NOT loopback and is up
        /// </summary>
        /// <returns>a valid network interface</returns>
        public static NetworkInterface FindValidNetworkInterface()
        {
            foreach (var nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (nic.OperationalStatus != OperationalStatus.Up ||
                    nic.NetworkInterfaceType == NetworkInterfaceType.Loopback) continue;
                Log.Info($"Interface {nic.Name} found!");
                return nic;
            }
            throw new NetworkInterfaceNotFoundException($"No network interface was found that was UP and not Loopback");
        }

        /// <summary>
        /// Creates a filtering func that can be passed to the Makaretu MulticastService.
        /// </summary>
        /// <param name="selectedInterfaces">Whitelist of network interfaces</param>
        /// <returns>
        /// A func that takes a list of interfaces and returns a list of those that are whitelisted.
        /// If no interfaces are passed to <para>selectedInterfaces</para>, no filter is applied.
        /// </returns>
        public static Func<IEnumerable<NetworkInterface>, IEnumerable<NetworkInterface>> CreateFilterFunc(params NetworkInterface[] selectedInterfaces)
        {
            IEnumerable<NetworkInterface> FilterFunc(IEnumerable<NetworkInterface> nics)
            {
                if (selectedInterfaces.Length > 0)
                {
                    var filtered = nics.Where(ni => selectedInterfaces.Select(n => n.Id).Contains(ni.Id));
                    Log.Debug($"Filtered {nics.Count()} interfaces down to {filtered.Count()} using a whitelist of {selectedInterfaces.Length}");
                    return filtered;
                }
                return nics;
            }
            return FilterFunc;
        }

        /// <summary>
        /// Enumerates the InterNetwork Unicast addresses for a given network interface.
        /// </summary>
        /// <param name="networkInterface">A network interface to inspect</param>
        /// <returns>list of InterNetwork Unicast IP v4 addresses</returns>
        public static IEnumerable<IPAddress> ListInterNetworkAddresses(NetworkInterface networkInterface)
        {
            var unicastAddresses = networkInterface.GetIPProperties().UnicastAddresses.Select(u => u.Address);
            return unicastAddresses.Where(a => a.AddressFamily == AddressFamily.InterNetwork);
        }
    }
}