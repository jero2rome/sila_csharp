﻿using System;

namespace Sila2.Utils
{
    public class ServerCli
    {
        public static void BlockUntilStop() {
            Console.WriteLine("Type \'stop\' to stop the server.");
            bool run = true;
            string line;
            while (run && (line = Console.ReadLine()) != null)
            {
                if (line.Contains("stop"))
                {
                    run = false;
                }
                else
                {
                    Console.WriteLine("Unknown command:" + line);
                }
            }
        }
    }
}