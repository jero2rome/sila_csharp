﻿namespace Sila2.Utils
{
    using System.Globalization;
    using System.Linq;

    /// <summary>
    /// This class provides functions to validate command parameters against their according constraints as defined for the given parameter in the given Feature Definition.
    /// Raises a SiLA validation error in case a constraint is violated.
    /// TODO: implement functions for all SiLA Basic types that can have validatable constraints
    /// </summary>
    public static class Validation
    {
        /// <summary>
        /// Validates the given value of a Real type parameter according to the constrains defined for the parameter of the given command in the given feature
        /// </summary>
        /// <param name="parameterValue">The value to be validated.</param>
        /// <param name="feature">The feature that defines the given parameter.</param>
        /// <param name="commandIdentifier">The identifier of the command the parameter is defined for.</param>
        /// <param name="parameterIdentifier">The identifier of the parameter thats constraints shall be used for validation.</param>
        public static void ValidateParameter(double parameterValue, Feature feature, string commandIdentifier, string parameterIdentifier)
        {
            // lookup command
            if (!(feature.Items.FirstOrDefault(i => i is FeatureCommand && ((FeatureCommand)i).Identifier == commandIdentifier) is FeatureCommand command)) { return; }

            // lookup parameter
            if (!(command.Parameter.FirstOrDefault(p => p.Identifier == parameterIdentifier) is SiLAElement parameter)) { return; }

            // get parameter data type
            DataTypeType dataType;
            if (parameter.DataType.Item is string s)
            {
                // get according datatype definition
                if (!(feature.Items.FirstOrDefault(i => i is SiLAElement && ((SiLAElement)i).Identifier == s) is SiLAElement silaElement)) { return; }

                dataType = silaElement.DataType;
            }
            else
            {
                dataType = parameter.DataType;
            }

            // check if parameter has constraints
            if (!(dataType.Item is ConstrainedType cType)) { return; }

            // check if parameter type matches the type of the given parameter value
            if (!(cType.DataType.Item is BasicType.Real)) { return; }

            // check for constraints
            if (cType.Constraints.MaximalExclusive != null && parameterValue >= double.Parse(cType.Constraints.MaximalExclusive, CultureInfo.InvariantCulture))
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                    feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                    "Value out of bound: the value must be less than " + cType.Constraints.MaximalExclusive));
            }

            if (cType.Constraints.MaximalInclusive != null && parameterValue > double.Parse(cType.Constraints.MaximalInclusive, CultureInfo.InvariantCulture))
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                    feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                    "Value out of bound: the value must not be greater than " + cType.Constraints.MaximalInclusive));
            }

            if (cType.Constraints.MinimalExclusive != null && parameterValue <= double.Parse(cType.Constraints.MinimalExclusive, CultureInfo.InvariantCulture))
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                    feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                    "Value out of bound: the value must be greater than " + cType.Constraints.MinimalExclusive));
            }

            if (cType.Constraints.MinimalInclusive != null && parameterValue < double.Parse(cType.Constraints.MinimalInclusive, CultureInfo.InvariantCulture))
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                    feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                    "Value out of bound: the value must not be less than " + cType.Constraints.MinimalInclusive));
            }
        }

        /// <summary>
        /// Validates the given value of an Integer type parameter according to the constrains defined for the parameter of the given command in the given feature
        /// </summary>
        /// <param name="parameterValue">The value to be validated.</param>
        /// <param name="feature">The feature that defines the given parameter.</param>
        /// <param name="commandIdentifier">The identifier of the command the parameter is defined for.</param>
        /// <param name="parameterIdentifier">The identifier of the parameter thats constraints shall be used for validation.</param>
        public static void ValidateParameter(long parameterValue, Feature feature, string commandIdentifier, string parameterIdentifier)
        {
            // lookup command
            if (!(feature.Items.FirstOrDefault(i => i is FeatureCommand && ((FeatureCommand)i).Identifier == commandIdentifier) is FeatureCommand command)) { return; }

            // lookup parameter
            if (!(command.Parameter.FirstOrDefault(p => p.Identifier == parameterIdentifier) is SiLAElement parameter)) { return; }

            // get parameter data type
            DataTypeType dataType;
            if (parameter.DataType.Item is string s)
            {
                // get according datatype definition
                if (!(feature.Items.FirstOrDefault(i => i is SiLAElement && ((SiLAElement)i).Identifier == s) is SiLAElement silaElement)) { return; }

                dataType = silaElement.DataType;
            }
            else
            {
                dataType = parameter.DataType;
            }

            // check if parameter has constraints
            if (!(dataType.Item is ConstrainedType cType)) { return; }

            // check if parameter type matches the type of the given parameter value
            if (!(cType.DataType.Item is BasicType.Integer)) { return; }

            // check for constraints
            if (cType.Constraints.MaximalExclusive != null && parameterValue >= long.Parse(cType.Constraints.MaximalExclusive, CultureInfo.InvariantCulture))
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                    feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                    "Value out of bound: the value must be less than " + cType.Constraints.MaximalExclusive));
            }

            if (cType.Constraints.MaximalInclusive != null && parameterValue > long.Parse(cType.Constraints.MaximalInclusive, CultureInfo.InvariantCulture))
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                    feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                    "Value out of bound: the value must not be greater than " + cType.Constraints.MaximalInclusive));
            }

            if (cType.Constraints.MinimalExclusive != null && parameterValue <= long.Parse(cType.Constraints.MinimalExclusive, CultureInfo.InvariantCulture))
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                    feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                    "Value out of bound: the value must be greater than " + cType.Constraints.MinimalExclusive));
            }

            if (cType.Constraints.MinimalInclusive != null && parameterValue < long.Parse(cType.Constraints.MinimalInclusive, CultureInfo.InvariantCulture))
            {
                ErrorHandling.RaiseSiLAError(ErrorHandling.CreateValidationError(
                    feature.GetFullyQualifiedCommandParameterIdentifier(commandIdentifier, parameterIdentifier),
                    "Value out of bound: the value must not be less than " + cType.Constraints.MinimalInclusive));
            }
        }
    }
}